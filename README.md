<div align="center">
  <h1>Treedome</h1>

  <p>
    <img alt="development status" src="https://img.shields.io/badge/status-in%20development-yellow" /> <img alt="release" src="https://img.shields.io/gitlab/v/release/treedome%2Ftreedome"> <img alt="matrix chat" src="https://img.shields.io/matrix/treedome%3Amatrix.org" /> <img alt="license" src="https://img.shields.io/gitlab/license/treedome%2Ftreedome" />
  </p>

  <p>
    A local-first, encrypted, note taking application with tree-like structures, all written and saved in markdown.
    Supports <a href="https://www.youtube.com/watch?v=cC1CqyCN9Q0">Linux</a>, <a href=https://youtu.be/XxbJw8PrIkc?t=41">Windows</a>, and <a href="https://www.youtube.com/watch?v=jPiUN8fb3Og">a certain walled-garden operating system</a> .
  </p>
</div>

|           opening a document           |         document editor         |
| :------------------------------------: | :-----------------------------: |
| ![](docs/media/create_document_ss.png) | ![](docs/media/document_ss.png) |


<div align="center">
  <p>
    <a href="#warning">Warning</a> •
    <a href="#features">Features</a> •
    <a href="#must-read-docs">Must Read Docs</a> •
    <a href="#origin-and-namesake">Origin and Namesake</a> •
    <a href="https://matrix.to/#/#treedome:matrix.org">Join The Community</a>
  </p>
</div>

# Warning

This software is still unstable and has many rough edges. The encryption used by the software was not professionally audited, yet. You may check the code for yourself in `src-tauri/src/app/crypto.rs`. Also, The data structure used to serialize and deserialized from DB might break as long as we are not reaching v1.

Please be aware to always be polite and cool-headed when talking to anyone involved in the project. We are doing this out of our free time, let's make it fun and refreshing for everyone to do.

# Features

Please note that this project is currently maintained by a single person only. You may search for the current milestone by [accessing this page](https://gitlab.com/tengkuizdihar/treedome/-/milestones/1). Broadly speaking, these are the features that's included in the application.

- [x] Storing notes securely with encryption
- [x] Storing notes inside of a file by using SQLite
- [x] Editing notes with a rich text editor
- [x] Embedding pictures inside of notes
- [x] Full text searching
- [x] Tree like note organization
- [ ] Customization
  - [ ] Custom keybinding
  - [ ] Custom theme
  - [ ] Custom zooming
  - [ ] Internationalization

# Must Read Docs

* [Installation](./docs/installation.md)
* [Comparison with Similar Applications](./docs/comparison-with-similar-applications.md)
* [Getting Started to Development](./docs/environment-setup-and-building.md)
* [For First Time Contributor](./docs/first-time-contributor.md)
* [About Encryption](./docs/encryption.md)
* [Release Standard Operating Procedure](./docs/release-sop.md)

# Origin and Namesake

The name "Treedome" came from the tree-like structure of notes and the protection/dome that encrypts your information. The main inspiration of making this application was from the creator's usage of another awesome note taking application, [CherryTree](https://github.com/giuspen/cherrytree). Although CherryTree is still being actively developed by the creator and his community, it's by no mean a perfect application. There are aspects that Treedome's creator wants to improve.

These aspects, non-exhaustively, and in no particular order are:
* Encrypted notes seems to be a second class citizen in CherryTree.
* In encrypted form, every notes are loaded into memory (it seems). This is apparent when someone is trying to save photos and other big binary files into a note, saving which take a long time, add another empty note, save that note, and it still save in a long time.
* Built on top of non-web technologies. Although this might be a plus for some users, the creator would argue that web technologies and libraries are just full of great user experience libraries. Would be a waste to not utilize it.

Some other motivations are:
* This project is a platform to polish up my skill in using Rust, Typescript, Nix, Docker, etc.
* The creator wants to incorporate the latest in cryptography technologies that will protect his information.

The creator would hope that you like it, and if god willing, contribute to the project in some way.