# Release Standard Operating Procedure

Releasing a software and bumping version is hard. Let's not shoot ourself in the foot and do it properly.

- [Release Standard Operating Procedure](#release-standard-operating-procedure)
  - [Checklist](#checklist)
  - [1. Pick A Commit for Release](#1-pick-a-commit-for-release)
  - [2. Bump Version in Application](#2-bump-version-in-application)
  - [3. Smoke Test this Particular Commit](#3-smoke-test-this-particular-commit)
  - [4. Create a Tag](#4-create-a-tag)
  - [5. Create a Release](#5-create-a-release)

## Checklist

- [ ] Pick a commit for release
  - [ ] Commit is after the latest release
  - [ ] Commit's last build is successful
  - [ ] Smoke test for the commit
- [ ] Pick a new version
  - [ ] Format is `x.y.z` and `x.y` will be defaulted to `x.y.0`
  - [ ] Version does not exist here https://codeberg.org/solver-orgz/treedome/releases
- [ ] Bump version in repository
  - [ ] `version` in `treedome.nix`
- [ ] Create a release note [detailed here](#5-create-a-release)

## 1. Pick A Commit for Release

Please pick a commit that you will use for the release. This particular commit can be chosen straight directly from master. Do not use any commit from any other branch that's not master, we are using trunk based development here.

## 2. Bump Version in Application

In your local machine, please checkout to the chosen commit and search for any versioning that might be there. Below are the potential files that you need to change.

* A `version` field  in `treedome.nix`.
* Ignore version in `Cargo.toml` for now, it's not used yet.

## 3. Smoke Test this Particular Commit

Smoke Test is testing an application and its important features before releasing it. You can do this by using the application and its daily usage for ten minutes. See what's wrong with it.

## 4. Create a Tag

Create a tag from the new commit in master. It's the commit with the new version bumped and smoke tested.

## 5. Create a Release

Create a new release based on a Tag. The title should be simple, the release version `0.4.3` for example, should be enough. You can always see the changes by doing https://codeberg.org/solver-orgz/treedome/compare/0.4.1...master for example.

```markdown
# Changelog

This release is all about XXX. Please consult this `git diff` for a more detailed changelog https://codeberg.org/solver-orgz/treedome/compare/0.4.1...0.4.2. These are the highlight of the release.

* Spotlight search now uses fuzzy search. You don't need to be 100% exact to find your notes by title and tags.
* Full path to your notes is now available below the tag field as [breadcrumbs](https://getbootstrap.com/docs/4.0/components/breadcrumb/).
* Document will close itself after a certain time of inactivity. The inactivity time can configured in escape menu→configure→idle timeout in milisecond. Configuration is encrypted along side your document.
* Releases artifact (e.g. deb file) will now be built automatically by woodpecker (codeberg's automation).
* Full text search will not use `ctrl+shift+f` will now search correctly with LARGE CASE.
* Fix tree not properly collapsing and when being opened from spotlight.
```