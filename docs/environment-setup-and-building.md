# Development Setup

We have first party support for nix and flake. It's a package management system that does literally everything, reproducibly (dependencies on your machine is the same as mine 99% of the time). Also, our preferred code editor is VSCode.

First, clone this repository to your system using `git clone https://gitlab.com/tengkuizdihar/treedome.git`. We will presume that you have installed [Nix](https://nixos.org/download.html) in your system. Just think of nix as `apt` but programmable. After installing Nix, please enable flake which is an experimental feature of Nix. Refer to [this document](https://nixos.wiki/wiki/Flakes) for enabling it.

Now that you've installed Nix and enabled Flake, you can `cd` into the Treedome project. To install all of the necessary program to build it, use `nix develop`. The first time you do this will take some time, but subsequent run will be much-much-much faster. After it's done, your terminal now have the power to access all of the installed dependencies like `cargo` and `npm`, but it doesn't "polute" the rest of your system. Meaning, you can't really break your OS by working on my project.

Using the same terminal, you can open your favorite text editor. I personally use `codium .`. Right now although you have installed all of the essentials, you haven't actually installed the project specific dependencies. Run `yarn install && cargo tauri dev` to install and build everything until it runs.

If you can open the application and use it normally, then it's all set! Sorry for making it a lot more involved than regular projects. But this way, everyone that already have nix and flake installed on their system can install the dependencies of my project just by using `nix develop`.

- [Development Setup](#development-setup)
- [Common Happenings](#common-happenings)
  - [GLIB Error](#glib-error)
  - [Building for production](#building-for-production)
  - [Prevent Flake from Garbage Collected](#prevent-flake-from-garbage-collected)

# Common Happenings

## GLIB Error

This most likely happens if you're not using any famous GTK Program in your system. Will also happens if you're using NixOS. If the program hangs when you click to open a path to your document or get this error,
```bash
GLib-GIO-ERROR **: 08:50:41.565: No GSettings schemas are installed on the system
```
This means you're directly using the program from terminal after you `nix build` it. If you are using NixOS please this lines to your `configuration.nix`.

```nix
  programs.bash = { 
    interactiveShellInit = ''
      export XDG_DATA_DIRS="${pkgs.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}:${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}:$XDG_DATA_DIRS"
    '';
  };
```

## Building for production

This project is entirely built on top of nix + flake. Please use `nix build` and see the binary inside of a newly created folder called `result`.

## Prevent Flake from Garbage Collected

Sometimes you want to free up some space with `sudo nix-collect-garbage` and it just so happens to delete the environment used for the project. You can start by using `nix develop --profile dev` to make sure it will not go away when you collect garbage again.