# Manual Development Setup

## Prerequisites

The following packages are required to build treedome.

`cargo git-lfs yarn`

To install them on Arch Linux run:

`sudo pacman -S cargo git-lfs yarn`

## Development Build

To clone the repo and assets stored under lfs:

```
git clone https://codeberg.org/solver-orgz/treedome.git
cd treedome
git lfs install
git lfs fetch --all
git lfs checkout
```

To run the development build:

`yarn && yarn run tauri dev`

## error: failed to read icon

If you receive this error:

```
error: proc macro panicked
  --> src/main.rs:79:14
   |
79 |         .run(tauri::generate_context!())
   |              ^^^^^^^^^^^^^^^^^^^^^^^^^^
   |
   = help: message: failed to read icon /home/user/Git/treedome/src-tauri/icons/32x32.png: Invalid PNG signature.

error: could not compile `treedome` (bin "treedome") due to 1 previous error
```

It is because the png files that are stored in git lfs are not checked out properly.

Install `git-lfs` and run these commands within the git repo.

```
git lfs install
git lfs fetch --all
git lfs checkout
````

And then try and rebuild the project.
