# Installation

So, you found this new application and just want to go straight installing this app into your computer. Alright, here is the list of supported operating system.

- [Installation](#installation)
  - [Debian/Ubuntu](#debianubuntu)
  - [Arch](#arch)
  - [NixOS](#nixos)
  - [NixOS + Flake](#nixos--flake)
  - [Certain Walled Garden Operating System](#certain-walled-garden-operating-system)

## Debian/Ubuntu

User can directly download the `.deb` file from release page. It's usually still in its archive form, so you need to extract it first. Then, install the app using `sudo dpkg -i /path/to/treedome_amd64.deb`. If there are new update for this app, you can reinstall it using `sudo dpkg -i /path/to/treedome_amd64.deb`. Auto updater is not configured for this application.


## Arch

If you're a user of Arch Linux (btw), you can use the recipe in https://aur.archlinux.org/packages/treedome-bin. Thank you @alyway for the package!

## NixOS

ATTENTION: currently the application is not submitted to nixpkgs yet, so you may need to wait for it. User of NixOS with Flake can add this application repository directly.

Below is the result of `nix flake show` for those who are curious about the flake structure of this project. Any changes to this structure should make a change the major version.

```bash
[notroot@nixos:~/programming/treedome]$ nix flake show
git+file:///home/notroot/programming/treedome?ref=refs%2fheads%2fmaster&rev=0804914c6ac553bf5dcbe686f4fa735c9bec8712
├───devShells
│   └───x86_64-linux
│       └───default: development environment 'nix-shell'
└───packages
    └───x86_64-linux
        └───default: package 'treedome-0.1'
```

## NixOS + Flake

User of this particular OS rejoice! You can directly add it into your system by following these steps.

First, in your `flake.nix`, add these two lines to your input.

```nix
inputs = {
  # There are many ways to reference flake inputs.
  # The most widely used is `github:owner/name/reference`,
  # which represents the GitHub repository URL + branch/commit-id/tag.

  # Official NixOS package source
  nixpkgs.url = "github:NixOS/nixpkgs/your-nixpkgs-version";
  
  # Add these two lines to add treedome, and let treedome follow your nixpkgs version to save space
  treedome.url = "gitlab:treedome/treedome/master";
  treedome.inputs.nixpkgs.follows = "nixpkgs";
};
```

Second, please add treedome to the arguments of the outputs. Add it like so.

```nix
outputs = { self, nixpkgs, nixpkgs-unstable, treedome,... }@inputs: { .. }
```

Third, at the part where you have `lib.nixosSystem`, search for a line that has `modules` field in it. Beside `modules` and **not** in it, add a field called `specialArgs = { inherit treedome; };`. This new line will make `treedome` available inside your `modules`. Below is an example of how it should look.

```nix
specialArgs = { inherit treedome; };
modules = [
  ./configuration.nix
];
```

Fourth, inside of your `configuration.nix` module, add `treedome` as your argument so it's accessible. After doing that add `treedome.packages.x86_64-linux.default` inside of `environment.systemPackages` array. Similar to the example below.

```nix
{ config, pkgs, treedome, ... }:
{
  environment.systemPackages = [
    treedome.packages.x86_64-linux.default
  ];
}
```

Lastly, do a `sudo nixos-rebuild switch` to apply your changes. If successful, you may access it from the terminal by `treedome` or by application `treedome`.

## Certain Walled Garden Operating System


ATTENTION: currently the application can be built and run in this OS. The problem lies in the automation of the package building, which are difficult to begin with.

1. Install yarn by following https://classic.yarnpkg.com/lang/en/docs/install/#mac-stable
2. Install rust by following curl https://sh.rustup.rs -sSf | sh
3. Run these commands to install all dependencies and build the application, `yarn && yarn run tauri build`
4. At the end of the command, there will be a prompt to drag and drop treedome to applications. If you missed this, the *.dmg is located at `src-tauri/target/release/bundle/dmg/treedome_0.0.0_aarch64.dmg`.
