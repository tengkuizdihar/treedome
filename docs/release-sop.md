# Release Standard Operating Procedure

Releasing a software and bumping version is hard. Let's not shoot ourself in the foot and do it properly.

- [Release Standard Operating Procedure](#release-standard-operating-procedure)
  - [1. Pick A Commit for Release](#1-pick-a-commit-for-release)
  - [2. Bump Version in Application](#2-bump-version-in-application)
  - [3. Smoke Test this Particular Commit](#3-smoke-test-this-particular-commit)
  - [4. Create a Tag](#4-create-a-tag)
  - [5. Create a Release](#5-create-a-release)


## 1. Pick A Commit for Release

Please pick a commit that you will use for the release. This particular commit can be chosen straight directly from master. Do not use any commit from any other branch that's not master, we are using trunk based development here.

## 2. Bump Version in Application

In your local machine, please checkout to the chosen commit and search for any versioning that might be there. Below are the potential files that you need to change.

* A `version` field  in `treedome.nix`.
* Ignore version in `Cargo.toml` for now, it's not used yet.

## 3. Smoke Test this Particular Commit

Smoke Test is testing an application and its important features before releasing it. You can do this by using the application and its daily usage for ten minutes. See what's wrong with it.

## 4. Create a Tag

Create a tag from the new commit in master. It's the commit with the new version bumped and smoke tested.

## 5. Create a Release

Create a new release based on a Tag. The title should be simple, the release version `0.3` for example, should be enough.

```markdown
This release is all about the "homework" part of this project.
* Move project from tengkuizdihar/treedome to treedome/treedome. Makes it more official, does it not?
* Relicense from AGPLv3 to GPLv3.
  * Motivation: https://news.ycombinator.com/item?id=24764481 and https://writing.kemitchell.com/2021/01/24/Reading-AGPL. AGPL is full of holes.
* Update to documentations.
* Refactor on frontend, so we can change encoding/decoding data easily in the future.
* Fixes to flake so it can be used by NixOS+Flake.
```