# Rules of Contributor

There are several rules when you want to interact with anyone that's involved with the projects, even the average layman user.

1. **Be nice**, we are doing all of this out of our free time. The last thing you want to do is to make the other's person day a bad one.
2. **Assume the best of others**. No one is trying to attack anyone. If the other party sounds rude, assumes the best out of them and refer to rule number 1.
3. **Everyone is dumb and wants to learn**. That's of course includes **you**. Learn to be patient and cooperative at explaining thing to other people. If they don't get it or doesn't understand your question, then *ask* or *verify* what they don't get.

If all else fails, reach out to admins or other prominent members of the project. Remember, you are not alone.

- [Rules of Contributor](#rules-of-contributor)
- [What should you do?](#what-should-you-do)

# What should you do?

You can be a lot of things in this young open source project. For example, you can:
* Add new exciting functionality to the app
* Fix that one annoying bug
* Create massive refactor pull request things so it can finally scratch your itchy perfectionist personality
* Make things look prettier
* Optimize the application
* Create new issues and feature request
* ... and more!

But generally speaking before you do all that, please be mindful of these things.
1. Communicate with the community, we are available here ![Matrix](https://img.shields.io/matrix/treedome%3Amatrix.org)!
2. Read anything that ends with `.md`, the documentation was meant to be read lightly in your spare time, with simple choice of words. If it's not simple enough, please raise an issue.
3. Be nice and cool your head before interacting with anyone, ever. If you can't do that, put the laptop/phone away for a moment.
