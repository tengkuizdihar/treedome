# Installation

So, you found this new application and just want to go straight installing this app into your computer. Alright, here is the list of supported operating system.

- [Installation](#installation)
  - [Debian/Ubuntu](#debianubuntu)
  - [Arch](#arch)
  - [NixOS](#nixos)
  - [NixOS + Flake](#nixos--flake)
  - [Walled Garden Operating System](#walled-garden-operating-system)
  - [Windows](#windows)

## Debian/Ubuntu

User can directly download the `.deb` file from release page. It's usually still in its archive form, so you need to extract it first. Then, install the app using `sudo dpkg -i /path/to/treedome_amd64.deb`. If there are new update for this app, you can reinstall it using `sudo dpkg -i /path/to/treedome_amd64.deb`. Auto updater is not configured for this application.

## Arch

If you're a user of Arch Linux (btw), you can use the recipe in https://aur.archlinux.org/packages/treedome-bin. Thank you @alliegaytor for the package!

## NixOS

You can install it to your Nix derivation by adding `treedome` to `environment.systemPackages`. It should be noted that treedome is not yet available for Nixos 23.11 but is available in Nixos Stable.

## NixOS + Flake

**_BIG WARNING: currently this is broken because flake doesn't support LFS yet, even though LFS is already supported in Nix_**

User of this particular OS rejoice! You can directly add it into your system by following these steps. First, in your `flake.nix`, add these two lines to your input.

```nix
inputs = {
  # There are many ways to reference flake inputs.
  # The most widely used is `github:owner/name/reference`,
  # which represents the GitHub repository URL + branch/commit-id/tag.

  # Official NixOS package source
  nixpkgs.url = "github:NixOS/nixpkgs/your-nixpkgs-version";

  # Add these two lines to add treedome, and let treedome follow your nixpkgs version to save space
  treedome.url = "git+https://codeberg.org/solver-orgz/treedome?ref=master&shallow=1";

  # uncomment the comment below if you want to save space, but treedome might break if you do this because it's using a different version of nixpkgs
  # treedome.inputs.nixpkgs.follows = "nixpkgs";
};
```

Second, please add treedome to the arguments of the outputs. Add it like so.

```nix
outputs = { self, nixpkgs, nixpkgs-unstable, treedome,... }@inputs: { .. }
```

Third, at the part where you have `lib.nixosSystem`, search for a line that has `modules` field in it. Beside `modules` and **not** in it, add a field called `specialArgs = { inherit treedome; };`. This new line will make `treedome` available inside your `modules`. Below is an example of how it should look.

```nix
specialArgs = { inherit treedome; };
modules = [
  ./configuration.nix
];
```

Fourth, inside of your `configuration.nix` module, add `treedome` as your argument so it's accessible. After doing that add `treedome.packages.x86_64-linux.default` inside of `environment.systemPackages` array. Similar to the example below.

```nix
{ config, pkgs, treedome, ... }:
{
  environment.systemPackages = [
    treedome.packages.x86_64-linux.default
  ];
}
```

Lastly, do a `sudo nixos-rebuild switch` to apply your changes. If successful, you may access it from the terminal by `treedome` or by application `treedome`.

Below is the result of `nix flake show` for those who are curious about the flake structure of this project. Any changes to this structure should make a change the major version.

```bash
[notroot@nixos:~/programming/treedome]$ nix flake show
git+file:///home/notroot/programming/treedome?ref=refs%2fheads%2fmaster&rev=0804914c6ac553bf5dcbe686f4fa735c9bec8712
├───devShells
│   └───x86_64-linux
│       └───default: development environment 'nix-shell'
└───packages
    └───x86_64-linux
        └───default: package 'treedome-0.1'
```

## Windows

It's more than possible to run this program on windows. You can always use the .exe that's automatically built in https://codeberg.org/solver-orgz/treedome/releases.

For those who wants to build it themselves can use the instruction below. Please use terminals to run the commands below.

1. Install git through https://git-scm.com/downloads/win.
2. Run `git clone https://codeberg.org/solver-orgz/treedome.git`.
3. Get inside the treedome directory using `cd treedome`.
4. Install the rust language.
   1. Install [Scoop](https://scoop.sh/) for the package manager.
   2. Run `scoop bucket add main`.
   3. Run `scoop install main/rustup`.
5. Open and exit your terminal.
6. Install yarn to compile the frontend.
   1. Install NPM using https://nodejs.org/en/download/prebuilt-installer
   2. Open and exit your terminal.
   3. Install yarn by running `npm install -g yarn`.
7. Get the frontend dependencies using `yarn`.
8. Build the project by running `yarn run tauri build`.
9. The installer for treedome will be located in `src-tauri/target/x86_64-pc-windows-msvc/release/bundle/nsis/treedome_0.0.0_x64-setup.exe`.

## Walled Garden Operating System

ATTENTION:

- treedome is VERY usable on a certain walled garden operating system, but they are an unfriendly platform for open source development, probably will never have an automated building forever

1. Install homebrew by following `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
2. Install yarn by following `brew install yarn`
3. Install rust by following `curl https://sh.rustup.rs -sSf | sh`
4. Run these commands to install all dependencies and build the application, `yarn && yarn run tauri build`
5. At the end of the command, there will be a prompt to drag and drop treedome to applications. If you missed this, the \*.dmg is located at `src-tauri/target/release/bundle/dmg/treedome_0.0.0_aarch64.dmg`.
