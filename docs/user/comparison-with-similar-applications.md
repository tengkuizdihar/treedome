# Comparison with Similar Applications

warning: comparison might be outdated, please check the git history to see when it's last updated

The comparison made in this section will not be comprehensive and will include biases. In no particular order, we will look at the similarities and differences between existing note taking application. Seriously, **this section was created by a guy according to his ideals**. You should never expect an unbiased and objective view from him. But knows, that he tries his best to give a useful comparison to his audience.

- [Comparison with Similar Applications](#comparison-with-similar-applications)
  - [CherryTree](#cherrytree)
  - [Joplin](#joplin)
  - [Google Keep](#google-keep)

## CherryTree
The main inspiration for this application. Let's start with the similarities.
* Both are open source and licensed under GPL3, although CherryTree does give options for the user to use any late version of GPL3.
* Notes are organized in a tree like structure.
* It has a rich WYSIWYG text editor.
* Notes are stored in a single file.
* Options to encrypt notes with user provided passphrase.

But treedome can't be just a 1 to 1 recreation of what CherryTree is. Below is some of the difference.
* Encryption isn't enabled by default. All treedome notes are always encrypted.
* We uses Rust + Tauri + React to build our app. Meanwhile CherryTree uses C++ instead. Some may cringe in the creator's decision to use web based technologies, but web based technologies means abundance in libraries.
* The `.note` file (`.ctb` or `.ctx` in the case of CherryTree) are not loaded to the memory in its entirety. If you're seeing only a single note, then that single note is the only one that lives in memory.
* Saving and loading notes are done per note basis, not the whole file. This cuts down saving and loading time.
* The interface. We have dark mode by default, to save your eyes and sanity.

## Joplin
Another open source note taking application. But this one is a little bit different than ours. Let's start with our similarities.

* Its open source.
* Has a WYSIWYG text editor.

Wait, that's it? Yeah, as far as we're concerned that's it. The differences are listed below.

* Notes are organized in a tag based way, not trees. Nothing wrong with that.
* It forces you to sync your notes with a cloud provider. Treedome note's on the other hand store all of your notes inside of a single file.
* Your notes are [not encrypted at rest locally](https://www.reddit.com/r/joplinapp/comments/t4cfyd/yet_another_plea_for_local_database_encryption/) but will be encrypted at your cloud provider.
* It has more export options, we don't have any **yet**.

## Google Keep
This is probably what most people will use if they are using the google ecosystem. The similarity are listed below.

* It's a note taking application.
* It has some rich text options.
* It uses web based technologies.

Just like Joplin, it doesn't really have much in common with Treedome. So let's get to the differences.

* Google Keep is not open source. Treedome is licensed under AGPLv3, an [OSS approved](https://opensource.org/license/agpl-v3/) open source license.
* Everything is stored in Google's servers. Treedome is offline and stored in a single file.
* According to this [Google Keep's documentation](https://support.google.com/keep/answer/10431250?hl=en-GB), they are encrypting the notes in transit and when at rest. Treedome stores it securely in disk locally.
* Notes are simple in structure, with tagging system that can optionally be used. Treedome uses tree-like organization instead.