# Encryption Method

One of the main principle of Treedome, which are inspired by the creator's extensive usage of [cherrytree](https://github.com/giuspen/cherrytree), is encryption. It is critical to preserve the user's privacy at all time. Therefore, we think it's necessary to explain to our end user about how we generally use the encryption here.

Although encryption is used for your notes, there are some data that's stored in plaintext:
* recently opened document's location.
* note's id, they are randomly generated so no need to encrypt it.
* cryptographic things that are expected to be stored in plaintext such as *nonces* and *salts*.

- [Encryption Method](#encryption-method)
  - [General Description](#general-description)
  - [Data Flows](#data-flows)
  - [Data Structure - General](#data-structure---general)
  - [Data Structure - Encrypted Table](#data-structure---encrypted-table)
  - [References](#references)

## General Description

Internally at `src-tauri/src/app/crypto.rs`, we are using [XChaCha20-Poly1305](https://en.wikipedia.org/wiki/ChaCha20-Poly1305) as the stream cipher. The private key is then derived from quality checked password using [Scrypt](https://en.wikipedia.org/wiki/Scrypt) with a hardcoded per version SCRYPT_DIFFICULTY of 15. There are of course several data that we **MUST** store in plaintext which are **password salt** for Scrypt and **nonces** for XChaCha20-Poly1305.

Because of how we generate nonce, which is from a secure random generation, we cannot use the regular ChaCha20. Instead we use XChaCha20-Poly1305 that's known to be resistant against nonce-misused

## Data Flows

Below is the diagram to describe the data flow when encrypting. Decryption will do the same thing, but in backwards.

[![](https://mermaid.ink/img/pako:eNp9klFLwzAQx79KyNMGG52KL3sQylpRNruwBpy2Q2JzW4NtUtLUWce-u1lThw_DEMjl8vvnLpc74ExxwFO8LdQ-y5k2iAaprJv3nWZVjkKZ6bYyQslUIjti6q_oYNAtwyEaj-8Q8eP4ebkK_gCdP_YX1Pl-icQjBRPSwJdBhNX1XmnubRw8W72QHj8JEy-GrNFQtGjFJFel-AaOYlaYS4LOTgLQ4hPQHFp0r1V5DuEEZN4nM088R3JE7MJMJ-mvXc8efDsd2m-SvgioqYXcWW_O7LyejIkq2qubye3G4dEymoWXE4-UzOBSCLLwHyMarmlfG0RPxQmYYf8k1B2EUffqMHgLfOonXp-kDebUqQTJ8QiXoEsmuP3iw-mSFJscSkjx1Jqc6Y8Up_JoOdYYFbcyw9MtK2oY4abitjaBYLYTyrMXuDBKP7mm6XpnhCsmX5WyjNENHH8AwGu5xA?type=png)](https://mermaid.live/edit#pako:eNp9klFLwzAQx79KyNMGG52KL3sQylpRNruwBpy2Q2JzW4NtUtLUWce-u1lThw_DEMjl8vvnLpc74ExxwFO8LdQ-y5k2iAaprJv3nWZVjkKZ6bYyQslUIjti6q_oYNAtwyEaj-8Q8eP4ebkK_gCdP_YX1Pl-icQjBRPSwJdBhNX1XmnubRw8W72QHj8JEy-GrNFQtGjFJFel-AaOYlaYS4LOTgLQ4hPQHFp0r1V5DuEEZN4nM088R3JE7MJMJ-mvXc8efDsd2m-SvgioqYXcWW_O7LyejIkq2qubye3G4dEymoWXE4-UzOBSCLLwHyMarmlfG0RPxQmYYf8k1B2EUffqMHgLfOonXp-kDebUqQTJ8QiXoEsmuP3iw-mSFJscSkjx1Jqc6Y8Up_JoOdYYFbcyw9MtK2oY4abitjaBYLYTyrMXuDBKP7mm6XpnhCsmX5WyjNENHH8AwGu5xA)

Notes:
* Plaintext Password is not stored anywhere in the disk.
* Securely Randomize Salt is stored in plaintext. It supposed to be stored in plaintext. (reference for why its stored in plaintext needed)
* Private key are generated and not stored in the disk.
* Securely Randomized Nonce is stored in plaintext. It supposed to be stored in plaintext. (reference for why its stored in plaintext needed)
* Plaintext Data (data generated by user such as notes) is not stored anywhere in the disk.

## Data Structure - General

There are 3 tables that we use:

1. **Plaintext Table**, the only table that's not encrypted at all. This should only be used to store salts and other non-sensitive, configurable part of the encryption such as password salt. **Do not put any user preference/information in this table**. You may do that in the Meta Table, explained below.
2. **Meta Table**, an encrypted table that has only one row, with ID `main`. This table will store the note-tree structure, note titles, and user preferences.
3. **Note Table**, an encrypted table that has many rows. The ID of each row is generated using [UUID-v4](https://datatracker.ietf.org/doc/html/rfc4122) and stored in plaintext. The ID should be in plaintext for ease of retrieval and the project maintainer doesn't see a point in encrypting a random information anyway. The note itself, such as the title and plaintext content are encrypted at rest.

## Data Structure - Encrypted Table

Still working on the diagram...

## References

1. Too Much Crypto, Jean-Philippe Aumasson, https://eprint.iacr.org/2019/585.pdf
2. On Misuse of Nonce-Misuse Resistance, Mustafa Khairallah - Shivam Bhasin - Anupam Chattopadhyay, https://eprint.iacr.org/2019/1492.pdf
3. General description of XChaCha20, https://en.wikipedia.org/wiki/ChaCha20-Poly1305
4. General description of Scrypt, https://en.wikipedia.org/wiki/Scrypt
5. The scrypt Password-Based Key Derivation Function, Colin Percival - Simon Josefsson, https://datatracker.ietf.org/doc/html/rfc7914
6. A Universally Unique IDentifier (UUID) URN Namespace, Paul J. Leach - Rich Salz - Michael H. Mealling, https://datatracker.ietf.org/doc/html/rfc4122
