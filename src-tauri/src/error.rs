use thiserror::Error;

use crate::app::crypto::v1::CipherError;

#[derive(Error, Debug)]
pub enum TreedomeError {
    #[error("Failed opening document, {0}")]
    FailedOpeningDocument(String),

    #[error("SqlxError {0}")]
    SqlxError(#[from] sqlx::Error),

    #[error("DeserializationError {0}")]
    DeserializationError(#[from] ciborium::de::Error<std::io::Error>),

    #[error("SerializationError {0}")]
    SerializationError(#[from] ciborium::ser::Error<std::io::Error>),

    #[error("TauriError {0}")]
    TauriError(#[from] tauri::Error),

    #[error("CryptoError {0}")]
    CryptoError(#[from] CryptoError),

    #[error("{0}")]
    SecretValidationError(#[from] SecretValidationError),

    #[error("Version Error: not supporting {0}")]
    VersionError(String),

    #[error("{0}")]
    RepositoryValidationError(String),

    #[error("{0}")]
    RegexError(String),
}

#[derive(Error, Debug)]
pub enum SecretValidationError {
    #[error("{0}")]
    ValidationError(String),
}

#[derive(Error, Debug)]
pub enum CryptoError {
    #[error("CipherError {0}")]
    CipherError(#[from] CipherError),
}
