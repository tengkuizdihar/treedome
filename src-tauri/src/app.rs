use std::{
    ops::Deref,
    path::{Path, PathBuf},
    sync::Arc,
};

use sqlx::sqlite::{SqliteConnectOptions, SqliteJournalMode, SqliteSynchronous};

use crate::error::TreedomeError;

use self::{
    crypto::{init_secret, v1::Cipher},
    db::MetaModel,
    repository::{
        sqlite::{
            init::{
                init_first_note_sqlite, init_meta_model_sqlite, init_plaintext_model_sqlite,
                init_table_sqlite,
            },
            DocumentRepositorySqlite, MetaRepositorySqlite, NoteRepositorySqlite,
        },
        DocumentRepository, MetaRepository, NoteRepository,
    },
    util::backup_file_if_exist,
};

pub mod constant;
pub mod crypto;
pub mod db;
pub mod model;
pub mod repository;
pub mod util;

pub struct TreedomeAppWrap(tauri::async_runtime::RwLock<TreedomeApp>);

impl TreedomeAppWrap {
    pub fn new_empty_document() -> Self {
        TreedomeAppWrap(TreedomeApp::new().into())
    }

    pub async fn document_close(&self) {
        let mut app = self.0.write().await;
        *app = TreedomeApp::Uninitialized;
    }

    pub async fn document_open<T: AsRef<Path>>(
        &self,
        path: T,
        password: &str,
    ) -> Result<MetaModel, TreedomeError> {
        self.document_close().await; // close current document before opening another one

        let mut app = self.0.write().await;
        let (new_app, meta_model) =
            TreedomeApp::new_open_document_sqlite(path.as_ref(), password).await?;
        *app = new_app;

        Ok(meta_model)
    }
}

impl std::fmt::Debug for TreedomeAppWrap {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("TreedomeAppWrap").finish()
    }
}

impl Deref for TreedomeAppWrap {
    type Target = tauri::async_runtime::RwLock<TreedomeApp>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub enum TreedomeApp {
    Initialized {
        open_document: PathBuf,
        note_repository: Arc<Box<dyn NoteRepository + Send + Sync>>,
        meta_repository: Arc<Box<dyn MetaRepository + Send + Sync>>,
        document_repository: Arc<Box<dyn DocumentRepository + Send + Sync>>,
        secret: Arc<Cipher>,
    },

    Uninitialized,
}

impl TreedomeApp {
    fn new() -> Self {
        return TreedomeApp::Uninitialized;
    }

    async fn new_open_document_sqlite<T: AsRef<Path>>(
        path: T,
        password: &str,
    ) -> Result<(Self, MetaModel), TreedomeError> {
        validate_path(&path)?;
        backup_file_if_exist(&path)?;

        // Init Resources
        let connection_options = SqliteConnectOptions::new()
            .filename(&path)
            .create_if_missing(true)
            .journal_mode(SqliteJournalMode::Truncate)
            .synchronous(SqliteSynchronous::Extra)
            .auto_vacuum(sqlx::sqlite::SqliteAutoVacuum::Incremental);

        let conn = sqlx::sqlite::SqlitePoolOptions::new()
            .min_connections(5)
            .max_connections(10)
            .connect_with(connection_options)
            .await?;

        let _ = init_table_sqlite(&conn).await?;
        let model = init_plaintext_model_sqlite(&conn).await?;

        match model.version.as_str() {
            "v1" => {
                // Init Secret
                let secret = init_secret(model, Some(&path), password)?;

                // Init Repository
                let note_repository = Box::new(NoteRepositorySqlite::new(conn.clone()));
                let meta_repository = Box::new(MetaRepositorySqlite::new(conn.clone()));
                let document_repository = Box::new(DocumentRepositorySqlite::new(conn.clone()));

                // Init Data When Empty
                let first_node_id = init_first_note_sqlite(&note_repository, &secret).await?;
                let meta_model =
                    init_meta_model_sqlite(&meta_repository, &secret, &first_node_id).await?;

                Ok((
                    Self::Initialized {
                        open_document: path.as_ref().to_path_buf(),
                        note_repository: Arc::new(note_repository),
                        meta_repository: Arc::new(meta_repository),
                        document_repository: Arc::new(document_repository),
                        secret: secret,
                    },
                    meta_model,
                ))
            }

            _ => Err(TreedomeError::VersionError(model.version)),
        }
    }
}

fn validate_path<T: AsRef<Path>>(path: T) -> Result<(), TreedomeError> {
    let process_path = path
        .as_ref()
        .extension()
        .map(|e| e.to_str())
        .flatten()
        .map(|s| s.ends_with(constant::TREEDOME_EXTENSION));

    if let Some(true) = process_path {
        return Ok(());
    }

    Err(TreedomeError::FailedOpeningDocument(
        "file name must ends with .note".to_string(),
    ))
}

#[cfg(test)]
mod test {
    use std::path::Path;

    #[test]
    fn path_testing() {
        let backed_up_path = Path::new("coba/lmao/coba")
            .file_stem()
            .map(|f| AsRef::<Path>::as_ref(f).with_extension("bak.note"));

        println!("{:?}", backed_up_path.unwrap());
    }
}
