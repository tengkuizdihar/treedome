pub mod model;
pub mod sqlite;

use async_trait::async_trait;
use model::NoteSizeResultRepo;

use crate::error::TreedomeError;

use self::model::{
    ExportWithCipherParamRepo, ExportWithCipherResultRepo, MetaGetParamRepo, MetaGetResultRepo,
    MetaPutParamRepo, MetaPutResultRepo, NoteDeleteParamRepo, NoteDeleteResultRepo,
    NoteDuplicateParamRepo, NoteDuplicateResultRepo, NoteGetFirstParamRepo, NoteGetFirstResultRepo,
    NoteGetParamRepo, NoteGetResultRepo, NotePutParamRepo, NotePutResultRepo, NoteSearchParamRepo,
    NoteSearchResultRepo,
};

#[async_trait]
pub trait NoteRepository {
    async fn note_search(
        &self,
        param: NoteSearchParamRepo,
    ) -> Result<NoteSearchResultRepo, TreedomeError>;
    async fn note_put(&self, param: NotePutParamRepo) -> Result<NotePutResultRepo, TreedomeError>;
    async fn note_get(&self, param: NoteGetParamRepo) -> Result<NoteGetResultRepo, TreedomeError>;
    async fn note_delete(
        &self,
        param: NoteDeleteParamRepo,
    ) -> Result<NoteDeleteResultRepo, TreedomeError>;
    async fn get_first_note(
        &self,
        param: NoteGetFirstParamRepo,
    ) -> Result<NoteGetFirstResultRepo, TreedomeError>;
    async fn note_duplicate(
        &self,
        param: NoteDuplicateParamRepo,
    ) -> Result<NoteDuplicateResultRepo, TreedomeError>;
    async fn note_sizes(&self) -> Result<NoteSizeResultRepo, TreedomeError>;
}

#[async_trait]
pub trait MetaRepository {
    async fn meta_put(&self, param: MetaPutParamRepo) -> Result<MetaPutResultRepo, TreedomeError>;
    async fn meta_get(&self, param: MetaGetParamRepo) -> Result<MetaGetResultRepo, TreedomeError>;
}

#[async_trait]
pub trait DocumentRepository {
    async fn document_export_with_cipher(
        &self,
        param: ExportWithCipherParamRepo,
    ) -> Result<ExportWithCipherResultRepo, TreedomeError>;

    async fn document_compact(&self) -> Result<(), TreedomeError>;
}
