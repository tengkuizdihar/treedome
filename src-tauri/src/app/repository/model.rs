use std::{path::PathBuf, sync::Arc};

use crate::app::{
    crypto::v1::Cipher,
    db::{EncryptedModel, MetaModel, NoteModel, PlaintextModel},
    model::{DuplicatedNote, SearchResult},
};

pub struct NotePutParamRepo {
    pub encrypted_data: EncryptedModel<NoteModel>,
}

pub struct NotePutResultRepo {
    pub new_key: String,
}

pub struct NoteGetParamRepo {
    pub ids: Vec<String>,
}

pub struct NoteGetResultRepo {
    pub model: Vec<EncryptedModel<NoteModel>>,
}

pub struct NoteGetFirstParamRepo {}

pub struct NoteGetFirstResultRepo {
    pub model: Option<(String, EncryptedModel<NoteModel>)>,
}

pub struct NoteDeleteParamRepo {
    pub id: String,
}

pub struct NoteDeleteResultRepo {
    #[allow(dead_code)]
    pub existed: bool,
}

pub struct NoteSearchParamRepo {
    pub query: String,
    pub secret: Arc<Cipher>,
}

#[derive(Debug)]
pub struct NoteSearchResultRepo {
    pub found: Vec<SearchResult>,
}

pub struct MetaPutParamRepo {
    pub encrypted_data: EncryptedModel<MetaModel>,
}

pub struct MetaPutResultRepo {}

pub struct MetaGetParamRepo {}

pub struct MetaGetResultRepo {
    pub model: Option<EncryptedModel<MetaModel>>,
}

pub struct ExportWithCipherParamRepo {
    pub old_document: PathBuf,
    pub old_secret: Arc<Cipher>,
    pub new_secret: Arc<Cipher>,
    pub new_plaintext: PlaintextModel,
}

pub struct ExportWithCipherResultRepo {
    #[allow(dead_code)]
    pub new_document: PathBuf,
}

pub struct NoteDuplicateParamRepo {
    pub ids: Vec<String>,
    pub secret: Arc<Cipher>,
}

pub struct NoteDuplicateResultRepo {
    pub old_to_new: Vec<DuplicatedNote>,
}

pub struct NoteSizeResultRepo {
    pub note_sizes: Vec<NoteSizeData>,
}

pub struct NoteSizeData {
    pub note_id: String,
    pub size: i64,
}
