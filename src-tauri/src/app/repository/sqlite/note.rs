use async_trait::async_trait;
use futures::StreamExt;
use regex::Regex;
use sqlx::Sqlite;
use tauri::async_runtime::spawn_blocking;

use crate::{
    app::{
        db::{DecryptedModel, EncryptedModel, NoteModel},
        model::{DuplicatedNote, FoundRange, SearchResult},
        repository::{
            model::{
                NoteDeleteParamRepo, NoteDeleteResultRepo, NoteDuplicateParamRepo,
                NoteDuplicateResultRepo, NoteGetFirstParamRepo, NoteGetFirstResultRepo,
                NoteGetParamRepo, NoteGetResultRepo, NotePutParamRepo, NotePutResultRepo,
                NoteSearchParamRepo, NoteSearchResultRepo, NoteSizeData, NoteSizeResultRepo,
            },
            NoteRepository,
        },
    },
    error::TreedomeError,
};

use super::{NoteRepositorySqlite, TreedomeSqliteConnection, DBV1};

impl NoteRepositorySqlite {
    pub fn new(conn: TreedomeSqliteConnection) -> Self {
        Self { conn }
    }
}

const NOTE_GET_ONE_V1: &str = "SELECT id, data FROM note WHERE id = ?";
const NOTE_GET_ALL_V1: &str = "SELECT id, data FROM note";
const NOTE_PUT_V1: &str = "INSERT OR REPLACE INTO note(id, data) VALUES (?, ?)";
const NOTE_DELETE_V1: &str = "DELETE FROM note WHERE id = ?";

fn generate_note_get_query_v1(len: usize) -> Result<String, TreedomeError> {
    if len < 1 {
        return Err(TreedomeError::RepositoryValidationError(format!(
            "length of note get query ids should be above 0 | value: {}",
            len
        )));
    }

    let arg_bind = (0..len).map(|_| "?").collect::<Vec<_>>().join(",");
    Ok(format!(
        "SELECT id, data FROM note WHERE id in ({})",
        arg_bind
    ))
}

#[async_trait]
impl NoteRepository for NoteRepositorySqlite {
    async fn note_search(
        &self,
        param: NoteSearchParamRepo,
    ) -> Result<NoteSearchResultRepo, TreedomeError> {
        let query = sqlx::query_as::<Sqlite, DBV1>(NOTE_GET_ALL_V1);
        let secret = param.secret.clone();
        let user_query = param.query.to_lowercase();
        let user_query = user_query.as_str(); // why? because we need to bind the first user_query so it will have some lifetime
        let re =
            Regex::new(r#"[\s\r\n]+"#).map_err(|e| TreedomeError::RegexError(e.to_string()))?;

        // TODO: not concurrent yet
        let found = query
            .fetch(&self.conn)
            // .for_each_concurrent(10, |v| async {
            //     let flat = v
            //         .ok()
            //         .map(|m| {
            //             ciborium::from_reader::<EncryptedModel<NoteModel>, _>(m.data.as_slice())
            //                 .ok()
            //         })
            //         .flatten();
            //     let secret = secret.clone();
            //     spawn_blocking(move || v.into_decrypt(&secret).ok())
            //         .await
            //         .ok()
            //         .flatten();
            // })
            .filter_map(|v| async {
                v.ok()
                    .map(|m| {
                        ciborium::from_reader::<EncryptedModel<NoteModel>, _>(m.data.as_slice())
                            .ok()
                    })
                    .flatten()
            })
            .filter_map(|v| async {
                let secret = secret.clone();
                spawn_blocking(move || v.into_decrypt(&secret).ok())
                    .await
                    .ok()
                    .flatten()
            })
            .filter_map(|decrypted| {
                let re_cloned = re.clone();
                let text = re_cloned
                    .replace_all(decrypted.data.text_only.as_str(), " ")
                    .to_string();

                async move {
                    let found_indices = text
                        .clone()
                        .to_ascii_lowercase()
                        .match_indices(user_query)
                        .map(|(i, found)| {
                            const AROUND_TEXT_LENGTH: usize = 30;

                            // this code RELIES on String::to_ascii_lowercase, because regular String::to_lowercase could expand
                            // lower case character and screw up the indexing.
                            let after =
                                String::from_utf8(text.bytes().skip(i + found.len()).collect())
                                    .map(|v| v.chars().take(AROUND_TEXT_LENGTH).collect::<String>())
                                    .unwrap_or_default();

                            // this code RELIES on String::to_ascii_lowercase, because regular String::to_lowercase could expand
                            // lower case character and screw up the indexing.
                            let before = String::from_utf8(text.bytes().take(i).collect())
                                .map(|v| {
                                    // goofy aah reverse and then reverse again
                                    // dealing with UTF-8 is worse than understanding quantum computing
                                    v.chars()
                                        .rev()
                                        .take(AROUND_TEXT_LENGTH)
                                        .collect::<String>()
                                        .chars()
                                        .rev()
                                        .collect::<String>()
                                })
                                .unwrap_or_default();

                            // will panic because AROUND_TEXT_LENGTH may end up INSIDE a unicode character
                            // match_indices uses byte spans, but usable text is using char spans WHY
                            FoundRange {
                                start: i as i64,
                                end: (i + found.len()) as i64, // turns out, len is byte sized, not char sized. Use string.chars().count() instead.
                                current: text.as_str()[i..i + found.len()].to_string(), // should be safe
                                before: before,
                                after: after,
                            }
                        })
                        .collect::<Vec<_>>();

                    let tags: Vec<String> = decrypted
                        .data
                        .tags
                        .clone()
                        .unwrap_or_default()
                        .iter()
                        .filter(|q| q.to_ascii_lowercase().contains(user_query))
                        .map(|q| q.to_string())
                        .collect();

                    let matching_name = decrypted
                        .data
                        .title
                        .to_ascii_lowercase()
                        .contains(user_query);

                    if found_indices.len() == 0 && tags.len() == 0 && !matching_name {
                        return None;
                    }

                    return Some(SearchResult {
                        note_id: decrypted.id,
                        title: decrypted.data.title,
                        ranges: found_indices,
                        tags: decrypted.data.tags.unwrap_or_default(),
                        text,
                    });
                }
            })
            .collect::<Vec<SearchResult>>()
            .await;

        Ok(NoteSearchResultRepo { found })
    }

    async fn note_put(&self, param: NotePutParamRepo) -> Result<NotePutResultRepo, TreedomeError> {
        let mut buffer = vec![];
        ciborium::into_writer(&param.encrypted_data, &mut buffer)?;

        let _ = sqlx::query::<Sqlite>(NOTE_PUT_V1)
            .bind(param.encrypted_data.get_owned_id())
            .bind(buffer)
            .execute(&self.conn)
            .await?;

        Ok(NotePutResultRepo {
            new_key: param.encrypted_data.get_owned_id(),
        })
    }

    async fn note_get(&self, param: NoteGetParamRepo) -> Result<NoteGetResultRepo, TreedomeError> {
        let get_query = generate_note_get_query_v1(param.ids.len())?;
        let mut query = sqlx::query_as::<Sqlite, DBV1>(&get_query);
        for id in param.ids {
            query = query.bind(id)
        }

        let result = query.fetch_all(&self.conn).await?.into_iter();
        let mut tried_result = Vec::with_capacity(result.len());
        for v in result {
            let encoded = ciborium::from_reader::<EncryptedModel<NoteModel>, _>(v.data.as_slice())?;
            tried_result.push(encoded);
        }

        Ok(NoteGetResultRepo {
            model: tried_result,
        })
    }

    async fn note_delete(
        &self,
        param: NoteDeleteParamRepo,
    ) -> Result<NoteDeleteResultRepo, TreedomeError> {
        let query = sqlx::query::<Sqlite>(NOTE_DELETE_V1).bind(param.id);
        let res = query.execute(&self.conn).await?;

        Ok(NoteDeleteResultRepo {
            existed: res.rows_affected() > 0,
        })
    }

    async fn get_first_note(
        &self,
        _: NoteGetFirstParamRepo,
    ) -> Result<NoteGetFirstResultRepo, TreedomeError> {
        let maybe_one = sqlx::query_as::<Sqlite, DBV1>(NOTE_GET_ALL_V1)
            .fetch_optional(&self.conn)
            .await?;

        if let Some(v) = maybe_one {
            return Ok(NoteGetFirstResultRepo {
                model: Some((
                    v.id,
                    ciborium::from_reader::<EncryptedModel<NoteModel>, _>(v.data.as_slice())?,
                )),
            });
        }

        Ok(NoteGetFirstResultRepo { model: None })
    }

    async fn note_duplicate(
        &self,
        mut param: NoteDuplicateParamRepo,
    ) -> Result<NoteDuplicateResultRepo, TreedomeError> {
        param.ids.sort();
        param.ids.dedup();

        let mut old_to_new: Vec<DuplicatedNote> = vec![];
        let mut tx = self.conn.begin().await?;
        for old_key in param.ids {
            // fetch from db
            let from_db = sqlx::query_as::<_, DBV1>(NOTE_GET_ONE_V1)
                .bind(&old_key)
                .fetch_optional(&mut *tx)
                .await?;

            if let Some(raw) = from_db {
                let secret = param.secret.clone();
                let (new_id, new_data) = spawn_blocking(move || -> Result<_, TreedomeError> {
                    let encrypted_note =
                        ciborium::from_reader::<EncryptedModel<NoteModel>, _>(raw.data.as_slice())?;

                    // still uses old nonce
                    let decrypted_note = encrypted_note.into_decrypt(&secret)?;

                    // reciphering with new nonce and id
                    let model =
                        DecryptedModel::new(None, decrypted_note.data).into_encrypt(&secret)?;

                    let new_id = model.get_owned_id();
                    let mut buffer = vec![];
                    ciborium::into_writer(&model, &mut buffer)?;
                    Ok((new_id, buffer))
                })
                .await??;

                let _ = sqlx::query::<Sqlite>(NOTE_PUT_V1)
                    .bind(&new_id)
                    .bind(new_data)
                    .execute(&mut *tx)
                    .await?;

                old_to_new.push(DuplicatedNote {
                    from: old_key,
                    to: new_id,
                })
            }
        }
        tx.commit().await?;

        Ok(NoteDuplicateResultRepo { old_to_new })
    }

    async fn note_sizes(&self) -> Result<NoteSizeResultRepo, TreedomeError> {
        let notes_size_data = sqlx::query_as::<Sqlite, DBV1>(NOTE_GET_ALL_V1)
            .fetch(&self.conn)
            .filter_map(|v| async { v.ok() })
            .map(|v| NoteSizeData {
                note_id: v.id,
                size: v.data.len() as i64,
            })
            .collect::<Vec<_>>()
            .await;

        Ok(NoteSizeResultRepo {
            note_sizes: notes_size_data,
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[ignore = "demonstration only"]
    #[test]
    fn testing_match_indices_in_byte_not_char() {
        let text = "私を愛して";
        let user_query = "愛";
        let found_indices = text
            .to_lowercase()
            .match_indices(user_query)
            .map(|(i, a)| {
                return FoundRange {
                    start: i as i64,
                    end: (i + user_query.len()) as i64, // turns out, len is byte sized, not char sized. Use string.chars().count() instead.
                    current: String::from(a),
                    before: text[(i - 20).clamp(0, text.len() - 1)..(i).clamp(0, text.len() - 1)]
                        .to_string(),
                    after: text[(i + user_query.len() + 20).clamp(0, text.len() - 1)
                        ..(i).clamp(0, text.len() - 1)]
                        .to_string(),
                };
            })
            .collect::<Vec<_>>();

        assert_eq!(
            found_indices[0],
            FoundRange {
                start: 6,
                end: 9,
                before: "".to_string(),
                current: "".to_string(),
                after: "".to_string(),
            }
        );
    }
}
