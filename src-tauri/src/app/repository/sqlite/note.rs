use async_trait::async_trait;
use futures::StreamExt;
use sqlx::Sqlite;
use tauri::async_runtime::spawn_blocking;

use crate::{
    app::{
        db::{DecryptedModel, EncryptedModel, NoteModel},
        model::{DuplicatedNote, FoundRange, SearchResult},
        repository::{
            model::{
                NoteDeleteParamRepo, NoteDeleteResultRepo, NoteDuplicateParamRepo,
                NoteDuplicateResultRepo, NoteGetFirstParamRepo, NoteGetFirstResultRepo,
                NoteGetParamRepo, NoteGetResultRepo, NotePutParamRepo, NotePutResultRepo,
                NoteSearchParamRepo, NoteSearchResultRepo,
            },
            NoteRepository,
        },
    },
    error::TreedomeError,
};

use super::{NoteRepositorySqlite, TreedomeSqliteConnection, DBV1};

impl NoteRepositorySqlite {
    pub fn new(conn: TreedomeSqliteConnection) -> Self {
        Self { conn }
    }
}

const NOTE_GET_ONE_V1: &str = "SELECT id, data FROM note WHERE id = ?";
const NOTE_GET_ALL_V1: &str = "SELECT id, data FROM note";
const NOTE_PUT_V1: &str = "INSERT OR REPLACE INTO note(id, data) VALUES (?, ?)";
const NOTE_DELETE_V1: &str = "DELETE FROM note WHERE id = ?";

fn generate_note_get_query_v1(len: usize) -> Result<String, TreedomeError> {
    if len < 1 {
        return Err(TreedomeError::RepositoryValidationError(format!(
            "length of note get query ids should be above 0 | value: {}",
            len
        )));
    }

    let arg_bind = (0..len).map(|_| "?").collect::<Vec<_>>().join(",");
    Ok(format!(
        "SELECT id, data FROM note WHERE id in ({})",
        arg_bind
    ))
}

#[async_trait]
impl NoteRepository for NoteRepositorySqlite {
    async fn note_search(
        &self,
        param: NoteSearchParamRepo,
    ) -> Result<NoteSearchResultRepo, TreedomeError> {
        let query = sqlx::query_as::<Sqlite, DBV1>(NOTE_GET_ALL_V1);

        let secret = param.secret.clone();
        let user_query = param.query.as_str();

        // TODO: not concurrent yet
        let found = query
            .fetch(&self.conn)
            // .for_each_concurrent(10, |v| async {
            //     let flat = v
            //         .ok()
            //         .map(|m| {
            //             ciborium::from_reader::<EncryptedModel<NoteModel>, _>(m.data.as_slice())
            //                 .ok()
            //         })
            //         .flatten();
            //     let secret = secret.clone();
            //     spawn_blocking(move || v.into_decrypt(&secret).ok())
            //         .await
            //         .ok()
            //         .flatten();
            // })
            .filter_map(|v| async {
                v.ok()
                    .map(|m| {
                        ciborium::from_reader::<EncryptedModel<NoteModel>, _>(m.data.as_slice())
                            .ok()
                    })
                    .flatten()
            })
            .filter_map(|v| async {
                let secret = secret.clone();
                spawn_blocking(move || v.into_decrypt(&secret).ok())
                    .await
                    .ok()
                    .flatten()
            })
            .filter_map(|decrypted| async move {
                let found_indices = decrypted
                    .data
                    .text_only
                    .to_lowercase()
                    .match_indices(user_query)
                    .map(|(i, _)| FoundRange {
                        start: i as i64,
                        end: (i + user_query.len()) as i64,
                    })
                    .collect::<Vec<_>>();

                if found_indices.len() == 0 {
                    return None;
                }

                return Some(SearchResult {
                    note_id: decrypted.id,
                    title: decrypted.data.title,
                    ranges: found_indices,
                });
            })
            .collect::<Vec<SearchResult>>()
            .await;

        Ok(NoteSearchResultRepo { found })
    }

    async fn note_put(&self, param: NotePutParamRepo) -> Result<NotePutResultRepo, TreedomeError> {
        let mut buffer = vec![];
        ciborium::into_writer(&param.encrypted_data, &mut buffer)?;

        let _ = sqlx::query::<Sqlite>(NOTE_PUT_V1)
            .bind(param.encrypted_data.get_owned_id())
            .bind(buffer)
            .execute(&self.conn)
            .await?;

        Ok(NotePutResultRepo {
            new_key: param.encrypted_data.get_owned_id(),
        })
    }

    async fn note_get(&self, param: NoteGetParamRepo) -> Result<NoteGetResultRepo, TreedomeError> {
        let get_query = generate_note_get_query_v1(param.ids.len())?;
        let mut query = sqlx::query_as::<Sqlite, DBV1>(&get_query);
        for id in param.ids {
            query = query.bind(id)
        }

        let result = query.fetch_all(&self.conn).await?.into_iter();
        let mut tried_result = Vec::with_capacity(result.len());
        for v in result {
            let encoded = ciborium::from_reader::<EncryptedModel<NoteModel>, _>(v.data.as_slice())?;
            tried_result.push(encoded);
        }

        Ok(NoteGetResultRepo {
            model: tried_result,
        })
    }

    async fn note_delete(
        &self,
        param: NoteDeleteParamRepo,
    ) -> Result<NoteDeleteResultRepo, TreedomeError> {
        let query = sqlx::query::<Sqlite>(NOTE_DELETE_V1).bind(param.id);
        let res = query.execute(&self.conn).await?;

        Ok(NoteDeleteResultRepo {
            existed: res.rows_affected() > 0,
        })
    }

    async fn get_first_note(
        &self,
        _: NoteGetFirstParamRepo,
    ) -> Result<NoteGetFirstResultRepo, TreedomeError> {
        let maybe_one = sqlx::query_as::<Sqlite, DBV1>(NOTE_GET_ALL_V1)
            .fetch_optional(&self.conn)
            .await?;

        if let Some(v) = maybe_one {
            return Ok(NoteGetFirstResultRepo {
                model: Some((
                    v.id,
                    ciborium::from_reader::<EncryptedModel<NoteModel>, _>(v.data.as_slice())?,
                )),
            });
        }

        Ok(NoteGetFirstResultRepo { model: None })
    }

    async fn note_duplicate(
        &self,
        mut param: NoteDuplicateParamRepo,
    ) -> Result<NoteDuplicateResultRepo, TreedomeError> {
        param.ids.sort();
        param.ids.dedup();

        let mut old_to_new: Vec<DuplicatedNote> = vec![];
        let mut tx = self.conn.begin().await?;
        for old_key in param.ids {
            // fetch from db
            let from_db = sqlx::query_as::<_, DBV1>(NOTE_GET_ONE_V1)
                .bind(&old_key)
                .fetch_optional(&mut *tx)
                .await?;

            if let Some(raw) = from_db {
                let secret = param.secret.clone();
                let (new_id, new_data) = spawn_blocking(move || -> Result<_, TreedomeError> {
                    let encrypted_note =
                        ciborium::from_reader::<EncryptedModel<NoteModel>, _>(raw.data.as_slice())?;

                    // still uses old nonce
                    let decrypted_note = encrypted_note.into_decrypt(&secret)?;

                    // reciphering with new nonce and id
                    let model = DecryptedModel::new(
                        None,
                        NoteModel {
                            title: decrypted_note.data.title,
                            content: decrypted_note.data.content,
                            text_only: decrypted_note.data.text_only,
                        },
                    )
                    .into_encrypt(&secret)?;

                    let new_id = model.get_owned_id();
                    let mut buffer = vec![];
                    ciborium::into_writer(&model, &mut buffer)?;
                    Ok((new_id, buffer))
                })
                .await??;

                let _ = sqlx::query::<Sqlite>(NOTE_PUT_V1)
                    .bind(&new_id)
                    .bind(new_data)
                    .execute(&mut *tx)
                    .await?;

                old_to_new.push(DuplicatedNote {
                    from: old_key,
                    to: new_id,
                })
            }
        }
        tx.commit().await?;

        Ok(NoteDuplicateResultRepo { old_to_new })
    }
}
