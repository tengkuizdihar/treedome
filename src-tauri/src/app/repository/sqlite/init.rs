use chrono::Local;

use crate::{
    app::{
        constant::{WELCOMING_NOTE_CONTENT, WELCOMING_NOTE_TEXT_ONLY, WELCOMING_NOTE_TITLE},
        crypto::v1::Cipher,
        db::{DecryptedModel, MetaModel, NoteModel, PlaintextModel},
        repository::{
            model::{MetaGetParamRepo, MetaPutParamRepo, NoteGetFirstParamRepo, NotePutParamRepo},
            sqlite::DBV1,
            MetaRepository, NoteRepository,
        },
    },
    error::{CryptoError, TreedomeError},
    tree::NoteTreeData,
};

use super::{MetaRepositorySqlite, NoteRepositorySqlite};

/// Table definition should always be the same across a single version.
/// There's no migration in minor version because everything is stored in Encrypted BLOB anyway.
const NOTE_TABLE_DEFINITION_V1: &str = "
    CREATE TABLE IF NOT EXISTS note (
        id    TEXT PRIMARY KEY,
        data  BLOB
    )
";

/// Table definition should always be the same across a single version.
/// There's no migration in minor version because everything is stored in Encrypted BLOB anyway.
const META_TABLE_DEFINITION_V1: &str = "
    CREATE TABLE IF NOT EXISTS meta (
        id    TEXT PRIMARY KEY,
        data  BLOB
    )
";

/// Table definition should always be the same across a single version.
/// There's no migration in minor version because everything is stored in Encrypted BLOB anyway.
const PLAINTEXT_ONLY_ID: &str = "MAIN";
const PLAINTEXT_TABLE_DEFINITION_V1: &str = "
    CREATE TABLE IF NOT EXISTS plaintext (
        id    TEXT PRIMARY KEY,
        data  BLOB
    )
";

const GET_PLAINTEXT_V1: &str = "SELECT id, data FROM plaintext where id = ?";
const SET_PLAINTEXT_V1: &str = "INSERT OR REPLACE INTO plaintext(id, data) VALUES (?, ?)";

// TODO: refactor to document repository
pub async fn init_table_sqlite(conn: &sqlx::Pool<sqlx::Sqlite>) -> Result<(), TreedomeError> {
    let mut tx = conn.begin().await?;

    sqlx::query(NOTE_TABLE_DEFINITION_V1)
        .execute(&mut *tx)
        .await?;

    sqlx::query(META_TABLE_DEFINITION_V1)
        .execute(&mut *tx)
        .await?;

    sqlx::query(PLAINTEXT_TABLE_DEFINITION_V1)
        .execute(&mut *tx)
        .await?;

    tx.commit().await?;

    Ok(())
}

// TODO: refactor to document repository
pub async fn init_plaintext_model_sqlite(
    conn: &sqlx::Pool<sqlx::Sqlite>,
) -> Result<PlaintextModel, TreedomeError> {
    let mut tx = conn.begin().await?;

    let data = sqlx::query_as::<_, DBV1>(GET_PLAINTEXT_V1)
        .bind(PLAINTEXT_ONLY_ID)
        .fetch_optional(&mut *tx)
        .await?
        .map(|v| ciborium::from_reader::<PlaintextModel, _>(v.data.as_slice()).ok())
        .flatten()
        .unwrap_or_default();

    let mut serialized = vec![];
    let _ = ciborium::into_writer(&data, &mut serialized)?;

    sqlx::query(SET_PLAINTEXT_V1)
        .bind(PLAINTEXT_ONLY_ID)
        .bind(serialized)
        .execute(&mut *tx)
        .await?;

    tx.commit().await?;

    Ok(data)
}

// TODO: refactor to meta repository
pub async fn init_meta_model_sqlite(
    meta_repository: &MetaRepositorySqlite,
    secret: &Cipher,
    first_note_id: &str,
) -> Result<MetaModel, TreedomeError> {
    let meta_model = meta_repository.meta_get(MetaGetParamRepo {}).await?.model;

    let init_meta_model = match meta_model {
        Some(v) => v.into_decrypt(secret)?.data,
        None => {
            let unencrypted_model = MetaModel {
                tree: vec![NoteTreeData {
                    id: first_note_id.to_owned(),
                    parent: "0".to_owned(),
                    droppable: true,
                    text: WELCOMING_NOTE_TITLE.to_string(),
                    data: None,
                }],
                opened_notes: vec![],
                last_opened_note_id: first_note_id.to_owned(),
                preference: None,
            };

            let model = DecryptedModel::new(None, unencrypted_model.clone())
                .into_encrypt(secret)
                .map_err(|e| CryptoError::from(e))?;

            meta_repository
                .meta_put(MetaPutParamRepo {
                    encrypted_data: model,
                })
                .await?;

            unencrypted_model
        }
    };

    Ok(init_meta_model)
}

// TODO: refactor to note repository
pub async fn init_first_note_sqlite(
    note_repository: &NoteRepositorySqlite,
    secret: &Cipher,
) -> Result<String, TreedomeError> {
    let first_note_model = note_repository
        .get_first_note(NoteGetFirstParamRepo {})
        .await?;

    let first_node_id = match first_note_model.model {
        Some((key, value)) => {
            // Try to decrypt this note, check if the provided password is correct
            let _ = value.into_decrypt(secret).map_err(|_| {
                TreedomeError::FailedOpeningDocument(
                    "failed decrypting file, wrong password maybe?".to_string(),
                )
            })?;
            key
        }
        None => {
            let model = DecryptedModel::new(
                None,
                NoteModel {
                    title: WELCOMING_NOTE_TITLE.to_string(),
                    content: WELCOMING_NOTE_CONTENT.to_string(),
                    text_only: WELCOMING_NOTE_TEXT_ONLY.to_string(),
                    tags: Some(vec![String::from("welcome")]),
                    create_at: Some(Local::now().to_rfc3339()),
                    update_at: Some(Local::now().to_rfc3339()),
                },
            )
            .into_encrypt(secret)
            .map_err(|e| CryptoError::from(e))?;

            note_repository
                .note_put(NotePutParamRepo {
                    encrypted_data: model,
                })
                .await?
                .new_key
        }
    };

    Ok(first_node_id)
}
