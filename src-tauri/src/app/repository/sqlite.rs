pub mod document;
pub mod init;
pub mod meta;
pub mod note;

pub type TreedomeSqliteConnection = sqlx::Pool<sqlx::Sqlite>;

#[derive(Clone)]
pub struct NoteRepositorySqlite {
    /// For the uninitiated, this is why it's structured like this.
    /// - Arc<T> because we want to Send this value to another thread via spawn_blocking
    /// - we are in "async world", so spawn_blocking is necessary to unload the work to another thread pool
    conn: TreedomeSqliteConnection,
}

#[derive(Clone)]
pub struct MetaRepositorySqlite {
    /// For the uninitiated, this is why it's structured like this.
    /// - Arc<T> because we want to Send this value to another thread via spawn_blocking
    /// - we are in "async world", so spawn_blocking is necessary to unload the work to another thread pool
    conn: TreedomeSqliteConnection,
}

#[derive(Clone)]
pub struct DocumentRepositorySqlite {
    /// For the uninitiated, this is why it's structured like this.
    /// - Arc<T> because we want to Send this value to another thread via spawn_blocking
    /// - we are in "async world", so spawn_blocking is necessary to unload the work to another thread pool
    conn: TreedomeSqliteConnection,
}

/// This should be private, only used in sqlite repository only
#[derive(sqlx::FromRow)]
struct DBV1 {
    #[sqlx(default)]
    id: String,
    #[sqlx(default)]
    data: Vec<u8>,
}
