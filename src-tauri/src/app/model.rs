#[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq)]
pub struct FoundRange {
    pub start: i64,
    pub end: i64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct SearchResult {
    pub note_id: String,
    pub title: String,
    pub ranges: Vec<FoundRange>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct DuplicatedNote {
    pub from: String,
    pub to: String,
}
