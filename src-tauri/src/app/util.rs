use std::path::Path;

use crate::error::{SecretValidationError, TreedomeError};

use super::constant::{TREEDOME_BACKUP_EXTENSION, TREEDOME_EXTENSION};

pub fn check_password_quality(unchecked_password: &str) -> Result<&str, SecretValidationError> {
    // validate password entropy
    let password_entropy = match zxcvbn::zxcvbn(unchecked_password, &[]) {
        Ok(v) => v,
        Err(e) => match e {
            zxcvbn::ZxcvbnError::BlankPassword => {
                return Err(SecretValidationError::ValidationError(
                    "password is blank or empty".to_owned(),
                ))
            }
            zxcvbn::ZxcvbnError::DurationOutOfRange => {
                return Err(SecretValidationError::ValidationError(
                    "`duration` is giving bad time value, please create an issue if this happens"
                        .to_owned(),
                ))
            }
        },
    };

    if password_entropy.score() < 3 {
        let feedback_struct = password_entropy.feedback().as_ref();

        let feedback_text = feedback_struct
            .map(|f| f.suggestions().first().map(|s| s.to_string()))
            .flatten();

        let warning_text = feedback_struct
            .map(|f| f.warning().map(|w| w.to_string()))
            .flatten();

        let final_text = feedback_text.or(warning_text).unwrap_or(String::from(
            "No suggestion for this password, please make another one",
        ));

        return Err(SecretValidationError::ValidationError(final_text));
    }

    Ok(unchecked_password)
}

#[cfg(test)]
pub fn speed_test<T, F>(title: &str, f: F) -> T
where
    F: FnOnce() -> T,
{
    let start = std::time::Instant::now();
    let result = f();
    println!("{} finished in {:?}", title, start.elapsed());
    result
}

pub fn backup_file_if_exist<T: AsRef<Path>>(path: &T) -> Result<(), TreedomeError> {
    let path_ref = path.as_ref();
    let is_path_exists = path_ref.exists();
    let is_path_file = path_ref.is_file();

    if is_path_exists && !is_path_file {
        return Err(TreedomeError::FailedOpeningDocument(
            "path should be pointing to a file".to_string(),
        ));
    }

    let corrected_path = path_ref.with_extension(TREEDOME_EXTENSION);
    if is_path_exists {
        let path_backup = path_ref.with_extension(TREEDOME_BACKUP_EXTENSION);
        std::fs::copy(corrected_path, path_backup).map_err(|_| {
            TreedomeError::FailedOpeningDocument("failed backing up note".to_string())
        })?;
    }

    Ok(())
}

/// WARNING: VERY DANGEROUS TOXIC
/// This command will directly execute the command given by the user.
/// It's very convenient tho.
pub fn spawn_shell<T>(command_template: &str, open_document: T) -> Result<(), String>
where
    T: AsRef<Path>,
{
    let directory_path = if open_document.as_ref().is_file() {
        open_document
            .as_ref()
            .parent()
            .map(|v| v.to_owned())
            .unwrap_or_default()
    } else {
        open_document.as_ref().to_owned()
    };

    #[derive(serde::Serialize)]
    struct Context {
        directory: String,
    }

    let mut tt = tinytemplate::TinyTemplate::new();
    tt.add_template("command", command_template)
        .map_err(|e| e.to_string())?;

    let context = Context {
        directory: directory_path
            .to_str()
            .map(|s| s.to_string())
            .unwrap_or_default(),
    };

    let rendered = tt.render("command", &context).map_err(|e| e.to_string())?;

    let _ = execute::command(rendered)
        .spawn()
        .map_err(|e| e.to_string());

    Ok(())
}
