use async_trait::async_trait;
use std::sync::Arc;

use crate::{
    app::{
        crypto::v1::Cipher,
        repository::{
            model::{NoteGetParamRepo, NotePutParamRepo},
            NoteRepository,
        },
    },
    error::TreedomeError,
};

use super::{
    model::{NoteRepairParamDatalogic, NoteRepairResultDatalogic},
    NoteDatalogic,
};

pub struct NoteDatalogicImpl {
    note_repository: Arc<Box<dyn NoteRepository + Send + Sync>>,
    secret: Arc<Cipher>,
}

impl NoteDatalogicImpl {
    pub fn new(
        note_repository: Arc<Box<dyn NoteRepository + Send + Sync>>,
        secret: Arc<Cipher>,
    ) -> Self {
        NoteDatalogicImpl {
            note_repository,
            secret,
        }
    }
}

#[async_trait]
impl NoteDatalogic for NoteDatalogicImpl {
    async fn note_repair(
        &self,
        param: NoteRepairParamDatalogic,
    ) -> Result<NoteRepairResultDatalogic, TreedomeError> {
        if !param.note_ids.is_empty() {
            let notes = self
                .note_repository
                .note_get(NoteGetParamRepo {
                    ids: param.note_ids,
                })
                .await?
                .model;

            for note in notes {
                let mut decrypted_note = note.into_decrypt(&self.secret)?;

                decrypted_note.data.title =
                    crate::app::constant::TREEDOME_TITLE_DEFAULT.to_string();

                let encrypted = decrypted_note.into_encrypt(&self.secret)?;

                let _ = self
                    .note_repository
                    .note_put(NotePutParamRepo {
                        encrypted_data: encrypted,
                    })
                    .await?;
            }
        }

        Ok(NoteRepairResultDatalogic {})
    }
}
