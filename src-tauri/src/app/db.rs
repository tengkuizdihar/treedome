//! Welcome to the ***meat*** of Treedome! This module is about storing and retrieving data from the disk.
//! As a developer, there are some precaution that you need to know.
//!
//! First is about our usage of structs. Our structs are serialized (from memory to binary that can be stored in disk) and deserialized (from disk to memory) using a library called [ciborium](https://github.com/enarx/ciborium).
//! This library implements the format of [CBOR](https://cbor.io/impls.html), a self describing message format. Think of it like [JSON](https://json.org/example.html) but binary.
//! If a developer wants to add another field to the struct, they must use `Option<T>` to make sure of backward compatibilities.
//!
//! Second, this module is tightly coupled with the [`super::crypto`] module because encrypted model are stored in [`EncryptedModel<T>`] which depends on [`EncryptedData<T>`].

use std::{fmt::Debug, ops::Deref};

use serde::{de::DeserializeOwned, Deserialize, Serialize};
use uuid::Uuid;

use crate::{
    error::{CryptoError, SecretValidationError},
    tree::{OpenedNoteTree, RootNoteTreeData},
};

use super::{
    crypto::v1::{Cipher, EncryptedData, CURRENT_VERSION_TAG},
    util::check_password_quality,
};

/// Can only be created through DecryptedModel::New()
#[derive(Serialize, Deserialize)]
pub struct EncryptedModel<T> {
    id: String,
    encrypted_data: EncryptedData<T>,
}

/// We are using CBOR, so don't just willy-nilly change the order of the structs!
/// New field should be inside Option<T> to avoid breaking compatibilities with older notes.
#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct NoteModel {
    pub title: String,
    pub content: String,

    /// used for searching by text
    pub text_only: String,

    /// tags is an easy way to search up your notes
    pub tags: Option<Vec<String>>,

    /// uses rfc3339 such as 1996-12-19T16:39:57-08:00
    pub create_at: Option<String>,

    /// uses rfc3339 such as 1996-12-19T16:39:57-08:00
    pub update_at: Option<String>,
}

/// We are using CBOR, so don't just willy-nilly change the order of the structs!
#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct PlaintextModel {
    pub version: String,
    pub salt: Vec<u8>,
}

impl Default for PlaintextModel {
    fn default() -> Self {
        Self {
            version: CURRENT_VERSION_TAG.to_string(),
            salt: Cipher::new_salt().to_vec(),
        }
    }
}

/// We are using CBOR, so don't just willy-nilly change the order of the structs!
#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct MetaModel {
    pub tree: RootNoteTreeData,
    pub opened_notes: OpenedNoteTree,
    pub last_opened_note_id: String,
    pub preference: Option<PreferenceModel>,
}

/// We are using CBOR, so don't just willy-nilly change the order of the structs!
#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct PreferenceModel {
    pub open_timeout_ms: Option<i64>,
    pub enable_toolbar: Option<bool>,
    pub enable_floating_toolbar: Option<bool>,
    pub document_name: Option<String>,
}

impl<T> Debug for EncryptedModel<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("EncryptedModel").finish()
    }
}

impl<T> EncryptedModel<T> {
    pub fn get_owned_id(&self) -> String {
        self.id.clone()
    }

    ///  Turn [`EncryptedModel<T>`], usually from database, then transform into a ready to use [`DecryptedModel<T>`]
    ///
    ///  TODO: right now there's unnecessary cloning everywhere:
    ///     - does DeserializeOwned means code smell? is it slow to use this?
    ///     - this is not async, so caller need to offload to spawn_blocking
    pub fn into_decrypt(self, cipher: &Cipher) -> Result<DecryptedModel<T>, CryptoError>
    where
        T: DeserializeOwned,
    {
        // deserialized into T
        let decrypt = cipher.decrypt(&self.encrypted_data)?;

        Ok(DecryptedModel {
            id: self.id,
            data: decrypt,
        })
    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct DecryptedModel<T> {
    /// Value should be insignificant, random, and fairly unique. Using uuid v4 by default.
    ///
    /// TODO: collision might happen, very rarely though.
    pub id: String,
    pub data: T,
}

impl<T> DecryptedModel<T>
where
    T: Serialize,
{
    /// Use this to create an unencrypted data wrapper, will prepare everything needed for encryption and db.
    /// Please only use this to create NEW document, old one should be retrieved from [`EncryptedModel::into_decrypt`].
    /// This means there will be no chance of creating a data that's not compatible with EncryptedModel.
    pub fn new(id: Option<String>, unencrypted_data: T) -> DecryptedModel<T> {
        let id = id.unwrap_or_else(|| Uuid::new_v4().simple().to_string());

        DecryptedModel {
            id,
            data: unencrypted_data,
        }
    }

    pub fn into_encrypt(&self, cipher: &Cipher) -> Result<EncryptedModel<T>, CryptoError> {
        let encrypted_data = cipher.encrypt(&self.data)?;
        Ok(EncryptedModel {
            id: self.id.clone(),
            encrypted_data,
        })
    }
}

#[derive(Debug, PartialEq)]
pub struct ValidatedSecret(SecretData);
pub type SecretData = String;

impl ValidatedSecret {
    pub fn new_from_password(unchecked_password: &str) -> Result<Self, SecretValidationError> {
        let valid_password = check_password_quality(unchecked_password)?.to_owned();
        Ok(ValidatedSecret(valid_password))
    }

    pub fn new_from_unchecked_password(unchecked_password: &str) -> Self {
        ValidatedSecret(unchecked_password.to_owned())
    }
}

impl Deref for ValidatedSecret {
    type Target = SecretData;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_different_struct_serde() {
        #[derive(Default, Serialize, Deserialize)]
        pub struct OldStruct {
            pub one: String,
            pub two: String,
        }

        #[derive(Default, Serialize, Deserialize)]
        pub struct NewStruct {
            pub one: String,
            pub two: String,

            // new fields should be appended with Option<T>
            pub three: Option<String>,
        }

        let original_data = OldStruct {
            one: "one".to_string(),
            two: "two".to_string(),
        };

        // testing for CBOR
        let mut buffer = vec![];
        let _ = ciborium::into_writer(&original_data, &mut buffer).unwrap();
        let deserialized = ciborium::from_reader::<NewStruct, _>(buffer.as_slice()).unwrap();

        // assertion
        assert_eq!(deserialized.one, original_data.one);
        assert_eq!(deserialized.two, original_data.two);
    }
}
