use crate::{
    app::{
        db::{DecryptedModel, MetaModel, NoteModel},
        model::{DuplicatedNote, SearchResult},
    },
    tree::{OpenedNoteTree, RootNoteTreeData},
};

#[derive(serde::Serialize, serde::Deserialize)]
pub struct DocumentOpenParamCommand {
    pub path: String,
    pub password: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct DocumentOpenResultCommand {
    pub meta_model: MetaModel,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NotePutParamCommand {
    pub id: Option<String>,
    pub title: String,
    pub content: String,
    pub text_only: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NotePutResultCommand {
    pub id: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NoteGetParamCommand {
    pub ids: Vec<String>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NoteGetResultCommand {
    pub result: Vec<DecryptedModel<NoteModel>>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NoteSearchParamCommand {
    pub query: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NoteSearchResultCommand {
    pub result: Vec<SearchResult>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NoteDeleteParamCommand {
    pub note_ids: Vec<String>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NoteDeleteResultCommand {}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct PasswordQualityCheckParamCommand {
    pub password: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct PasswordQualityCheckResultCommand {}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct MetaPutParamCommand {
    pub tree: RootNoteTreeData,
    pub opened_notes: OpenedNoteTree,
    pub last_opened_note_id: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct MetaPutResultCommand {}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct MetaGetParamCommand {}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct MetaGetResultCommand {
    pub result: Option<DecryptedModel<MetaModel>>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct DocumentCompactParamCommand {}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct DocumentCompactResultCommand {}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct DocumentChangePasswordExportParamCommand {
    pub new_password: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct DocumentChangePasswordExportResultCommand {}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct SpawnShellInDocumentDirectoryParamCommand {
    pub command: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct SpawnShellInDocumentDirectoryResultCommand {}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NoteDuplicateParamCommand {
    pub ids: Vec<String>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NoteDuplicateResultCommand {
    pub result: Vec<DuplicatedNote>,
}
