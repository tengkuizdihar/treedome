pub type RootNoteTreeData = Vec<NoteTreeData>;
pub type OpenedNoteTree = Vec<String>;

#[derive(Debug, PartialEq, Clone, serde::Serialize, serde::Deserialize)]
pub struct NoteTreeData {
    pub id: String,
    pub parent: String,
    pub droppable: bool,
    pub text: String,
}
