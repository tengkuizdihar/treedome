use vergen::EmitBuilder;

fn main() {
    EmitBuilder::builder()
        .git_describe(true, true, None) // will give the output of `git describe --dirty` to VERGEN_GIT_DESCRIBE
        .emit()
        .unwrap();
    tauri_build::build();
}
