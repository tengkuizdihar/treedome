import { Button, ComboboxItem, Menu, Select } from "@mantine/core"
import { EmojiEmotions } from "@mui/icons-material"
import { useEffect, useState } from "react"
import { useDebouncedValue } from "@mantine/hooks"
import { useAtom } from "jotai"
import { EmojiSearchFuse } from "../../util/emoji-picker"

export interface EmojiPickerButtonProps {
  onChangeEmoji: (value: string) => void
}
export function EmojiPickerButton(props: EmojiPickerButtonProps) {
  const [fuseSearch] = useAtom(EmojiSearchFuse)
  const [query, setQuery] = useState("")
  const [debouncedQuery] = useDebouncedValue(query, 200)
  const [data, setData] = useState<ComboboxItem[]>([])

  useEffect(() => {
    if (debouncedQuery !== "") {
      let result = fuseSearch
        .search(debouncedQuery, {
          limit: 10,
        })
        .map((v) => ({
          label: `${v.item.emoji} - ${v.item.label}`,
          value: v.item.emoji,
        }))

      setData(result)
    }
  }, [debouncedQuery, fuseSearch])

  return (
    <>
      <Menu
        transitionProps={{ transition: "pop" }}
        position="bottom-end"
        withinPortal
      >
        <Menu.Target>
          <Button>
            <EmojiEmotions />
          </Button>
        </Menu.Target>

        <Menu.Dropdown>
          <Select
            placeholder="search your emoji"
            onSearchChange={(query) => setQuery(query ?? "")}
            data={data}
            filter={(_input) => {
              return data // override data being displayed with fuse
            }}
            searchable
            onChange={(value) => {
              if (value !== null && props.onChangeEmoji !== undefined) {
                props.onChangeEmoji(value)
              }
            }}
          />
        </Menu.Dropdown>
      </Menu>
    </>
  )
}
