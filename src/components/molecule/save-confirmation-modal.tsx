import { Modal, Group, Button, Center, Space, FocusTrap } from "@mantine/core"
import { INSTANT_TRANSITION_MODAL } from "../../util/modal"

export interface SaveConfirmationModalProps {
  opened: boolean
  close: () => void
}

export function SaveConfirmationModal(props: SaveConfirmationModalProps) {
  return (
    <Modal
      shadow="0" // linux webview went to a crawl when scrolling if shadow is on
      {...INSTANT_TRANSITION_MODAL}
      size="md"
      opened={props.opened}
      onClose={props.close}
      centered
      withCloseButton={false}
      trapFocus={true}
    >
      please save your note before switching
      <Space h="md" />
      <Group justify="right">
        <FocusTrap active={true}>
          <Button onClick={props.close}>close</Button>
        </FocusTrap>
      </Group>
    </Modal>
  )
}
