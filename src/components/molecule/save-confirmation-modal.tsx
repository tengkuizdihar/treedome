import { Modal, Group, Button, Center, Space, FocusTrap } from "@mantine/core"

export interface SaveConfirmationModalProps {
  opened: boolean
  close: () => void
}

export function SaveConfirmationModal(props: SaveConfirmationModalProps) {
  return (
    <Modal
      size="md"
      opened={props.opened}
      onClose={props.close}
      centered
      withCloseButton={false}
      trapFocus={true}
    >
      please save your note before switching
      <Space h="md" />
      <Group position="right">
        <FocusTrap active={true}>
          <Button onClick={props.close}>close</Button>
        </FocusTrap>
      </Group>
    </Modal>
  )
}
