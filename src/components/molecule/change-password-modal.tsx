import {
  Button,
  Loader,
  Modal,
  ModalProps,
  Overlay,
  PasswordInput,
  Stack,
  Title,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { useDebouncedValue } from "@mantine/hooks"
import {
  document_change_password_export,
  password_quality_check,
} from "../../util/treedome-app"
import { useEffect, useState } from "react"
import { notifications } from "@mantine/notifications"
import { INSTANT_TRANSITION_MODAL } from "../../util/modal"

export interface ChangePasswordModalProps extends ModalProps {}

export const ChangePasswordModal = (props: ChangePasswordModalProps) => {
  const [isLoading, setIsLoading] = useState(false)
  const form = useForm({
    initialValues: {
      password: "",
      confirmPassword: "",
    },
  })
  const [debouncedPassword] = useDebouncedValue(form.values.password, 200)

  useEffect(() => {
    password_quality_check({
      password: form.values.password,
    }).catch((e) => {
      form.setFieldError("password", String(e))
    })
  }, [debouncedPassword])

  return (
    <Modal
      shadow="0" // linux webview went to a crawl when scrolling if shadow is on
      {...props}
      {...INSTANT_TRANSITION_MODAL}
      withCloseButton={false}
      styles={{ content: { border: "1px solid white" } }}
      size="lg"
    >
      {isLoading && (
        <Overlay blur={15} center>
          <Stack align="center" gap="sm" justify="center">
            <Loader color="indigo" size="xl" />
            <h3>Exporting document with new password...</h3>
          </Stack>
        </Overlay>
      )}
      <Stack>
        <Title ta="center" order={2}>
          Change Password Modal
        </Title>
        <form
          onSubmit={form.onSubmit((values) => {
            let passwordIsDifferent = values.password !== values.confirmPassword

            if (passwordIsDifferent) {
              form.setFieldError("confirmPassword", "Passwords do not match")
            }

            if (!passwordIsDifferent) {
              setIsLoading(true)
              document_change_password_export({
                new_password: values.password,
              })
                .then(() => {
                  notifications.show({
                    message:
                      "successfully exporting document with new password in the same directory",
                    autoClose: false,
                    withCloseButton: true,
                  })

                  props.onClose()
                  setIsLoading(false)
                })
                .catch((reason) => {
                  notifications.show({
                    message: String(reason),
                    color: "red.7",
                    autoClose: false,
                    withCloseButton: true,
                  })

                  props.onClose()
                  setIsLoading(false)
                })
            }
          })}
        >
          <PasswordInput
            label="New Password"
            placeholder="Long but easy to remember"
            required
            mt="md"
            {...form.getInputProps("password")}
          />
          <PasswordInput
            label="Repeat Your New Password"
            placeholder="Long but easy to remember"
            required
            mt="md"
            {...form.getInputProps("confirmPassword")}
          />
          <Button type="submit" fullWidth mt="xl">
            Open Document
          </Button>
        </form>
      </Stack>
    </Modal>
  )
}
