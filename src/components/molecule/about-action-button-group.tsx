import { Group, Tooltip, ActionIcon } from "@mantine/core"
import { open } from "@tauri-apps/plugin-shell"
import {
  IconBrandGit,
  IconMessage,
  IconBug,
  IconCopyleft,
} from "@tabler/icons-react"
import {
  GIT_URL,
  CONTACT_URL,
  GIT_ISSUE_URL,
  LICENSE_URL,
} from "../../util/treedome-app"

export const AboutActionButtonGroup = () => (
  <Group justify="center">
    <Tooltip position="bottom" label="Source Code and Releases" offset={10}>
      <ActionIcon
        size="xl"
        onClick={() => {
          open(GIT_URL).catch((e: any) => console.error(e))
        }}
      >
        <IconBrandGit />
      </ActionIcon>
    </Tooltip>

    <Tooltip
      position="bottom"
      label="Community"
      offset={10}
      onClick={() => {
        open(CONTACT_URL).catch((e: any) => console.error(e))
      }}
    >
      <ActionIcon size="xl">
        <IconMessage />
      </ActionIcon>
    </Tooltip>

    <Tooltip
      position="bottom"
      label="Report Bugs and Propose Features"
      offset={10}
      onClick={() => {
        open(GIT_ISSUE_URL).catch((e: any) => console.error(e))
      }}
    >
      <ActionIcon size="xl">
        <IconBug />
      </ActionIcon>
    </Tooltip>

    <Tooltip
      position="bottom"
      label="License"
      offset={10}
      onClick={() => {
        open(LICENSE_URL).catch((e: any) => console.error(e))
      }}
    >
      <ActionIcon size="xl">
        <IconCopyleft />
      </ActionIcon>
    </Tooltip>
  </Group>
)
