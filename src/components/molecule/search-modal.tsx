import {
  Card,
  Container,
  Grid,
  Group,
  List,
  Modal,
  ModalProps,
  Pagination,
  Pill,
  Stack,
  Text,
  TextInput,
  ThemeIcon,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { Search } from "@mui/icons-material"
import { useAtom } from "jotai"
import { ceil, map, sortBy } from "lodash"
import { useEffect, useState } from "react"
import {
  searchingState,
  SearchingStateEnum,
  searchResult,
} from "../../util/note-search"
import { note_search } from "../../util/treedome-app"
import { INSTANT_TRANSITION_MODAL } from "../../util/modal"
import { IconChevronRight } from "@tabler/icons-react"
import { currentTreeData } from "../../util/markdown-editor"
import { BreadcrumbNotePath } from "../atom/breadcrumb-note-path"
import { buildNoteParentListBreadcrumb } from "../atom/note-tree"

const PAGE_SIZE = 10
export interface SearchModalProps extends ModalProps {
  redirectNote: (document_id: string, query?: string) => void
}
export interface CustomPagination {
  page: number
  total: number
}

export function SearchModal(props: SearchModalProps) {
  const [searchingStateData, setSearchingStateData] = useAtom(searchingState)
  const [searchResultData, setSearchResultData] = useAtom(searchResult)
  const [latestSearchQuery, setlatestSearchQuery] = useState("")
  const [pagination, setPagination] = useState<CustomPagination>({
    page: 0,
    total: 0,
  })

  const form = useForm({
    initialValues: {
      query: "",
    },

    validate: {
      query: (v) => {
        if (v.length === 0) {
          return "query must not be empty"
        }
        if (v.length < 2) {
          return "you need a longer query"
        }
      },
    },
  })

  const buildPlaceholderText = (query: string) => {
    if (query === "") {
      return "search..."
    }

    return `last searched: ${query}`
  }

  const isOpened =
    props.opened || searchingStateData === SearchingStateEnum.InProgress // keep it open when searching is in progress

  useEffect(() => {
    if (!isOpened) {
      if (form.values.query != "") {
        setlatestSearchQuery(form.values.query)
      }

      form.setFieldValue("query", "") // clear query when it's closed
    }
  }, [isOpened])

  const { redirectNote, ...rest } = props

  return (
    <Modal
      {...rest}
      {...INSTANT_TRANSITION_MODAL}
      opened={isOpened}
      withCloseButton={false}
      fullScreen
    >
      <Container h="100%">
        <Stack h="100%" justify="space-between">
          <Stack gap="xs">
            <form
              onSubmit={form.onSubmit((values) => {
                setSearchingStateData(SearchingStateEnum.InProgress)
                setSearchResultData([])
                setlatestSearchQuery(values.query)
                note_search({
                  query: values.query,
                }).then(({ result }) => {
                  setPagination({
                    page: 1,
                    total: ceil(result.length / PAGE_SIZE),
                  })
                  setSearchResultData(sortBy(result, ["note_id"]))
                  setSearchingStateData(SearchingStateEnum.Done)
                })
              })}
            >
              {/* SEARCH BAR */}
              <Grid gutter="xs">
                <Grid.Col span="auto">
                  <TextInput
                    leftSection={<Search />}
                    placeholder={buildPlaceholderText(latestSearchQuery)}
                    {...form.getInputProps("query")}
                  />
                </Grid.Col>
              </Grid>
            </form>

            {/* SEARCH RESULTS */}
            <Stack w="100%" gap="xs">
              {map(
                searchResultData.slice(
                  (pagination.page - 1) * PAGE_SIZE,
                  (pagination.page - 1) * PAGE_SIZE + PAGE_SIZE,
                ),
                (v, i) => (
                  <Card
                    shadow="0" // linux webview went to a crawl when scrolling if shadow is on
                    padding="sm"
                    key={i}
                  >
                    <Stack gap="xs">
                      <Grid justify="space-between" align="stretch">
                        <Grid.Col span="content">
                          <Text size="lg" fw="bold">
                            {`${v.title}`}
                          </Text>
                        </Grid.Col>
                        <Grid.Col span="content">
                          <ThemeIcon
                            onClick={() => {
                              redirectNote(v.note_id, latestSearchQuery)
                              props.onClose()
                            }}
                          >
                            <IconChevronRight size={18} stroke={1.5} />
                          </ThemeIcon>
                        </Grid.Col>
                      </Grid>

                      <List px="md">
                        {map(v.ranges, (range_found) => (
                          <List.Item>
                            <Text span>
                              {range_found.before}
                              <Text
                                span
                                bg="rgb(255, 255, 0)" // yellow
                              >
                                {range_found.current}
                              </Text>
                              {range_found.after}
                            </Text>
                          </List.Item>
                        ))}
                      </List>

                      <Group justify="flex-end">
                        {v.tags.length > 0 && (
                          <Pill.Group>
                            {v.tags.map((tagName, index) => (
                              <Pill key={index}>{tagName}</Pill>
                            ))}
                          </Pill.Group>
                        )}
                      </Group>
                    </Stack>
                  </Card>
                ),
              )}
            </Stack>

            {searchingStateData === SearchingStateEnum.Done &&
              searchResultData.length === 0 && (
                <Text c="dimmed" ta="center">
                  nothing was found...
                </Text>
              )}

            {searchingStateData === SearchingStateEnum.InProgress && (
              <Text c="dimmed" ta="center">
                searching...
              </Text>
            )}
          </Stack>

          <Pagination.Root
            total={pagination.total}
            value={pagination.page}
            onChange={(v) => {
              setPagination({
                page: v,
                total: pagination.total,
              })
            }}
            style={{
              paddingBottom: "2em",
            }}
          >
            <Group gap={5} justify="center">
              <Pagination.First />
              <Pagination.Previous />
              <Pagination.Items />
              <Pagination.Next />
              <Pagination.Last />
            </Group>
          </Pagination.Root>
        </Stack>
      </Container>
    </Modal>
  )
}

const clamp = (num: number, min: number, max: number): number => {
  if (min > num) {
    return min
  }
  if (max < num) {
    return max
  }

  return num
}
