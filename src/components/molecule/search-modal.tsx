import {
  Button,
  Container,
  Grid,
  Modal,
  ModalProps,
  Pagination,
  Stack,
  Text,
  TextInput,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { Search } from "@mui/icons-material"
import { useAtom } from "jotai"
import { ceil, map, sortBy } from "lodash"
import { useState } from "react"
import { useLocation } from "wouter"
import {
  searchingState,
  SearchingStateEnum,
  searchResult,
} from "../../util/note-search"
import { NOTE_PATH } from "../../util/route-path"
import { note_search } from "../../util/treedome-app"

const PAGE_SIZE = 10
export interface SearchModalProps extends ModalProps {}
export interface CustomPagination {
  page: number
  total: number
}

export function SearchModal(props: SearchModalProps) {
  const [searchingStateData, setSearchingStateData] = useAtom(searchingState)
  const [searchResultData, setSearchResultData] = useAtom(searchResult)
  const [_, setLocation] = useLocation()
  const [pagination, setPagination] = useState<CustomPagination>({
    page: 0,
    total: 0,
  })

  const form = useForm({
    initialValues: {
      query: "",
    },

    validate: {
      query: (v) => {
        if (v.length === 0) {
          return "query must not be empty"
        }
        if (v.length < 3) {
          return "you need a longer query"
        }
      },
    },
  })

  return (
    <Modal
      {...props}
      opened={
        props.opened || searchingStateData === SearchingStateEnum.InProgress // keep it open when searching is in progress
      }
      withCloseButton={false}
      fullScreen
      styles={{
        body: {
          height: "100%",
        },
      }}
    >
      <Container h="100%">
        <Stack h="100%" justify="space-between">
          <Stack spacing="xs">
            <form
              onSubmit={form.onSubmit((values) => {
                setSearchingStateData(SearchingStateEnum.InProgress)
                setSearchResultData([])
                note_search({
                  query: values.query,
                }).then(({ result }) => {
                  setPagination({
                    page: 1,
                    total: ceil(result.length / PAGE_SIZE),
                  })
                  setSearchResultData(sortBy(result, ["note_id"]))
                  setSearchingStateData(SearchingStateEnum.Done)
                })
              })}
            >
              <Grid gutter="xs">
                <Grid.Col span="auto">
                  <TextInput
                    icon={<Search />}
                    placeholder="search..."
                    {...form.getInputProps("query")}
                  />
                </Grid.Col>
              </Grid>
            </form>
            {map(
              searchResultData.slice(
                (pagination.page - 1) * PAGE_SIZE,
                (pagination.page - 1) * PAGE_SIZE + PAGE_SIZE,
              ),
              (v) => (
                <>
                  <Button
                    fullWidth
                    variant="outline"
                    onClick={() => {
                      setLocation(
                        `${NOTE_PATH}/${v.note_id}/${form.values.query}`,
                      )
                      props.onClose()
                    }}
                  >
                    {`${v.title}, with ${v.ranges.length} occurences`}
                  </Button>
                </>
              ),
            )}

            {searchingStateData === SearchingStateEnum.Done &&
              searchResultData.length === 0 && (
                <Text c="dimmed" align="center">
                  nothing was found...
                </Text>
              )}

            {searchingStateData === SearchingStateEnum.InProgress && (
              <Text c="dimmed" align="center">
                searching...
              </Text>
            )}
          </Stack>

          <Pagination
            position="center"
            total={pagination.total}
            value={pagination.page}
            onChange={(v) => {
              setPagination({
                page: v,
                total: pagination.total,
              })
            }}
          />
        </Stack>
      </Container>
    </Modal>
  )
}
