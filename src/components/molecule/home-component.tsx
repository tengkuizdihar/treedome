import { Container, Space, Paper } from "@mantine/core"
import { DocumentPageButton, PageValues } from "../atom/document-page-button"
import { DocumentTitle } from "../atom/document-title"
import { AboutActionButtonGroup } from "./about-action-button-group"

export interface HomeComponentProps {
  currentPage: PageValues
  children: React.ReactNode
}

export const HomeComponent = (props: HomeComponentProps) => {
  return (
    <Container py={40} size={420}>
      <DocumentTitle />
      <Space h="md" />
      <DocumentPageButton currentPage={props.currentPage} />

      <Paper withBorder p={30} mt={30} radius="md">
        {props.children}
      </Paper>

      <Paper withBorder p={30} mt={30} radius="md">
        <AboutActionButtonGroup />
      </Paper>
    </Container>
  )
}
