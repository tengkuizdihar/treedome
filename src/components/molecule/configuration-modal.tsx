import {
  Divider,
  Grid,
  Modal,
  ModalProps,
  SimpleGrid,
  Stack,
  Title,
  Text,
  Button,
} from "@mantine/core"
import { document_compact } from "../../util/treedome-app"
import { notifications } from "@mantine/notifications"
import { ChangePasswordModal } from "./change-password-modal"
import { useDisclosure } from "@mantine/hooks"

export interface ConfigurationModalProps extends ModalProps {}

export const ConfigurationModal = (props: ConfigurationModalProps) => {
  const [
    changePasswordNoteOpened,
    { open: changePasswordNoteOpen, close: changePasswordNoteClose },
  ] = useDisclosure(false)

  const compact = () => {
    document_compact()
      .then(() => {
        notifications.show({
          message: "successfully compacting document",
          autoClose: true,
          withCloseButton: true,
        })
      })
      .catch((e) => {
        console.error(e)
        notifications.show({
          message: "failed compacting document",
          color: "red.7",
          autoClose: true,
          withCloseButton: true,
        })
      })
  }

  return (
    <Modal
      {...props}
      withCloseButton={false}
      styles={{ content: { border: "1px solid white" } }}
      size="lg"
    >
      <ChangePasswordModal
        opened={changePasswordNoteOpened}
        onClose={changePasswordNoteClose}
        centered
      />
      <Stack>
        <Title align="center" order={2}>
          General
        </Title>
        <Divider></Divider>
        <SimpleGrid cols={1}>
          {/* COMPACT */}
          <ConfigItem title="Optimize Document Size">
            <Button variant="outline" onClick={compact}>
              execute
            </Button>
          </ConfigItem>
          <ConfigItem title="Change Password and Export">
            <Button variant="outline" onClick={changePasswordNoteOpen}>
              execute
            </Button>
          </ConfigItem>
        </SimpleGrid>
      </Stack>
    </Modal>
  )
}

interface ConfigItemProps {
  title: string
}

const ConfigItem = (props: React.PropsWithChildren<ConfigItemProps>) => {
  return (
    <Grid justify="space-between" align="center">
      <Grid.Col span={6}>
        <Text>{props.title}</Text>
      </Grid.Col>
      <Grid.Col span="content">{props.children}</Grid.Col>
    </Grid>
  )
}
