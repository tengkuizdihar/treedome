import {
  Divider,
  Grid,
  Kbd,
  Modal,
  ModalProps,
  SimpleGrid,
  Stack,
  Text,
  Title,
} from "@mantine/core"
import { map, orderBy, values } from "lodash"
import {
  DEFAULT_ACTION_KEY_ITEMS,
  KeyItems,
  osType,
} from "../../util/markdown-editor"
import { useEffect, useState } from "react"
import { useAtom } from "jotai"

// Shortcuts according to https://tiptap.dev/api/keyboard-shortcuts
const TEXT_EDITOR_SHORTCUT: KeyItems[] = [
  {
    isModifier: true,
    isShift: false,
    key: "z",
    title: "Undo",
  },
  {
    isModifier: true,
    isShift: true,
    key: "z",
    title: "Redo",
  },
  {
    isModifier: true,
    isShift: false,
    key: "c",
    title: "Copy",
  },
  {
    isModifier: true,
    isShift: false,
    key: "v",
    title: "Paste",
  },
  {
    isModifier: true,
    isShift: true,
    key: "v",
    title: "Paste without Formatting",
  },
  {
    isModifier: true,
    isShift: false,
    key: "b",
    title: "Bold",
  },
  {
    isModifier: true,
    isShift: false,
    key: "i",
    title: "Italicize",
  },
  {
    isModifier: true,
    isShift: false,
    key: "u",
    title: "Underline",
  },
  {
    isModifier: true,
    isShift: false,
    key: "e",
    title: "Code",
  },
  {
    isModifier: true,
    isShift: true,
    key: "x",
    title: "Strikethrough",
  },
  {
    isModifier: true,
    isShift: true,
    key: "h",
    title: "Highlight",
  },
]

export interface HelpModalProps extends ModalProps {}
export function HelpModal(props: HelpModalProps) {
  const [shortcutInfoData, setShortcutInfoData] = useState<KeyItems[]>([])

  useEffect(() => {
    const sortedShortcuts = orderBy(values(DEFAULT_ACTION_KEY_ITEMS), "title")
    setShortcutInfoData(sortedShortcuts)
  }, [])

  return (
    <Modal
      {...props}
      withCloseButton={false}
      styles={{ content: { border: "1px solid white" } }}
      size="xl"
    >
      <Stack>
        <Title align="center" order={2}>
          General Shortcut
        </Title>
        <Divider></Divider>
        <SimpleGrid cols={2}>
          {map(shortcutInfoData, (data) => (
            <ShortcutInfo data={data} />
          ))}
        </SimpleGrid>
        <Divider></Divider>

        <Title align="center" order={2}>
          Text Editor Shortcut
        </Title>
        <Divider></Divider>
        <SimpleGrid cols={2}>
          {map(TEXT_EDITOR_SHORTCUT, (data) => (
            <ShortcutInfo data={data} />
          ))}
        </SimpleGrid>
        <Divider></Divider>
      </Stack>
    </Modal>
  )
}

export function ShortcutInfo(props: { data: KeyItems }) {
  const [os] = useAtom(osType)

  return (
    <Grid justify="space-between" align="center">
      <Grid.Col span={6}>
        <Text>{props.data.title}</Text>
      </Grid.Col>
      <Grid.Col span={6}>
        {props.data.isModifier && (
          <>
            {os !== "Darwin" && <Kbd mr="xs">Ctrl</Kbd>}
            {os === "Darwin" && <Kbd mr="xs">⌘</Kbd>}
          </>
        )}
        {props.data.isShift && (
          <>
            <Kbd mr="xs">Shift</Kbd>
          </>
        )}
        <Kbd>{props.data.key}</Kbd>
      </Grid.Col>
    </Grid>
  )
}
