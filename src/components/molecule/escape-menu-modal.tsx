import { Button, Divider, Modal, ModalProps, Stack, Title } from "@mantine/core"
import { notifications } from "@mantine/notifications"
import { exit } from "@tauri-apps/api/process"
import { document_close } from "../../util/treedome-app"
import { useLocation } from "wouter"
import { CREATE_DOCUMENT_PATH, OPEN_DOCUMENT_PATH } from "../../util/route-path"
import { useDisclosure } from "@mantine/hooks"
import { AboutModal } from "./about-modal"
import { ConfigurationModal } from "./configuration-modal"

export interface EscapeMenuModalProps extends ModalProps {
  searchToggle: () => void
  helpModalToggle: () => void
}

export function EscapeMenuModal(props: EscapeMenuModalProps) {
  const [, setLocation] = useLocation()
  const [aboutModalOpened, { open: aboutModalOpen, close: aboutModalClose }] =
    useDisclosure(false)
  const [
    configurationModalOpened,
    { open: configurationModalOpen, close: configurationModalClose },
  ] = useDisclosure(false)

  const { searchToggle, helpModalToggle, ...rest } = props

  return (
    <Modal {...rest} withCloseButton={false}>
      <AboutModal
        opened={aboutModalOpened}
        onClose={aboutModalClose}
        centered
      />
      <ConfigurationModal
        opened={configurationModalOpened}
        onClose={configurationModalClose}
        centered
      />

      <Stack spacing="xs">
        <Title order={2} align="center">
          treedome
        </Title>

        <Divider></Divider>

        <Button
          variant="outline"
          onClick={() => {
            document_close({})
              .then(() => {
                setLocation(OPEN_DOCUMENT_PATH)
              })
              .catch(() => {
                notifications.show({
                  message: "failed closing document",
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
          }}
        >
          Open New Document
        </Button>
        <Button
          variant="outline"
          onClick={() => {
            document_close({})
              .then(() => {
                setLocation(CREATE_DOCUMENT_PATH)
              })
              .catch(() => {
                notifications.show({
                  message: "failed closing document",
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
          }}
        >
          Create New Document
        </Button>
        <Button variant="outline" onClick={configurationModalOpen}>
          Configure
        </Button>
        <Button variant="outline" onClick={props.helpModalToggle}>
          Help
        </Button>
        <Button
          variant="outline"
          onClick={() => {
            props.onClose()
            props.searchToggle()
          }}
        >
          Search
        </Button>
        <Button variant="outline" onClick={aboutModalOpen}>
          About
        </Button>

        <Divider></Divider>

        <Button
          color="red.7"
          variant="outline"
          onClick={() => {
            document_close({})
              .then(() => {
                setLocation(OPEN_DOCUMENT_PATH)
              })
              .catch(() => {
                notifications.show({
                  message: "failed closing document",
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
          }}
        >
          Close Document
        </Button>
        <Button
          color="red.7"
          variant="outline"
          onClick={() => {
            exit(0)
              .then(() => {
                notifications.show({
                  message: "exiting application",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
              .catch((e) => {
                notifications.show({
                  message: `failed exiting application, ${e}`,
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
          }}
        >
          Exit
        </Button>
      </Stack>
    </Modal>
  )
}
