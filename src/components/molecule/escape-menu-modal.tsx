import { Button, Divider, Group, Modal, ModalProps, Stack } from "@mantine/core"
import { notifications } from "@mantine/notifications"
import { exit } from "@tauri-apps/plugin-process"
import { useLocation } from "wouter"
import { CREATE_DOCUMENT_PATH, OPEN_DOCUMENT_PATH } from "../../util/route-path"
import { useDisclosure } from "@mantine/hooks"
import { ConfigurationModal } from "./configuration-modal"
import { AboutActionButtonGroup } from "./about-action-button-group"
import { useAtom } from "jotai"
import {
  closeDocumentAndReset,
  preferenceAtom,
} from "../../util/frontend-command"
import { NodeModel } from "@minoru/react-dnd-treeview"
import { DeepNoteData } from "../../util/treedome-app"
import { SaveDocumentParams } from "../organism/document"
import { DocumentTitle } from "../atom/document-title"
import { INSTANT_TRANSITION_MODAL } from "../../util/modal"

export interface EscapeMenuModalProps extends ModalProps {
  searchToggle: () => void
  helpModalOpen: () => void
  tagsModalOpen: () => void
  saveDocument: (
    param?: SaveDocumentParams,
  ) => Promise<[NodeModel<DeepNoteData>[], string]>
}

export function EscapeMenuModal(props: EscapeMenuModalProps) {
  const [, setLocation] = useLocation()
  const [, setPreference] = useAtom(preferenceAtom)
  const [
    configurationModalOpened,
    { open: configurationModalOpen, close: configurationModalClose },
  ] = useDisclosure(false)

  const { searchToggle, helpModalOpen, saveDocument, tagsModalOpen, ...rest } =
    props

  const parentCloseButton = () => {
    props.onClose()
    configurationModalClose()
  }

  return (
    <Modal
      shadow="0" // linux webview went to a crawl when scrolling if shadow is on
      {...rest}
      {...INSTANT_TRANSITION_MODAL}
      keepMounted
      onClose={parentCloseButton}
      centered
      withCloseButton={false}
    >
      <ConfigurationModal
        {...INSTANT_TRANSITION_MODAL}
        keepMounted
        opened={configurationModalOpened}
        onClose={configurationModalClose}
        saveDocument={saveDocument}
        centered
      />

      <Stack gap="xs">
        <DocumentTitle smol />

        <Divider></Divider>

        <Button
          color="dark.0"
          onClick={() => {
            closeDocumentAndReset(setPreference)
              .then(() => {
                setLocation(OPEN_DOCUMENT_PATH)
              })
              .catch(() => {
                notifications.show({
                  message: "failed closing document",
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
          }}
        >
          Open New Document
        </Button>
        <Button
          color="dark.0"
          onClick={() => {
            closeDocumentAndReset(setPreference)
              .then(() => {
                setLocation(CREATE_DOCUMENT_PATH)
              })
              .catch(() => {
                notifications.show({
                  message: "failed closing document",
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
          }}
        >
          Create New Document
        </Button>
        <Button
          color="dark.0"
          onClick={() => {
            props.onClose()
            configurationModalOpen()
          }}
        >
          Configure
        </Button>
        <Button
          color="dark.0"
          onClick={() => {
            props.onClose()
            helpModalOpen()
          }}
        >
          Help
        </Button>
        <Group justify="space-between" grow gap="xs">
          <Button
            color="dark.0"
            fullWidth
            onClick={() => {
              props.onClose()
              tagsModalOpen()
            }}
          >
            Tags
          </Button>
          <Button
            color="dark.0"
            fullWidth
            onClick={() => {
              props.onClose()
              searchToggle()
            }}
          >
            Search
          </Button>
        </Group>

        <Divider></Divider>

        <Button
          color="red.7"
          onClick={() => {
            closeDocumentAndReset(setPreference)
              .then(() => {
                setLocation(OPEN_DOCUMENT_PATH)
              })
              .catch(() => {
                notifications.show({
                  message: "failed closing document",
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
          }}
        >
          Close Document
        </Button>
        <Button
          color="red.7"
          onClick={() => {
            exit(0)
              .then(() => {
                notifications.show({
                  message: "exiting application",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
              .catch((e) => {
                notifications.show({
                  message: `failed exiting application, ${e}`,
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              })
          }}
        >
          Exit
        </Button>

        <Divider></Divider>

        <AboutActionButtonGroup />
      </Stack>
    </Modal>
  )
}
