import { Anchor, Center, Modal, ModalProps } from "@mantine/core"
import { open } from "@tauri-apps/api/shell"
import { CONTACT_URL, GIT_URL } from "../../util/treedome-app"

export interface AboutModalProps extends ModalProps {}

export function AboutModal(props: AboutModalProps) {
  return (
    <Modal
      {...props}
      title="About"
      styles={{ content: { border: "1px solid white" } }}
    >
      <Center>
        <div style={{ textAlign: "center" }}>
          <p>Created with love by Tengku Izdihar</p>
          <p>
            Software Licensed as GPLv3,{" "}
            <Anchor
              onClick={() => {
                open(GIT_URL).catch((e) => console.error(e))
              }}
            >
              Source Code
            </Anchor>
          </p>
          <p>
            Contact,{" "}
            <Anchor
              onClick={() => {
                open(CONTACT_URL).catch((e) => console.error(e))
              }}
            >
              Matrix Space
            </Anchor>
          </p>
        </div>
      </Center>
    </Modal>
  )
}
