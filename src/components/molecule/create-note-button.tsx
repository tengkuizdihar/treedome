import { Button, Group, Modal, TextInput } from "@mantine/core"
import { AddBox } from "@mui/icons-material"
import { useAtom } from "jotai"
import { currentTreeData, openNoteData } from "../../util/markdown-editor"
import { note_put } from "../../util/treedome-app"
import { notifications } from "@mantine/notifications"
import { concat, find, forEach } from "lodash"
import { NodeModel } from "@minoru/react-dnd-treeview"
import { useLocation } from "wouter"
import { NOTE_PATH } from "../../util/route-path"
import { useDisclosure } from "@mantine/hooks"
import { useForm } from "@mantine/form"
import { buildNoteParentList } from "../atom/note-tree"
import { DEFAULT_DOCUMENT_CONTENT_STRING } from "../../util/frontend-command"

export interface CreateNoteButtonProps {
  noteID?: string
}

export function CreateNoteButton(props: CreateNoteButtonProps) {
  const [opened, { open, close }] = useDisclosure(false)

  return (
    <>
      <Button w="100%" variant="outline" color="blue.7" onClick={open}>
        <AddBox />
      </Button>
      <CreateNewNoteModal
        makeChildNoteID={false}
        opened={opened}
        close={close}
        noteID={props.noteID}
      />
    </>
  )
}

export interface CreateNewNoteModalProps {
  opened: boolean
  close: () => void
  makeChildNoteID: boolean
  noteID?: string
  buttonTitle?: string
}

export function CreateNewNoteModal(props: CreateNewNoteModalProps) {
  const [treeNote, setTreeNote] = useAtom(currentTreeData)
  const [currentOpenNoteData, setCurrentOpenNoteData] = useAtom(openNoteData)

  const [_, setLocation] = useLocation()
  const form = useForm({
    initialValues: {
      title: "",
    },

    validate: {
      title: (v) => {
        if (v.length === 0) {
          return "note title must not be empty"
        }
      },
    },
  })

  const createNewNote = (title: string) => {
    props.close()

    note_put({
      title: title,
      content: DEFAULT_DOCUMENT_CONTENT_STRING,
      text_only: "",
    })
      .then((res) => {
        let parentOfCurrentNote = props.noteID ?? "0"

        if (!props.makeChildNoteID) {
          parentOfCurrentNote = String(
            find(treeNote, (v) => {
              return String(v.id) === props.noteID ?? ""
            })?.parent ?? "0",
          )
        }
        const newNote: NodeModel = {
          id: res.id,
          parent: parentOfCurrentNote,
          text: title,
          droppable: true,
        }
        setTreeNote(concat(treeNote, newNote))

        let currentlyOpen: Set<string> = new Set()
        forEach(currentOpenNoteData, (v) => {
          currentlyOpen.add(String(v))
        })
        let parents = buildNoteParentList(treeNote, parentOfCurrentNote)
        let parentToOpen: string[] = []
        forEach(parents, (parent) => {
          if (!currentlyOpen.has(parent)) {
            parentToOpen = concat(parentToOpen, parent)
          }
        })
        setCurrentOpenNoteData(concat(currentOpenNoteData, parentToOpen))

        setLocation(`${NOTE_PATH}/${res.id}`)

        form.reset()
      })
      .catch((e) => {
        console.error("failed to create new note", e)
        notifications.show({
          message: "failed to create new note",
          color: "red.7",
          autoClose: true,
          withCloseButton: true,
        })
      })
  }

  return (
    <Modal
      size="md"
      opened={props.opened}
      onClose={props.close}
      centered
      withCloseButton={false}
    >
      <form
        onSubmit={form.onSubmit((values) => {
          createNewNote(values.title)
        })}
      >
        <TextInput {...form.getInputProps("title")} placeholder="note title" />
        <Group position="right">
          <Button type="submit" mt="md" variant="outline">
            {props.buttonTitle ??
              (props.makeChildNoteID
                ? "Create Note as Child"
                : "Create Note as Sibling")}
          </Button>
        </Group>
      </form>
    </Modal>
  )
}
