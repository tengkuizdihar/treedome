import { useAtom } from "jotai"
import { useEffect } from "react"
import { useIdleTimer } from "react-idle-timer"
import { useLocation } from "wouter"
import {
  preferenceAtom,
  DEFAULT_PREFERENCE,
  onUserIdle,
} from "../../util/frontend-command"

export const MAX_TIMEOUT_DURATION = 2_147_483_640 // int32 max minus 1

export interface DocumentInactivityProps {
  saveDocument: () => void
}

export const DocumentInactivity = (props: DocumentInactivityProps) => {
  const [currentPreferenceAtom, setCurrentPreferenceAtom] =
    useAtom(preferenceAtom)
  const [_, setLocation] = useLocation()

  // For Inactivity Stuff
  const validatedTime = buildValidatedTimeMilisecond(
    currentPreferenceAtom?.open_timeout_ms,
  )

  const { start, reset } = useIdleTimer({
    timeout: validatedTime,
    startManually: true,
    stopOnIdle: true,
    onIdle: onUserIdle(
      setLocation,
      setCurrentPreferenceAtom,
      props.saveDocument,
    ),
  })

  useEffect(() => {
    if (currentPreferenceAtom !== undefined) {
      reset()
      start()
    }
  }, [currentPreferenceAtom])

  return <></>
}

function buildValidatedTimeMilisecond(milisecond?: number): number {
  if (
    milisecond === undefined ||
    milisecond < 0 ||
    milisecond > MAX_TIMEOUT_DURATION
  ) {
    return DEFAULT_PREFERENCE.open_timeout_ms!
  }

  return milisecond
}
