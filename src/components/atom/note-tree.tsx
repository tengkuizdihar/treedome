import {
  Tree,
  MultiBackend,
  getBackendOptions,
  DndProvider,
  NodeModel,
} from "@minoru/react-dnd-treeview"
import styled from "@emotion/styled"
import { ExpandLess, ExpandMore, Remove } from "@mui/icons-material"
import { currentTreeData, openNoteData } from "../../util/markdown-editor"
import { useAtom } from "jotai"
import styles from "./note-tree.module.css"
import { concat, forEach, join, map } from "lodash"

export interface NoteTreeProps {
  onNoteClick: (id: string) => void
  currentOpenID: string
}

function NoteText(props: { isCurrentOpenedNote: boolean; title: string }) {
  if (props.isCurrentOpenedNote) {
    return <b>{props.title}</b>
  }

  return <>{props.title}</>
}

// Directly used from https://github.com/minop1205/react-dnd-treeview
export function NoteTree(props: NoteTreeProps) {
  const [treeData, setTreeData] = useAtom(currentTreeData)
  const [currentOpenNoteData, setOpenNoteData] = useAtom(openNoteData)

  return (
    <StyledComponent>
      <DndProvider backend={MultiBackend} options={getBackendOptions()}>
        <Tree
          initialOpen={currentOpenNoteData}
          onChangeOpen={(newOpenIDs) => {
            setOpenNoteData(newOpenIDs as string[])
          }}
          tree={treeData}
          sort={false}
          rootId={"0"}
          classes={{
            draggingSource: styles.draggingSource,
            dropTarget: styles.dropTarget,
          }}
          render={(node, { isOpen, onToggle, hasChild }) => (
            <div>
              {node.droppable && hasChild && (
                <span onClick={onToggle} style={{ marginRight: "0.5em" }}>
                  {isOpen ? (
                    <ExpandLess style={{ verticalAlign: "middle" }} />
                  ) : (
                    <ExpandMore style={{ verticalAlign: "middle" }} />
                  )}
                </span>
              )}

              {node.droppable && !hasChild && (
                <span style={{ marginRight: "0.5em" }}>
                  <Remove style={{ verticalAlign: "middle" }} />
                </span>
              )}

              <span onClick={() => props.onNoteClick(node.id.toString())}>
                <NoteText
                  title={node.text}
                  isCurrentOpenedNote={node.id === props.currentOpenID}
                />
              </span>
            </div>
          )}
          dragPreviewRender={(monitorProps) => (
            <div>{monitorProps.item.text}</div>
          )}
          onDrop={(newTree) => setTreeData(newTree)}
          placeholderRender={(node, { depth }) => (
            <Placeholder node={node} depth={depth} />
          )}
          dropTargetOffset={10}
          insertDroppableFirst={false}
          canDrop={(tree, { dragSource, dropTargetId, dropTarget }) => {
            if (dragSource?.parent === dropTargetId) {
              return true
            }
          }}
        />
      </DndProvider>
    </StyledComponent>
  )
}

const StyledComponent = styled.div`
  & > ul {
    height: 96%;
  }

  ul {
    list-style: none;
    width: max-content;
  }

  & {
    height: 100%;
    overflow: scroll;
  }
`

type Props = {
  node: NodeModel
  depth: number
}

export const Placeholder: React.FC<Props> = (props) => {
  const left = props.depth * 24
  return <div className={styles.placeholderRoot} style={{ left }}></div>
}

export function buildActionFromTreeData(
  treeData: NodeModel<unknown>[],
  redirectToID: (to: string) => void,
) {
  let nodeMap: Map<string, NodeModel<any>> = new Map()
  forEach(treeData, (v) => {
    nodeMap.set(String(v.id), v)
  })

  let actions = map(treeData, (v) => {
    let parents: string[] = []
    let lastParent = v.parent
    for (let index = 0; index < 10; index++) {
      let parentData = nodeMap.get(String(lastParent))
      if (parentData === undefined) {
        break
      }

      parents = [...parents, parentData.text]
      lastParent = parentData.parent
    }

    return {
      title: `Note: ${v.text}`,
      onTrigger: () => redirectToID(String(v.id)),
      description: join(parents, " > "),
    }
  })
  return actions
}

export function buildNoteParentList(
  treeNote: NodeModel<any>[],
  firstParentToSearch: string,
): string[] {
  let nodeMap: Map<string, NodeModel<any>> = new Map()
  forEach(treeNote, (v) => {
    nodeMap.set(String(v.id), v)
  })

  let parents: string[] = []
  let lastParent = firstParentToSearch
  for (let index = 0; index < 10; index++) {
    let parentData = nodeMap.get(String(lastParent))
    if (parentData === undefined) {
      break
    }

    parents = concat(parents, String(parentData.id))
    lastParent = String(parentData.parent)
  }

  return parents
}
