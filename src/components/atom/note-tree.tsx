import {
  Tree,
  MultiBackend,
  getBackendOptions,
  DndProvider,
  NodeModel,
} from "@minoru/react-dnd-treeview"
import styled from "@emotion/styled"
import { ExpandLess, ExpandMore, Remove } from "@mui/icons-material"
import {
  currentTreeData,
  noteSizeData,
  openNoteData,
} from "../../util/markdown-editor"
import { useAtom } from "jotai"
import styles from "./note-tree.module.css"
import {
  concat,
  find,
  forEach,
  has,
  join,
  keyBy,
  map,
  reverse,
} from "lodash"
import { DeepNoteData } from "../../util/treedome-app"
import {
  SpotlightActions,
  SpotlightFilterFunction,
} from "@mantine/spotlight/lib/Spotlight"
import Fuse from "fuse.js"
import { NotePathItem } from "./breadcrumb-note-path"
import { useEffect, useState } from "react"
import { useScrollIntoView } from "@mantine/hooks"
import { HumanFileSize } from "../../util/math"
import { Tooltip } from "@mantine/core"
import { MONTH_NAMES } from "../../util/frontend-command"

export const MAX_TREE_DEPTH = 100

export interface NoteTreeProps {
  onNoteClick: (id: string) => void
  currentOpenID: string
  isLoaded: boolean
}

function NoteText(props: {
  isCurrentOpenedNote: boolean
  title: string
  showSize: boolean
  selfSize?: number // size of the note itself excluding the children's
  inclusiveSize?: number // size of the note including all of its children
}): React.ReactNode {
  const sizeInfoComponent = props.showSize &&
    props.selfSize !== undefined &&
    props.inclusiveSize !== undefined && (
      <>
        {" "}
        <b style={{ color: "#218380" }}>
          <Tooltip inline label="note size including children">
            <span>{HumanFileSize(props.inclusiveSize)}</span>
          </Tooltip>
          {" - "}
          <Tooltip inline label="this note size only">
            <span>{HumanFileSize(props.selfSize)}</span>
          </Tooltip>
        </b>
      </>
    )

  if (props.isCurrentOpenedNote) {
    return (
      <>
        <b style={{ color: "#FFCC66" }}>{props.title}</b>
        {sizeInfoComponent}
      </>
    )
  }

  return (
    <>
      {props.title} {sizeInfoComponent}
    </>
  )
}

// Directly used from https://github.com/minop1205/react-dnd-treeview
export function NoteTree(props: NoteTreeProps) {
  const [noteSizeInclusive, setNoteSizeInclusive] = useState(
    // doesn't include its own size
    new Map<string, number>(),
  )
  const [noteSize, setNoteSize] = useAtom(noteSizeData)
  const [treeData, setTreeData] = useAtom(currentTreeData)
  const [currentOpenNoteData, setOpenNoteData] = useAtom(openNoteData)
  const { scrollIntoView, targetRef, scrollableRef } = useScrollIntoView<
    HTMLDivElement,
    HTMLDivElement
  >({
    duration: 250,
  })

  // will force to scroll when currentOpenID is moved
  useEffect(() => {
    scrollIntoView({ alignment: "center" })
  }, [props.currentOpenID])

  // hide note size data if tree changed
  useEffect(() => {
    setNoteSize({
      show: false,
      data: new Map(),
    })
  }, [treeData])

  useEffect(() => {
    if (!(noteSize?.show ?? false)) {
      return
    }

    let idToParent = new Map<string, string>()
    for (const datum of treeData) {
      idToParent.set(String(datum.id), String(datum.parent))
    }

    let sizes = new Map<string, number>()
    for (const datum of noteSize?.data ?? []) {
      let parentID = idToParent.get(datum[0]) ?? "0"

      let parents = buildNoteParentList(treeData, parentID)
      for (const parent of parents) {
        sizes.set(parent, (sizes.get(parent) ?? 0) + datum[1])
      }
    }

    setNoteSizeInclusive(sizes)
  }, [noteSize, treeData])

  return (
    <StyledComponent ref={scrollableRef}>
      <DndProvider backend={MultiBackend} options={getBackendOptions()}>
        <Tree
          initialOpen={currentOpenNoteData}
          onChangeOpen={(newOpenIDs) => {
            setOpenNoteData(newOpenIDs as string[])
          }}
          tree={treeData}
          sort={false}
          rootId={"0"}
          classes={{
            draggingSource: styles.draggingSource,
            dropTarget: styles.dropTarget,
          }}
          render={(node, { isOpen, onToggle, hasChild }) => (
            <NodeRendered
              node={node}
              hasChild={hasChild}
              onToggle={onToggle}
              isOpen={isOpen}
              currentOpenID={props.currentOpenID}
              onNoteClick={props.onNoteClick}
              targetRef={targetRef}
              showSize={noteSize?.show ?? false}
              selfSize={noteSize?.data.get(String(node.id))}
              inclusiveSize={
                (noteSizeInclusive.get(String(node.id)) ?? 0) +
                (noteSize?.data.get(String(node.id)) ?? 0)
              } // TODO must be built in NoteTree everytime noteSizeData changes
            />
          )}
          dragPreviewRender={(monitorProps) => (
            <div>{monitorProps.item.text}</div>
          )}
          onDrop={(newTree) => setTreeData(newTree)}
          placeholderRender={(node, { depth }) => (
            <Placeholder node={node} depth={depth} />
          )}
          dropTargetOffset={10}
          insertDroppableFirst={false}
          canDrop={(_tree, { dragSource, dropTargetId }) => {
            if (dragSource?.parent === dropTargetId) {
              return true
            }
          }}
        />
      </DndProvider>
    </StyledComponent>
  )
}

interface NodeRenderedProps {
  node: NodeModel<DeepNoteData>
  hasChild: boolean
  onToggle: () => void
  isOpen: boolean
  currentOpenID: string | number
  onNoteClick(param: string | number): void
  targetRef: React.MutableRefObject<HTMLDivElement>
  showSize: boolean
  selfSize?: number // size of the note itself excluding the children's
  inclusiveSize?: number // size of the note including all of its children
}

const NodeRendered = (props: NodeRenderedProps) => {
  return (
    <div
      ref={props.node.id === props.currentOpenID ? props.targetRef : undefined}
    >
      {props.node.droppable && props.hasChild && (
        <span onClick={props.onToggle} style={{ marginRight: "0.25em" }}>
          {props.isOpen ? (
            <ExpandLess style={{ verticalAlign: "middle" }} />
          ) : (
            <ExpandMore style={{ verticalAlign: "middle" }} />
          )}
        </span>
      )}

      {props.node.droppable && !props.hasChild && (
        <span style={{ marginRight: "0.25em" }}>
          <Remove style={{ verticalAlign: "middle" }} />
        </span>
      )}

      <span onClick={() => props.onNoteClick(props.node.id.toString())}>
        <NoteText
          title={props.node.text}
          isCurrentOpenedNote={props.node.id === props.currentOpenID}
          showSize={props.showSize}
          selfSize={props.selfSize}
          inclusiveSize={props.inclusiveSize}
        />
      </span>
    </div>
  )
}

const StyledComponent = styled.div`
  & > ul {
    height: 96%;
  }

  ul {
    list-style: none;
    width: max-content;
    padding-left: 1.5em;
  }

  & {
    height: 100%;
    overflow: scroll;
  }
`

type Props = {
  node: NodeModel
  depth: number
}

export const Placeholder: React.FC<Props> = (props) => {
  const left = props.depth * 24
  return <div className={styles.placeholderRoot} style={{ left }}></div>
}

export function buildActionFromTreeData(
  treeData: NodeModel<DeepNoteData>[],
  redirectToID: (to: string) => void,
) {
  let nodeMap: Map<string, NodeModel<DeepNoteData>> = new Map()
  forEach(treeData, (v) => {
    nodeMap.set(String(v.id), v)
  })

  let actions: SpotlightActions[] = map(treeData, (v) => {
    let parents: string[] = []
    let lastParent = v.parent
    for (let index = 0; index < MAX_TREE_DEPTH; index++) {
      let parentData = nodeMap.get(String(lastParent))
      if (parentData === undefined) {
        break
      }

      parents = [...parents, parentData.text]
      lastParent = parentData.parent
    }

    let parentToChildren = reverse([v.text, ...parents])
    let keywords = join([...(v.data?.tags ?? [])], " ")
    let labelValue = join(parentToChildren, " ➜ ")
    if (labelValue.length >= 57) {
      labelValue = `...${labelValue.slice(-57)}`
    }

    return {
      id: String(v.id),
      label: labelValue,
      onClick: () => redirectToID(String(v.id)),
      description: join(v.data?.tags, ", "),
      keywords: keywords,
    }
  })
  return actions
}

export function buildNoteParentList(
  treeNote: NodeModel<DeepNoteData>[],
  firstParentToSearch: string,
): string[] {
  let nodeMap: Map<string, NodeModel<DeepNoteData>> = new Map()
  forEach(treeNote, (v) => {
    nodeMap.set(String(v.id), v)
  })

  let parents: string[] = []
  let lastParent = firstParentToSearch
  for (let index = 0; index < MAX_TREE_DEPTH; index++) {
    let parentData = nodeMap.get(String(lastParent))
    if (parentData === undefined) {
      break
    }

    parents = concat(parents, String(parentData.id))
    lastParent = String(parentData.parent)
  }

  return parents
}

export function buildNoteParentListBreadcrumb(
  treeNote: NodeModel<DeepNoteData>[],
  firstParentToSearch: string,
): NotePathItem[] {
  let nodeMap: Map<string, NodeModel<DeepNoteData>> = new Map()
  forEach(treeNote, (v) => {
    nodeMap.set(String(v.id), v)
  })

  let parents: NotePathItem[] = []
  let lastParent = firstParentToSearch
  for (let index = 0; index < MAX_TREE_DEPTH; index++) {
    let parentData = nodeMap.get(String(lastParent))
    if (parentData === undefined) {
      break
    }

    parents = concat(parents, {
      noteID: String(parentData.id),
      title: parentData.text,
    })
    lastParent = String(parentData.parent)
  }

  return reverse(parents)
}

// result is its children recursively, not including parent
export function buildChildren(
  treeNote: NodeModel<DeepNoteData>[],
  parent: string,
): NodeModel<DeepNoteData>[] {
  // check whether parent exist or not
  let firstNode = find(treeNote, (v) => String(v.parent) === parent)
  if (firstNode === undefined) {
    return []
  }

  // make cache for parentID => child[]
  let parentToChildren: Map<string, NodeModel<DeepNoteData>[]> = new Map()
  forEach(treeNote, (v) => {
    let parentKey = String(v.parent)
    let children = parentToChildren.get(parentKey)
    if (children === undefined) {
      parentToChildren.set(parentKey, [v])
    } else {
      parentToChildren.set(parentKey, [...children, v])
    }
  })

  // start searching with the parent id first
  let parentSearchQueue = [parent] // the id we want to search
  let result: NodeModel<DeepNoteData>[] = []

  const MAX_ITERATION = 10_000_000 // ten millions operation, anything larger than this is a bug
  let iteration = 0
  while (iteration < MAX_ITERATION) {
    let parent = parentSearchQueue.pop()
    if (parent === undefined) {
      break
    }

    let children = parentToChildren.get(parent) ?? []
    forEach(children, (v) => {
      result.push(v)
      parentSearchQueue.push(String(v.id))
    })

    iteration += 1
  }

  // if NASA could use this rule and go to the moon
  // then at least this can prevent the program from hanging https://en.wikipedia.org/wiki/The_Power_of_10:_Rules_for_Developing_Safety-Critical_Code
  if (iteration >= MAX_ITERATION) {
    throw new Error(
      "buildChildrenRecursively exceeds iteration limit, please report to https://codeberg.org/solver-orgz/treedome/issues/new",
    )
  }

  return result
}

export function NaturalSortNote(a: NodeModel<DeepNoteData>, b: NodeModel<DeepNoteData>): number {
  let isAMonth = find(MONTH_NAMES, (v) => v == a.text) // YES YOU NEED TO USE FUNCTION INSTEAD OF JUST VALUE
  let isBMonth = find(MONTH_NAMES, (v) => v == b.text) // YES YOU NEED TO USE FUNCTION INSTEAD OF JUST VALUE

  // don't try to be smart with me, there's only 12 month names, no need for map!
  if (isAMonth !== undefined && isBMonth !== undefined) {
    return MONTH_NAMES.indexOf(a.text) - MONTH_NAMES.indexOf(b.text)
  }

  // sort with text
  const localeSort = a.text.localeCompare(b.text, undefined, {
    numeric: true,
    ignorePunctuation: true,
  })

  if (localeSort != 0) {
    return localeSort
  }

  // if not possible then sort with id, just to be consistent if there's two different name under a tree
  return String(a.id).localeCompare(String(b.id))
}

export function sortChildrenRecursively(
  treeNote: NodeModel<DeepNoteData>[],
  parent: string,
): NodeModel<DeepNoteData>[] {
  let sortedChildren = buildChildren(treeNote, parent).toSorted((a, b) => NaturalSortNote(a, b))

  let childrenByID = keyBy(sortedChildren, "id")
  let newTree: NodeModel<DeepNoteData>[] = []

  forEach(treeNote, (note) => {
    if (has(childrenByID, note.id)) {
      return
    }

    newTree.push(note)

    if (note.id === parent) {
      newTree.push(...sortedChildren)
    }
  })

  return newTree
}

export const spotlightFilter = (
  fuse: Fuse<SpotlightActions>,
): SpotlightFilterFunction => {
  return (query, actions) => {
    let result = fuse.search(query, {
      limit: 15,
    })

    return map(result, (v) => actions[v.refIndex])
  }
}
