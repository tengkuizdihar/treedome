import { Anchor, Paper, ScrollArea, Breadcrumbs } from "@mantine/core"
import { useHover } from "@mantine/hooks"
import { useRef, useState, useEffect } from "react"
import { NOTE_PATH } from "../../util/route-path"
import { useLocation } from "wouter"

export interface NotePathItem {
  title: string
  noteID: string
}

export interface BreadcrumbNotePathProps {
  notePathItems: NotePathItem[]
}

export const BreadcrumbNotePath = (props: BreadcrumbNotePathProps) => {
  const [_, setLocation] = useLocation()
  const scrollArea = useRef<HTMLDivElement>(null)
  const [paddingBottom, setPaddingBottom] = useState("0rem")
  const { hovered, ref } = useHover()

  useEffect(() => {
    scrollArea.current?.scrollTo({
      left: scrollArea.current?.scrollWidth,
      behavior: "instant",
    })
  }, [props.notePathItems])

  useEffect(() => {
    if (hovered && isHScrollable()) {
      setPaddingBottom("0.6rem")
    }

    if (!hovered) {
      setPaddingBottom("0rem")
    }
  }, [hovered])

  // got it from here https://stackoverflow.com/a/29956714
  const isHScrollable = () => {
    return (
      (scrollArea.current?.scrollWidth ?? 0) >
      (scrollArea.current?.clientWidth ?? 0)
    )
  }

  const redirectToAnotherNote = (noteID: string) => {
    setLocation(`${NOTE_PATH}/${noteID}`)
  }

  return (
    <Paper bg="dark.6" px="0.75rem" py="0.6rem">
      <ScrollArea
        viewportRef={scrollArea}
        ref={ref}
        type="hover"
        scrollbarSize="0.5rem"
        pb={paddingBottom}
        scrollHideDelay={0}
      >
        <Breadcrumbs separator="/">
          {props.notePathItems.map((item, index) => (
            <Anchor
              onClick={() => redirectToAnotherNote(item.noteID)}
              key={index}
            >
              {item.title}
            </Anchor>
          ))}
        </Breadcrumbs>
      </ScrollArea>
    </Paper>
  )
}
