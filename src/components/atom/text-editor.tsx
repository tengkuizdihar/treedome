import { useEffect } from "react"
import { lowlight } from "lowlight"
import Image from "@tiptap/extension-image"
import { useEditor } from "@tiptap/react"
import StarterKit from "@tiptap/starter-kit"
import Highlight from "@tiptap/extension-highlight"
import Underline from "@tiptap/extension-underline"
import TextAlign from "@tiptap/extension-text-align"
import Superscript from "@tiptap/extension-superscript"
import SubScript from "@tiptap/extension-subscript"
import Placeholder from "@tiptap/extension-placeholder"
import { Plugin } from "@tiptap/pm/state"
import CodeBlockLowlight from "@tiptap/extension-code-block-lowlight"
import { RichTextEditor } from "@mantine/tiptap"
import { editorContent, editorRef } from "../../util/markdown-editor"
import { SearchAndReplace } from "../../util/text-editor-extension-search-and-replace"
import { useAtom } from "jotai"
import "./text-editor.css"

export interface TextEditorProp {}

// TODO: https://github.com/ueberdosis/tiptap/discussions/2988#discussioncomment-3200048
//       https://github.com/ueberdosis/tiptap/issues/2912#issuecomment-1169631614
//       https://github.com/search?q=repo%3Aueberdosis%2Ftiptap%20handlePaste&type=code
export const MyImage = Image.extend({
  addProseMirrorPlugins() {
    return [
      new Plugin({
        props: {
          handleDOMEvents: {
            drop(view, event) {
              const hasFiles =
                event.dataTransfer &&
                event.dataTransfer.files &&
                event.dataTransfer.files.length

              if (!hasFiles) {
                return
              }

              const images = Array.from(event.dataTransfer.files).filter(
                (file) => /image/i.test(file.type),
              )

              if (images.length === 0) {
                return
              }

              event.preventDefault()

              const { schema } = view.state
              const coordinates = view.posAtCoords({
                left: event.clientX,
                top: event.clientY,
              })

              images.forEach((image) => {
                const reader = new FileReader()

                reader.onload = (readerEvent) => {
                  const node = schema.nodes.image.create({
                    src: readerEvent.target?.result,
                  })
                  const transaction = view.state.tr.insert(
                    coordinates?.pos ?? 0,
                    node,
                  )
                  view.dispatch(transaction)
                }
                reader.readAsDataURL(image)
              })
            },
            paste(view, event) {
              const hasFiles =
                event.clipboardData &&
                event.clipboardData.files &&
                event.clipboardData.files.length

              if (!hasFiles) {
                return
              }

              const images = Array.from(event.clipboardData.files).filter(
                (file) => /image/i.test(file.type),
              )

              if (images.length === 0) {
                return
              }

              event.preventDefault()

              const { schema } = view.state

              images.forEach((image) => {
                const reader = new FileReader()

                reader.onload = (readerEvent) => {
                  const node = schema.nodes.image.create({
                    src: readerEvent.target?.result,
                  })
                  const transaction = view.state.tr.replaceSelectionWith(node)
                  view.dispatch(transaction)
                }
                reader.readAsDataURL(image)
              })
            },
          },
        },
      }),
    ]
  },
})

export function TextEditor(props: { search_text?: string }) {
  const [, setEditor] = useAtom(editorRef)
  const [content] = useAtom(editorContent)

  const editor = useEditor({
    extensions: [
      StarterKit.configure({
        codeBlock: false,
      }),
      Underline,

      // we currently disable this extension because user might
      // incidentally press it and the whole tauri could load
      // another page from the internet lmao
      // Link,

      Superscript,
      SubScript,
      Highlight,
      TextAlign.configure({ types: ["heading", "paragraph"] }),
      MyImage.configure({
        allowBase64: true,
      }),
      CodeBlockLowlight.configure({
        lowlight,
      }),
      Placeholder.configure({ placeholder: "Write something..." }),
      SearchAndReplace.configure({
        searchResultClass: "search-result",
        caseSensitive: false,
        disableRegex: false,
      }),
    ],
    content: content,
    autofocus: "start",
    editorProps: {
      transformPastedHTML(html) {
        // disable copy pasting image from html, most likely src is URL
        // from https://www.codemzy.com/blog/tiptap-pasting-images
        return html.replace(
          /<img.*?src="(?<imgSrc>.*?)".*?>/gm,
          function (match, imgSrc) {
            if (imgSrc.startsWith("data:image/")) {
              return match
            }
            return ""
          },
        )
      },
    },
  })

  useEffect(() => {
    setEditor(editor)
  }, [editor])

  useEffect(() => {
    editor?.commands.clearContent()
    editor?.commands.focus("start", {
      scrollIntoView: true,
    })
    editor?.commands.setContent(content)
  }, [content])

  useEffect(() => {
    editor?.commands.setSearchTerm(decodeURI(props.search_text ?? ""))
  }, [props.search_text])

  return (
    <>
      <RichTextEditor
        h="100%"
        editor={editor}
        style={{
          overflowY: "scroll",
        }}
      >
        <RichTextEditor.Toolbar sticky>
          <RichTextEditor.ControlsGroup>
            <RichTextEditor.Bold />
            <RichTextEditor.Italic />
            <RichTextEditor.Underline />
            <RichTextEditor.Strikethrough />
            <RichTextEditor.ClearFormatting />
            <RichTextEditor.Highlight />
            <RichTextEditor.Code />
          </RichTextEditor.ControlsGroup>

          <RichTextEditor.ControlsGroup>
            <RichTextEditor.H1 />
            <RichTextEditor.H2 />
            <RichTextEditor.H3 />
            <RichTextEditor.H4 />
          </RichTextEditor.ControlsGroup>

          <RichTextEditor.ControlsGroup>
            <RichTextEditor.Blockquote />
            <RichTextEditor.Hr />
            <RichTextEditor.BulletList />
            <RichTextEditor.OrderedList />
            <RichTextEditor.Subscript />
            <RichTextEditor.Superscript />
          </RichTextEditor.ControlsGroup>

          <RichTextEditor.ControlsGroup>
            <RichTextEditor.Link />
            <RichTextEditor.Unlink />
          </RichTextEditor.ControlsGroup>

          <RichTextEditor.ControlsGroup>
            <RichTextEditor.AlignLeft />
            <RichTextEditor.AlignCenter />
            <RichTextEditor.AlignJustify />
            <RichTextEditor.AlignRight />
          </RichTextEditor.ControlsGroup>
        </RichTextEditor.Toolbar>

        <RichTextEditor.Content h="100%" />
      </RichTextEditor>
    </>
  )
}
