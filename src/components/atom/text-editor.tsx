import { useEffect } from "react"
import { lowlight } from "lowlight"
import Image from "@tiptap/extension-image"
import { BubbleMenu, FloatingMenu, useEditor } from "@tiptap/react"
import StarterKit from "@tiptap/starter-kit"
import Highlight from "@tiptap/extension-highlight"
import Underline from "@tiptap/extension-underline"
import TextAlign from "@tiptap/extension-text-align"
import Superscript from "@tiptap/extension-superscript"
import SubScript from "@tiptap/extension-subscript"
import Placeholder from "@tiptap/extension-placeholder"
import Typography from "@tiptap/extension-typography"
import { Plugin } from "@tiptap/pm/state"
import CodeBlockLowlight from "@tiptap/extension-code-block-lowlight"
import { getTaskListExtension, RichTextEditor } from "@mantine/tiptap"
import TipTapTaskList from "@tiptap/extension-task-list"
import { editorContent, editorRef } from "../../util/markdown-editor"
import { SearchAndReplace } from "../../util/text-editor-extension-search-and-replace"
import { useAtom } from "jotai"
import "./text-editor.css"
import { DEFAULT_PREFERENCE, preferenceAtom } from "../../util/frontend-command"
import TaskItem from "@tiptap/extension-task-item"
import { Link } from "../../util/text-editor-link"
import "katex/dist/katex.min.css"
import MathExtension from "@aarkue/tiptap-math-extension"

export interface TextEditorProp {}

// TODO: https://github.com/ueberdosis/tiptap/discussions/2988#discussioncomment-3200048
//       https://github.com/ueberdosis/tiptap/issues/2912#issuecomment-1169631614
//       https://github.com/search?q=repo%3Aueberdosis%2Ftiptap%20handlePaste&type=code
export const MyImage = Image.extend({
  addProseMirrorPlugins() {
    return [
      new Plugin({
        props: {
          handleDOMEvents: {
            drop(view, event) {
              const hasFiles =
                event.dataTransfer &&
                event.dataTransfer.files &&
                event.dataTransfer.files.length

              if (!hasFiles) {
                return
              }

              const images = Array.from(event.dataTransfer.files).filter(
                (file) => /image/i.test(file.type),
              )

              if (images.length === 0) {
                return
              }

              event.preventDefault()

              const { schema } = view.state
              const coordinates = view.posAtCoords({
                left: event.clientX,
                top: event.clientY,
              })

              images.forEach((image) => {
                const reader = new FileReader()

                reader.onload = (readerEvent) => {
                  const node = schema.nodes.image.create({
                    src: readerEvent.target?.result,
                  })
                  const transaction = view.state.tr.insert(
                    coordinates?.pos ?? 0,
                    node,
                  )
                  view.dispatch(transaction)
                }
                reader.readAsDataURL(image)
              })
            },
            paste(view, event) {
              const hasFiles =
                event.clipboardData &&
                event.clipboardData.files &&
                event.clipboardData.files.length

              if (!hasFiles) {
                return
              }

              const images = Array.from(event.clipboardData.files).filter(
                (file) => /image/i.test(file.type),
              )

              if (images.length === 0) {
                return
              }

              event.preventDefault()

              const { schema } = view.state

              images.forEach((image) => {
                const reader = new FileReader()

                reader.onload = (readerEvent) => {
                  const node = schema.nodes.image.create({
                    src: readerEvent.target?.result,
                  })
                  const transaction = view.state.tr.replaceSelectionWith(node)
                  view.dispatch(transaction)
                }
                reader.readAsDataURL(image)
              })
            },
          },
        },
      }),
    ]
  },
})

export function TextEditor(props: { search_text?: string }) {
  const [, setEditor] = useAtom(editorRef)
  const [content] = useAtom(editorContent)
  const [currentPreferenceAtom] = useAtom(preferenceAtom)

  const editor = useEditor({
    extensions: [
      StarterKit.configure({
        codeBlock: false,
      }),
      Placeholder.configure({ placeholder: "Write something..." }),
      MathExtension.configure({
        evaluation: false,
        addInlineMath: true,
        katexOptions: { macros: { "\\B": "\\mathbb{B}" } },
        delimiters: "dollar",
      }),
      Underline,
      Link.configure({
        openOnClick: true,
        HTMLAttributes: {
          class: "custom-link-style",
        },
      }),
      Superscript,
      SubScript,
      Highlight,
      TextAlign.configure({ types: ["heading", "paragraph"] }),
      MyImage.configure({
        allowBase64: true,
      }),
      CodeBlockLowlight.configure({
        lowlight,
      }),
      SearchAndReplace.configure({
        searchResultClass: "search-result",
        disableRegex: false,
      }),
      Typography.configure({
        openDoubleQuote: false,
        closeDoubleQuote: false,
        openSingleQuote: false,
        closeSingleQuote: false,
      }),
      getTaskListExtension(TipTapTaskList),
      TaskItem.configure({
        nested: true,
        HTMLAttributes: {
          class: "test-item",
        },
      }),
    ],
    content: content,
    autofocus: "start",
    editorProps: {
      transformPastedHTML(html) {
        // disable copy pasting image from html, most likely src is URL
        // from https://www.codemzy.com/blog/tiptap-pasting-images
        return html.replace(
          /<img.*?src="(?<imgSrc>.*?)".*?>/gm,
          function (match, imgSrc) {
            if (imgSrc.startsWith("data:image/")) {
              return match
            }
            return ""
          },
        )
      },
    },
  })

  useEffect(() => {
    setEditor(editor)
  }, [editor])

  useEffect(() => {
    editor?.commands.clearContent()
    editor?.commands.focus("start", {
      scrollIntoView: true,
    })
    editor?.commands.setContent(content)
  }, [content])

  useEffect(() => {
    editor?.commands.setSearchTerm(decodeURI(props.search_text ?? ""))
  }, [props.search_text])

  const isShowToolbar =
    currentPreferenceAtom?.enable_toolbar ?? DEFAULT_PREFERENCE.enable_toolbar!

  return (
    <>
      <RichTextEditor
        h="100%"
        editor={editor}
        style={{
          overflowY: "scroll",
        }}
      >
        {editor && isShowToolbar ? (
          <RichTextEditor.Toolbar sticky>
            <RichTextEditor.ControlsGroup>
              <RichTextEditor.Bold />
              <RichTextEditor.Italic />
              <RichTextEditor.Underline />
              <RichTextEditor.Strikethrough />
              <RichTextEditor.ClearFormatting />
              <RichTextEditor.Highlight />
              <RichTextEditor.Code />
            </RichTextEditor.ControlsGroup>

            <RichTextEditor.ControlsGroup>
              <RichTextEditor.H1 />
              <RichTextEditor.H2 />
              <RichTextEditor.H3 />
              <RichTextEditor.H4 />
            </RichTextEditor.ControlsGroup>

            <RichTextEditor.ControlsGroup>
              <RichTextEditor.Blockquote />
              <RichTextEditor.Hr />
              <RichTextEditor.BulletList />
              <RichTextEditor.OrderedList />
              <RichTextEditor.TaskList />
              <RichTextEditor.Subscript />
              <RichTextEditor.Superscript />
            </RichTextEditor.ControlsGroup>

            <RichTextEditor.ControlsGroup>
              <RichTextEditor.Link initialExternal={true} />
              <RichTextEditor.Unlink />
            </RichTextEditor.ControlsGroup>

            <RichTextEditor.ControlsGroup>
              <RichTextEditor.AlignLeft />
              <RichTextEditor.AlignCenter />
              <RichTextEditor.AlignJustify />
              <RichTextEditor.AlignRight />
            </RichTextEditor.ControlsGroup>

            <RichTextEditor.ControlsGroup>
              <RichTextEditor.Undo />
              <RichTextEditor.Redo />
            </RichTextEditor.ControlsGroup>
          </RichTextEditor.Toolbar>
        ) : (
          <div></div>
        )}

        <div>
          {(currentPreferenceAtom?.enable_floating_toolbar ??
            DEFAULT_PREFERENCE.enable_floating_toolbar!) &&
            editor && (
              <div>
                <div>
                  <BubbleMenu editor={editor}>
                    <RichTextEditor.ControlsGroup>
                      <RichTextEditor.Bold />
                      <RichTextEditor.Italic />
                      <RichTextEditor.Underline />
                      <RichTextEditor.Strikethrough />
                      <RichTextEditor.ClearFormatting />
                      <RichTextEditor.Highlight />
                      <RichTextEditor.Code />
                      <RichTextEditor.Link initialExternal={true} />
                      <RichTextEditor.Unlink />
                    </RichTextEditor.ControlsGroup>
                  </BubbleMenu>
                </div>
                <div>
                  <FloatingMenu editor={editor}>
                    <RichTextEditor.ControlsGroup>
                      <RichTextEditor.H1 />
                      <RichTextEditor.H2 />
                      <RichTextEditor.BulletList />
                      <RichTextEditor.Blockquote />
                      <RichTextEditor.AlignLeft />
                      <RichTextEditor.AlignCenter />
                      <RichTextEditor.AlignJustify />
                      <RichTextEditor.AlignRight />
                    </RichTextEditor.ControlsGroup>
                  </FloatingMenu>
                </div>
              </div>
            )}
        </div>
        <RichTextEditor.Content h="100%" />
      </RichTextEditor>
    </>
  )
}
