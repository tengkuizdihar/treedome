import {
  Container,
  Title,
  Paper,
  Space,
  TextInput,
  Button,
  Stack,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { useEffect, useState } from "react"
import {
  get_terminal_command,
  set_terminal_command,
} from "../../util/frontend-command"
import { DocumentPageButton } from "../atom/document-page-button"

export const SettingsDocument: React.FC<{}> = () => {
  const form = useForm({
    initialValues: {
      terminalCommand: "",
    },
  })

  useEffect(() => {
    form.setFieldValue("terminalCommand", get_terminal_command())
  }, [])

  return (
    <Container py={40} size={420}>
      <Title align="center">treedome</Title>
      <Space h="md" />
      <DocumentPageButton currentPage="settings" />

      <Paper withBorder shadow="md" p={30} mt={30} radius="md">
        <form
          onSubmit={form.onSubmit((v) => {
            set_terminal_command(v.terminalCommand)
          })}
        >
          <Stack align="stretch">
            <TextInput
              {...form.getInputProps("terminalCommand")}
              label="Open Terminal Command"
              placeholder={'alacritty --working-directory "{directory}"'}
            />
            <Button type="submit">submit</Button>
          </Stack>
        </form>
      </Paper>
    </Container>
  )
}
