import {
  Button,
  PasswordInput,
  Input,
  Overlay,
  Stack,
  Loader,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { save } from "@tauri-apps/plugin-dialog"
import { document_open, password_quality_check } from "../../util/treedome-app"
import { currentTreeData } from "../../util/markdown-editor"
import { useAtom } from "jotai"
import { useLocation } from "wouter"
import { NOTE_PATH } from "../../util/route-path"
import { useEffect, useState } from "react"
import { useDebouncedValue } from "@mantine/hooks"
import { notifications } from "@mantine/notifications"
import {
  buildFileLocation,
  DEFAULT_PREFERENCE,
  preferenceAtom,
  set_opened_document_paths,
} from "../../util/frontend-command"
import { HomeComponent } from "../molecule/home-component"
import { isEmpty } from "lodash"

export const CreateDocument: React.FC<{}> = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [, setTreeData] = useAtom(currentTreeData)
  const [, setPreferenceAtom] = useAtom(preferenceAtom)
  const [, setLocation] = useLocation()
  const form = useForm({
    initialValues: {
      fileLocation: "",
      password: "",
      confirmPassword: "",
    },
  })
  const [debouncedPassword] = useDebouncedValue(form.values.password, 200)

  useEffect(() => {
    password_quality_check({
      password: form.values.password,
    }).catch((e) => {
      form.setFieldError("password", String(e))
    })
  }, [debouncedPassword])

  return (
    <HomeComponent currentPage="create_document">
      {isLoading && (
        <Overlay blur={15} center>
          <Stack align="center" gap="sm" justify="center">
            <Loader color="indigo" size="xl" />
            <h3>Creating your lovely document...</h3>
          </Stack>
        </Overlay>
      )}

      <form
        onSubmit={form.onSubmit((values) => {
          const weAsyncNow = async () => {
            try {
              await password_quality_check({
                password: form.values.password,
              })
            } catch (error) {
              form.setFieldError("password", String(error))
            }

            let passwordIsDifferent = values.password !== values.confirmPassword
            if (passwordIsDifferent) {
              form.setFieldError("confirmPassword", "Passwords do not match")
            }

            let fileLocationIsEmpty = values.fileLocation.length === 0
            if (fileLocationIsEmpty) {
              form.setFieldError(
                "fileLocation",
                "New file location must not be empty",
              )
            }

            if (isEmpty(form.errors)) {
              setIsLoading(true)

              try {
                var fileLocation = buildFileLocation(values.fileLocation)
                let res = await document_open({
                  path: fileLocation,
                  password: values.password,
                })

                notifications.clean()
                set_opened_document_paths(fileLocation)
                setTreeData(res.meta_model.tree)
                setPreferenceAtom(
                  res.meta_model.preference ?? DEFAULT_PREFERENCE,
                )
                setLocation(
                  NOTE_PATH + `/${res.meta_model.last_opened_note_id}`,
                )
              } catch (error) {
                notifications.show({
                  message: String(error),
                  color: "red.7",
                  autoClose: false,
                  withCloseButton: true,
                })
              }

              setIsLoading(false)
            }
          }

          weAsyncNow()
        })}
      >
        <Input.Wrapper
          withAsterisk
          label="New File Location"
          key={form.key("fileLocation")}
          onClick={() => {
            save({
              filters: [
                {
                  name: "Treedome Note",
                  extensions: ["note"],
                },
              ],
            }).then((value) => {
              form.setFieldValue("fileLocation", value ?? "")
            })
          }}
          error={form.getInputProps("fileLocation").error}
        >
          <Input
            readOnly
            value={
              form.values.fileLocation.length > 0
                ? form.values.fileLocation
                : ""
            }
            placeholder="Path to your document"
          />
        </Input.Wrapper>
        <PasswordInput
          label="Password"
          placeholder="Long but easy to remember"
          required
          mt="md"
          {...form.getInputProps("password")}
          key={form.key("password")}
        />
        <PasswordInput
          label="Repeat Your Password"
          placeholder="Long but easy to remember"
          required
          mt="md"
          {...form.getInputProps("confirmPassword")}
          key={form.key("confirmPassword")}
        />
        <Button type="submit" fullWidth mt="xl">
          Create New Document
        </Button>
      </form>
    </HomeComponent>
  )
}
