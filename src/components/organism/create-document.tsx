import {
  Container,
  Title,
  Paper,
  Button,
  PasswordInput,
  Input,
  Overlay,
  Stack,
  Loader,
  Space,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { save } from "@tauri-apps/api/dialog"
import { document_open, password_quality_check } from "../../util/treedome-app"
import { currentTreeData } from "../../util/markdown-editor"
import { useAtom } from "jotai"
import { useLocation } from "wouter"
import { NOTE_PATH } from "../../util/route-path"
import { useEffect, useState } from "react"
import { useDebouncedValue } from "@mantine/hooks"
import { notifications } from "@mantine/notifications"
import { set_opened_document_paths } from "../../util/frontend-command"
import { DocumentPageButton } from "../atom/document-page-button"

export const CreateDocument: React.FC<{}> = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [, setTreeData] = useAtom(currentTreeData)
  const [, setLocation] = useLocation()
  const form = useForm({
    initialValues: {
      fileLocation: "",
      password: "",
      confirmPassword: "",
    },
  })
  const [debouncedPassword] = useDebouncedValue(form.values.password, 200)

  useEffect(() => {
    password_quality_check({
      password: form.values.password,
    }).catch((e) => {
      form.setFieldError("password", String(e))
    })
  }, [debouncedPassword])

  return (
    <Container size={420} py={40}>
      {isLoading && (
        <Overlay blur={15} center>
          <Stack align="center" spacing="sm" justify="center">
            <Loader color="indigo" size="xl" />
            <h3>Creating your lovely document...</h3>
          </Stack>
        </Overlay>
      )}

      <Title align="center">treedome</Title>
      <Space h="md" />
      <DocumentPageButton currentPage="create_document" />

      <Paper withBorder shadow="md" p={30} mt={30} radius="md">
        <form
          onSubmit={form.onSubmit((values) => {
            let passwordIsDifferent = values.password !== values.confirmPassword
            let fileLocationIsEmpty = values.fileLocation.length === 0
            let fileLocationIncorrectFileExtension =
              !values.fileLocation.endsWith(".note")

            let isError =
              fileLocationIncorrectFileExtension ||
              passwordIsDifferent ||
              fileLocationIsEmpty

            if (passwordIsDifferent) {
              form.setFieldError("confirmPassword", "Passwords do not match")
            }

            if (fileLocationIsEmpty) {
              form.setFieldError(
                "fileLocation",
                "New file location must not be empty",
              )
            }

            if (fileLocationIncorrectFileExtension) {
              form.setFieldError("fileLocation", "file must ends with .note")
            }

            if (isError) {
              return
            }

            if (!isError) {
              setIsLoading(true)
              document_open({
                path: values.fileLocation,
                password: values.password,
              })
                .then((res) => {
                  set_opened_document_paths(values.fileLocation)
                  setTreeData(res.meta_model.tree)
                  setLocation(
                    NOTE_PATH + `/${res.meta_model.last_opened_note_id}`,
                  )
                })
                .catch((reason) => {
                  notifications.show({
                    message: String(reason),
                    color: "red.7",
                    autoClose: false,
                    withCloseButton: true,
                  })
                  setIsLoading(false)
                })
            }
          })}
        >
          <Input.Wrapper
            withAsterisk
            label="New File Location"
            onClick={() => {
              save({
                filters: [
                  {
                    name: "Treedome Note",
                    extensions: ["note"],
                  },
                ],
              }).then((value) => {
                form.setFieldValue("fileLocation", value ?? "")
              })
            }}
            error={form.getInputProps("fileLocation").error}
          >
            <Input
              readOnly
              value={
                form.values.fileLocation.length > 0
                  ? form.values.fileLocation
                  : ""
              }
              placeholder="Path to your document"
            />
          </Input.Wrapper>
          <PasswordInput
            label="Password"
            placeholder="Long but easy to remember"
            required
            mt="md"
            {...form.getInputProps("password")}
          />
          <PasswordInput
            label="Repeat Your Password"
            placeholder="Long but easy to remember"
            required
            mt="md"
            {...form.getInputProps("confirmPassword")}
          />
          <Button type="submit" fullWidth mt="xl">
            Create New Document
          </Button>
        </form>
      </Paper>
    </Container>
  )
}
