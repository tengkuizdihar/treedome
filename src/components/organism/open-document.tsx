import {
  Container,
  Title,
  Text,
  Anchor,
  Paper,
  Button,
  PasswordInput,
  Input,
  Overlay,
  Stack,
  Loader,
  Divider,
  Space,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { open } from "@tauri-apps/api/dialog"
import { document_open } from "../../util/treedome-app"
import { currentTreeData, openNoteData } from "../../util/markdown-editor"
import { useAtom } from "jotai"
import { useLocation } from "wouter"
import { CREATE_DOCUMENT_PATH, NOTE_PATH } from "../../util/route-path"
import { useEffect, useState } from "react"
import {
  RecentDocumentPathsModel,
  get_recently_opened_document_paths,
  set_opened_document_paths,
  remove_opened_document_paths,
} from "../../util/frontend-command"
import { map } from "lodash"
import { CloseOutlined } from "@mui/icons-material"
import { notifications } from "@mantine/notifications"
import { DocumentPageButton } from "../atom/document-page-button"

export const OpenDocument: React.FC<{}> = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [recentDocumentPaths, setRecentDocumentPaths] =
    useState<RecentDocumentPathsModel>([])
  const [, setTreeData] = useAtom(currentTreeData)
  const [, setOpenNoteData] = useAtom(openNoteData)
  const [, setLocation] = useLocation()
  const form = useForm({
    initialValues: {
      fileLocation: "",
      password: "",
    },
  })

  useEffect(() => {
    setRecentDocumentPaths(get_recently_opened_document_paths())
  }, [])

  return (
    <Container py={40} size={420}>
      {isLoading && (
        <Overlay blur={15} center>
          <Stack align="center" spacing="sm" justify="center">
            <Loader color="indigo" size="xl" />
            <h3>Loading your lovely document...</h3>
          </Stack>
        </Overlay>
      )}

      <Title align="center">treedome</Title>
      <Space h="md" />
      <DocumentPageButton currentPage="open_document" />

      <Paper withBorder shadow="md" p={30} mt={30} radius="md">
        <Stack align="stretch">
          <form
            onSubmit={form.onSubmit((values) => {
              let fileLocationIsEmpty = values.fileLocation.length === 0
              let isError = fileLocationIsEmpty

              if (fileLocationIsEmpty) {
                form.setFieldError(
                  "fileLocation",
                  "New file location must not be empty",
                )
              }

              if (isError) {
                return
              }

              if (!isError) {
                setIsLoading(true)
                document_open({
                  path: values.fileLocation,
                  password: values.password,
                })
                  .then((res) => {
                    set_opened_document_paths(values.fileLocation)
                    setTreeData(res.meta_model.tree)
                    setOpenNoteData(res.meta_model.opened_notes)
                    setLocation(
                      NOTE_PATH + `/${res.meta_model.last_opened_note_id}`,
                    )
                  })
                  .catch((reason) => {
                    form.setFieldError("password", String(reason))
                    setIsLoading(false)
                  })
              }
            })}
          >
            <Input.Wrapper
              withAsterisk
              label="File Location"
              onClick={() => {
                open({
                  multiple: false,
                  filters: [
                    {
                      name: "Treedome Note",
                      extensions: ["note"],
                    },
                  ],
                }).then((value) => {
                  if (typeof value == "string") {
                    form.setFieldValue("fileLocation", value)
                  }
                })
              }}
              error={form.getInputProps("fileLocation").error}
            >
              <Input
                readOnly
                value={
                  form.values.fileLocation.length > 0
                    ? form.values.fileLocation
                    : ""
                }
                placeholder="Path to your document"
              />
            </Input.Wrapper>
            <PasswordInput
              label="Password"
              placeholder="Long but easy to remember"
              required
              mt="md"
              {...form.getInputProps("password")}
            />
            <Button type="submit" fullWidth mt="xl">
              Open Document
            </Button>
          </form>

          {recentDocumentPaths.length > 0 && (
            <>
              <Divider label="recent documents" labelPosition="center" />
              <Stack spacing="0">
                {map(recentDocumentPaths, (documentPath, i) => {
                  let trimmedPath = "..." + documentPath.slice(-20)

                  return (
                    <Button.Group key={i}>
                      <Button
                        color="blue.7"
                        fullWidth
                        variant="outline"
                        radius="0"
                        onClick={() => {
                          form.setFieldValue("fileLocation", documentPath)
                        }}
                      >
                        {trimmedPath}
                      </Button>
                      <Button
                        color="gray.7"
                        variant="outline"
                        radius="0"
                        onClick={() => {
                          setRecentDocumentPaths(
                            remove_opened_document_paths(documentPath),
                          )
                          notifications.show({
                            message:
                              "removed recent document without deleting from disk",
                            autoClose: true,
                            withCloseButton: true,
                          })
                        }}
                      >
                        <CloseOutlined />
                      </Button>
                    </Button.Group>
                  )
                })}
              </Stack>
            </>
          )}
        </Stack>
      </Paper>
    </Container>
  )
}
