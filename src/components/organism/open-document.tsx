import {
  Text,
  Button,
  PasswordInput,
  Input,
  Overlay,
  Stack,
  Loader,
  Divider,
  FocusTrap,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { open } from "@tauri-apps/plugin-dialog"
import { document_open } from "../../util/treedome-app"
import { currentTreeData, openNoteData } from "../../util/markdown-editor"
import { useAtom } from "jotai"
import { useLocation } from "wouter"
import { NOTE_PATH } from "../../util/route-path"
import { useEffect, useState } from "react"
import {
  RecentDocumentPathsModel,
  get_recently_opened_document_paths,
  set_opened_document_paths,
  remove_opened_document_paths,
  preferenceAtom,
  DEFAULT_PREFERENCE,
} from "../../util/frontend-command"
import { map } from "lodash"
import { CloseOutlined } from "@mui/icons-material"
import { notifications } from "@mantine/notifications"
import { HomeComponent } from "../molecule/home-component"

export const OpenDocument: React.FC<{}> = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [recentDocumentPaths, setRecentDocumentPaths] =
    useState<RecentDocumentPathsModel>([])
  const [, setPreferenceAtom] = useAtom(preferenceAtom)
  const [, setTreeData] = useAtom(currentTreeData)
  const [, setOpenNoteData] = useAtom(openNoteData)
  const [, setLocation] = useLocation()
  const form = useForm({
    initialValues: {
      fileLocation: "",
      password: "",
    },
  })

  useEffect(() => {
    setRecentDocumentPaths(get_recently_opened_document_paths())
  }, [])

  useEffect(() => {
    if (recentDocumentPaths.length > 0) {
      form.setFieldValue("fileLocation", recentDocumentPaths[0])
    }
  }, [recentDocumentPaths])

  return (
    <HomeComponent currentPage="open_document">
      {isLoading && (
        <Overlay blur={15} center>
          <Stack align="center" gap="sm" justify="center">
            <Loader color="indigo" size="xl" />
            <h3>Loading your lovely document...</h3>
          </Stack>
        </Overlay>
      )}

      <FocusTrap active>
        <Stack align="stretch">
          <form
            onSubmit={form.onSubmit((values) => {
              let fileLocationIsEmpty = values.fileLocation.length === 0
              let isError = fileLocationIsEmpty

              if (fileLocationIsEmpty) {
                form.setFieldError(
                  "fileLocation",
                  "New file location must not be empty",
                )
              }

              if (isError) {
                return
              }

              if (!isError) {
                setIsLoading(true)
                document_open({
                  path: values.fileLocation,
                  password: values.password,
                })
                  .then((res) => {
                    notifications.clean()
                    // if preference not exist in current note then use the one in DEFAULT_PREFERENCE
                    setPreferenceAtom(
                      res.meta_model.preference ?? DEFAULT_PREFERENCE,
                    )

                    set_opened_document_paths(values.fileLocation)
                    setTreeData(res.meta_model.tree)
                    setOpenNoteData(res.meta_model.opened_notes)
                    setLocation(
                      NOTE_PATH + `/${res.meta_model.last_opened_note_id}`,
                    )
                  })
                  .catch((reason) => {
                    form.setFieldError("password", String(reason))
                    setIsLoading(false)
                  })
              }
            })}
          >
            <Input.Wrapper
              withAsterisk
              label="File Location"
              onClick={() => {
                open({
                  multiple: false,
                  filters: [
                    {
                      name: "Treedome Note",
                      extensions: ["note"],
                    },
                  ],
                }).then((value: any) => {
                  if (typeof value == "string") {
                    form.setFieldValue("fileLocation", value)
                  }
                })
              }}
              error={form.getInputProps("fileLocation").error}
            >
              <Input
                readOnly
                value={
                  form.values.fileLocation.length > 0
                    ? form.values.fileLocation
                    : ""
                }
                placeholder="Path to your document"
              />
            </Input.Wrapper>
            <PasswordInput
              label="Password"
              placeholder="Long but easy to remember"
              required
              mt="md"
              {...form.getInputProps("password")}
              data-autofocus
            />
            <Button type="submit" fullWidth mt="xl">
              Open Document
            </Button>
          </form>

          {recentDocumentPaths.length > 0 && (
            <>
              <Divider
                label={
                  <Text size="xs" c="gray.0">
                    recent documents
                  </Text>
                }
                labelPosition="center"
              />
              <Stack gap="0">
                {map(recentDocumentPaths, (documentPath, i) => {
                  let trimmedPath = "..." + documentPath.slice(-20)

                  return (
                    <Button.Group key={i}>
                      <Button
                        color="gray.4"
                        fullWidth
                        radius="0"
                        onClick={() => {
                          form.setFieldValue("fileLocation", documentPath)
                        }}
                      >
                        {trimmedPath}
                      </Button>
                      <Button
                        color="red.7"
                        radius="0"
                        onClick={() => {
                          setRecentDocumentPaths(
                            remove_opened_document_paths(documentPath),
                          )
                          notifications.show({
                            message:
                              "removed recent document without deleting from disk",
                            autoClose: true,
                            withCloseButton: true,
                          })
                        }}
                      >
                        <CloseOutlined />
                      </Button>
                    </Button.Group>
                  )
                })}
              </Stack>
            </>
          )}
        </Stack>
      </FocusTrap>
    </HomeComponent>
  )
}
