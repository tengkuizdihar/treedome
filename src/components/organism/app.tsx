import { Route, Switch, useLocation } from "wouter"
import "./app.css"
import {
  CREATE_DOCUMENT_PATH,
  NOTE_PATH,
  OPEN_DOCUMENT_PATH,
  SETTINGS_DOCUMENT_PATH,
} from "../../util/route-path"
import { DocumentEditor } from "./document"
import { useEffect } from "react"
import { CreateDocument } from "./create-document"
import { OpenDocument } from "./open-document"
import { initOsType, osType } from "../../util/markdown-editor"
import { useAtom } from "jotai"
import { SettingsDocument } from "./settings-document"

function App() {
  const [_, setLocation] = useLocation()
  const [, setAtom] = useAtom(osType)

  useEffect(() => {
    initOsType(setAtom)
    setLocation("/open-document")
  }, [])

  return (
    <Switch>
      <Route path={`${NOTE_PATH}/:document_id/:search_text?`}>
        {(param) => (
          <DocumentEditor
            document_id={param.document_id}
            search_text={param.search_text}
          />
        )}
      </Route>
      <Route path={CREATE_DOCUMENT_PATH} component={CreateDocument} />
      <Route path={OPEN_DOCUMENT_PATH} component={OpenDocument} />
      <Route path={SETTINGS_DOCUMENT_PATH} component={SettingsDocument} />
    </Switch>
  )
}

export default App
