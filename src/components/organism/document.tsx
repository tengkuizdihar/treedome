import "./app.css"

import { TextEditor } from "../atom/text-editor"
import { useAtom } from "jotai"
import {
  currentTreeData,
  editorContent,
  editorIsChanged,
  editorRef,
  handleShortcuts,
  openNoteData,
  treedome_decode,
  treedome_encode,
} from "../../util/markdown-editor"
import { useEffect, useState } from "react"
import {
  meta_put,
  note_duplicate,
  note_get,
  spawn_shell_in_document_directory,
} from "../../util/treedome-app"
import {
  Button,
  Container,
  Divider,
  Grid,
  Input,
  Skeleton,
  Stack,
  Tooltip,
} from "@mantine/core"
import {
  buildActionFromTreeData,
  buildNoteParentList,
  NoteTree,
} from "../atom/note-tree"
import { useLocation } from "wouter"
import { NOTE_PATH } from "../../util/route-path"
import { notifications } from "@mantine/notifications"
import { concat, forEach, map, reduce } from "lodash"
import { useDisclosure } from "@mantine/hooks"
import { DeleteNoteButton } from "../molecule/delete-note-button"
import {
  CreateNewNoteModal,
  CreateNoteButton,
} from "../molecule/create-note-button"
import { EscapeMenuModal } from "../molecule/escape-menu-modal"
import {
  createDateNote,
  DEFAULT_DOCUMENT_CONTENT_STRING,
  DEFAULT_DOCUMENT_CONTENT,
  saveNoteAndTrees,
  get_terminal_command,
  get_tree_children,
} from "../../util/frontend-command"
import {
  ContentCopy,
  Menu,
  Shortcut,
  UnfoldLess,
  UnfoldMore,
} from "@mui/icons-material"
import { SpotlightAction, SpotlightProvider } from "@mantine/spotlight"
import { SearchModal } from "../molecule/search-modal"
import { HelpModal } from "../molecule/help-modal"
import { JSONContent } from "@tiptap/react"
import { SaveNoteButton } from "../molecule/save-note-button"
import { SaveConfirmationModal } from "../molecule/save-confirmation-modal"
import { NodeModel } from "@minoru/react-dnd-treeview"

export interface DocumentEditorProps {
  document_id: string
  search_text?: string
}

export function DocumentEditor(props: DocumentEditorProps) {
  const [_, setLocation] = useLocation()

  const [isLoaded, setIsLoaded] = useState(false)
  const [canLoad, setCanLoad] = useState(true) // acting like a mutex, so we will wait until the note is loaded to load another one. Don't use isLoaded because it's used by the UI.
  const [id, setId] = useState<string | undefined>()
  const [title, setTitleText] = useState("")

  const [treeData, setTreeData] = useAtom(currentTreeData)
  const [currentOpenNoteData, setCurrentOpenNoteData] = useAtom(openNoteData)
  const [spotlightActions, setSpotlightActions] = useState<SpotlightAction[]>(
    [],
  )

  // For Modals
  const [
    createNewNoteOpened,
    { open: createNewNoteOpen, close: createNewNoteClose },
  ] = useDisclosure(false)
  const [
    createNewNoteAsChildOpened,
    { open: createNewNoteAsChildOpen, close: createNewNoteAsChildClose },
  ] = useDisclosure(false)
  const [
    escapeMenuOpened,
    { close: escapeMenuClose, toggle: escapeMenuToggle },
  ] = useDisclosure(false)
  const [searchOpened, { close: searchClose, toggle: searchToggle }] =
    useDisclosure(false)
  const [helpModalOpened, { toggle: helpModalToggle, close: helpModalClose }] =
    useDisclosure(false)
  const [
    saveConfirmationOpened,
    { open: saveConfirmationOpen, close: saveConfirmationClose },
  ] = useDisclosure(false)

  const [editor] = useAtom(editorRef)
  const [, setContent] = useAtom(editorContent)
  const [isChanged, setIsChanged] = useAtom(editorIsChanged)
  const setChangedOnce = () => {
    if (!isChanged) {
      setIsChanged(true)
    }
  }

  useEffect(() => {
    editor?.on("update", setChangedOnce)

    return () => {
      editor?.off("update", setChangedOnce)
    }
  }, [editor])

  const reloadCurrentNote = async (stored_id: string) => {
    if (!canLoad) {
      return
    }

    setIsChanged(false)
    setCanLoad(false)
    setIsLoaded(false)

    let res = await note_get({
      ids: [stored_id],
    })

    let currentParent = ""
    forEach(treeData, (noteData) => {
      if (String(noteData.id) === stored_id) {
        currentParent = String(noteData.parent)
      }
    })

    // FIXME: cache currentlyOpen to an atomic hook
    let currentlyOpen: Set<string> = new Set()
    forEach(currentOpenNoteData, (v) => {
      currentlyOpen.add(String(v))
    })

    let parents = buildNoteParentList(treeData, currentParent)
    let parentToOpen: string[] = []
    forEach(parents, (parent) => {
      if (!currentlyOpen.has(parent)) {
        parentToOpen = concat(parentToOpen, parent)
      }
    })

    setCurrentOpenNoteData(concat(currentOpenNoteData, parentToOpen))

    setIsLoaded(true)
    setCanLoad(true)

    if (res.result === undefined || res.result.length === 0) {
      notifications.show({
        message: "note not found",
        color: "red.7",
        autoClose: true,
        withCloseButton: true,
      })
    }
    let note_result = res.result[0]

    setId(stored_id)
    setTitleText(note_result.data.title ?? "")

    let value =
      treedome_decode<JSONContent>(
        note_result.data.content ?? DEFAULT_DOCUMENT_CONTENT_STRING,
      ) ?? DEFAULT_DOCUMENT_CONTENT

    setContent(value)
  }

  const redirectToAnotherNote = (document_id: string) => {
    if (isChanged) {
      saveConfirmationOpen()
      return
    }

    let currentParent = ""
    forEach(treeData, (noteData) => {
      if (String(noteData.id) === document_id) {
        currentParent = String(noteData.parent)
      }
    })

    // FIXME: cache currentlyOpen to an atomic hook
    let currentlyOpen: Set<string> = new Set()
    forEach(currentOpenNoteData, (v) => {
      currentlyOpen.add(String(v))
    })

    let parents = buildNoteParentList(treeData, currentParent)
    let parentToOpen: string[] = []
    forEach(parents, (parent) => {
      if (!currentlyOpen.has(parent)) {
        parentToOpen = concat(parentToOpen, parent)
      }
    })

    setCurrentOpenNoteData(concat(currentOpenNoteData, parentToOpen))
    setLocation(NOTE_PATH + `/${document_id}`)
  }

  useEffect(() => {
    reloadCurrentNote(props.document_id).catch((e) => {
      console.error("failed loading note from disk", e)
      notifications.show({
        message: "failed loading note from disk",
        color: "red.7",
        autoClose: true,
        withCloseButton: true,
      })
    })
  }, [props.document_id])

  useEffect(() => {
    setSpotlightActions(
      buildActionFromTreeData(treeData, redirectToAnotherNote),
    )
  }, [treeData, isChanged])

  const collapseAllNotes = () => {
    setCurrentOpenNoteData([])
  }

  const duplicateNoteIDs = async () => {
    try {
      setIsLoaded(false)
      let childrenAndParent = get_tree_children(treeData, props.document_id)

      let duplicated = await note_duplicate({
        ids: childrenAndParent,
      })
      let duplicatedMap: Record<string, string> = reduce(
        duplicated.result,
        (prev: Record<string, string>, v) => {
          prev[v.from] = v.to
          return prev
        },
        {},
      )

      const keyToData: Record<string, NodeModel> = {}
      forEach(treeData, (v) => {
        keyToData[v.id] = v
      })

      let newTreeData: NodeModel[] = []
      forEach(duplicated.result, (v) => {
        // transform all old id (own and parent) using duplicateMap
        let oldNode = keyToData[v.from]
        let newNode: NodeModel = {
          id: duplicatedMap[v.from],
          parent: duplicatedMap[oldNode.parent] ?? oldNode.parent,
          text: oldNode.text,
          droppable: true,
        }

        newTreeData = concat(newTreeData, newNode)
      })

      let newTree = concat(treeData, newTreeData)
      setTreeData(concat(treeData, newTreeData))
      await meta_put({
        tree: newTree,
        opened_notes: currentOpenNoteData,
        last_opened_note_id: props.document_id,
      })

      notifications.show({
        message: "successfully duplicating notes",
        color: "green.7",
        autoClose: 750,
        withCloseButton: true,
      })

      setIsLoaded(true)
    } catch (e) {
      console.error("failed duplicating your notes", e)
      notifications.show({
        message: "failed duplicating your notes",
        color: "red.7",
        withCloseButton: true,
      })

      setIsLoaded(true)
    }
  }

  const expandAllNotes = () => {
    let allNoteIDs: string[] = map(treeData, (v) => {
      return String(v.id)
    })
    setCurrentOpenNoteData(allNoteIDs)
  }

  return (
    <SpotlightProvider
      actions={spotlightActions} // make this done from query instead
      searchIcon={<Shortcut />}
      searchPlaceholder="Quick Shortcut..."
      shortcut="mod + shift + p"
      triggerOnContentEditable={true}
      nothingFoundMessage="No Shortcut Found..."
      limit={10}
      disabled={!isLoaded}
    >
      <Container
        size="100%"
        style={{ paddingTop: "0.8em", height: "100vh" }}
        onKeyDown={(event) => {
          handleShortcuts(event, {
            save: async () => {
              saveNoteAndTrees(
                true,
                props.document_id,
                title,
                treedome_encode(editor?.getJSON() ?? DEFAULT_DOCUMENT_CONTENT),
                editor?.getText() ?? "",
                treeData,
                currentOpenNoteData,
                () => setIsChanged(false),
              ).then((newTree) => {
                setTreeData(newTree)
              })
            },
            createNote: async () => createNewNoteOpen(),
            createNoteAsChild: async () => createNewNoteAsChildOpen(),
            createNoteDate: async () => {
              setCanLoad(false)
              setIsLoaded(false)

              const [dateID, newTree, newCurrentOpenNoteData] =
                await createDateNote(treeData, currentOpenNoteData)

              setTreeData(newTree)
              setCurrentOpenNoteData(newCurrentOpenNoteData)

              setCanLoad(true)
              setIsLoaded(true)

              setLocation(NOTE_PATH + `/${dateID}`)
            },
            escapeMenu: async () => {
              if (
                !(
                  createNewNoteOpened ||
                  createNewNoteAsChildOpened ||
                  searchOpened ||
                  helpModalOpened
                )
              ) {
                escapeMenuToggle()
              }
            },
            searchMenu: async () => {
              if (
                !(
                  createNewNoteOpened ||
                  createNewNoteAsChildOpened ||
                  escapeMenuOpened ||
                  helpModalOpened
                )
              ) {
                searchToggle()
              }
            },
            helpMenu: async () => {
              if (
                !(
                  createNewNoteOpened ||
                  createNewNoteAsChildOpened ||
                  escapeMenuOpened ||
                  searchOpened
                )
              ) {
                helpModalToggle()
              }
            },
            spawnNewTerminal: async () => {
              try {
                await spawn_shell_in_document_directory({
                  command: get_terminal_command(),
                })
              } catch (e) {
                notifications.show({
                  message: `failed opening terminal: ${e}`,
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              }
            },
          })
        }}
        tabIndex={0} // yes, this is important so onKeyDown can work anywhere you're focused on.
      >
        <Grid>
          <EscapeMenuModal
            opened={escapeMenuOpened}
            onClose={escapeMenuClose}
            searchToggle={searchToggle}
            helpModalToggle={helpModalToggle}
            centered
          />
          <CreateNewNoteModal
            makeChildNoteID={false}
            opened={createNewNoteOpened}
            close={createNewNoteClose}
            noteID={props.document_id}
          />
          <CreateNewNoteModal
            makeChildNoteID={true}
            opened={createNewNoteAsChildOpened}
            close={createNewNoteAsChildClose}
            noteID={props.document_id}
          />
          <SearchModal opened={searchOpened} onClose={searchClose} />
          <HelpModal
            opened={helpModalOpened}
            onClose={helpModalClose}
            centered
          />
          <SaveConfirmationModal
            opened={saveConfirmationOpened}
            close={saveConfirmationClose}
          />

          <Grid.Col orderMd={0} orderSm={1} md={3}>
            <Skeleton visible={!isLoaded}>
              <Stack
                spacing="0"
                h="96vh"
                style={{ backgroundColor: "#101113" }}
              >
                <Button.Group p="sm">
                  <Tooltip label="Open Menu">
                    <Button
                      w="100%"
                      variant="outline"
                      color="blue.2"
                      onClick={() => escapeMenuToggle()}
                    >
                      <Menu />
                    </Button>
                  </Tooltip>
                  <Tooltip
                    label="Expand All Trees"
                    onClick={() => expandAllNotes()}
                  >
                    <Button w="100%" variant="outline" color="blue.2">
                      <UnfoldMore />
                    </Button>
                  </Tooltip>
                  <Tooltip
                    label="Collapse All Trees"
                    onClick={() => collapseAllNotes()}
                  >
                    <Button w="100%" variant="outline" color="blue.2">
                      <UnfoldLess />
                    </Button>
                  </Tooltip>
                  <Tooltip
                    label="Duplicate Note"
                    onClick={() => duplicateNoteIDs()}
                  >
                    <Button w="100%" variant="outline" color="blue.2">
                      <ContentCopy />
                    </Button>
                  </Tooltip>
                </Button.Group>
                <Divider my="xs" />
                <NoteTree
                  currentOpenID={props.document_id}
                  onNoteClick={(id) => redirectToAnotherNote(id)}
                />
              </Stack>
            </Skeleton>
          </Grid.Col>

          <Grid.Col orderMd={1} orderSm={0} md={9}>
            <Skeleton visible={!isLoaded}>
              {/* FIXME: oh my god the height thingy makes me so mad */}
              <Stack
                justify="flex-start"
                spacing="xs"
                align="stretch"
                style={{ height: "96vh" }}
              >
                <Grid gutter="xs">
                  <Grid.Col span="auto">
                    <Input
                      placeholder="Title"
                      type="text"
                      style={{ minWidth: "100%" }}
                      value={title}
                      onChange={(e: any) => setTitleText(e.target.value)}
                    />
                  </Grid.Col>
                  <Tooltip label="Create New Note">
                    <Grid.Col span="content">
                      <CreateNoteButton noteID={id} />
                    </Grid.Col>
                  </Tooltip>
                  <Tooltip label="Delete Current Note">
                    <Grid.Col span="content">
                      <DeleteNoteButton note_id={id} note_title={title} />
                    </Grid.Col>
                  </Tooltip>
                  {isChanged && (
                    <Tooltip label="Save Button">
                      <Grid.Col span="content">
                        <SaveNoteButton
                          onClick={() => {
                            saveNoteAndTrees(
                              true,
                              props.document_id,
                              title,
                              treedome_encode(
                                editor?.getJSON() ?? DEFAULT_DOCUMENT_CONTENT,
                              ),
                              editor?.getText() ?? "",
                              treeData,
                              currentOpenNoteData,
                              () => setIsChanged(false),
                            ).then((newTree) => {
                              setTreeData(newTree)
                            })
                          }}
                        />
                      </Grid.Col>
                    </Tooltip>
                  )}
                </Grid>

                {/* YEAH THIS IS IT, THE TEXT EDITOR! */}
                {/* YEAH THIS IS IT, THE TEXT EDITOR! */}
                {/* YEAH THIS IS IT, THE TEXT EDITOR! */}
                {/* YEAH THIS IS IT, THE TEXT EDITOR! */}
                {/* YEAH THIS IS IT, THE TEXT EDITOR! */}
                {/* YEAH THIS IS IT, THE TEXT EDITOR! */}
                <TextEditor search_text={props.search_text} />
              </Stack>
            </Skeleton>
          </Grid.Col>
        </Grid>
      </Container>
    </SpotlightProvider>
  )
}
