import { notifications } from "@mantine/notifications"
import { NodeModel } from "@minoru/react-dnd-treeview"
import {
  cloneDeep,
  concat,
  dropRight,
  filter,
  find,
  forEach,
  map,
  max,
} from "lodash"
import { meta_put, note_put } from "./treedome-app"
import { treedome_decode, treedome_encode } from "./markdown-editor"

export const TERMINAL_COMMAND_LS_KEY = "TERMINAL_COMMAND" // LS: Local Storage
export const RECENT_DOCUMENT_PATHS_LS_KEY = "RECENT_DOCUMENT_PATHS" // LS: Local Storage
export const MAX_RECENT_DOCUMENT_PATHS = 5
export const MONTH_NAMES = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
]

export const DEFAULT_DOCUMENT_CONTENT = {
  type: "doc",
  content: [{ type: "paragraph", attrs: { textAlign: "left" } }],
}
export const DEFAULT_DOCUMENT_CONTENT_STRING = treedome_encode(
  DEFAULT_DOCUMENT_CONTENT,
)

// DANGER: plaintext data use with caution!
// Get recently opened document paths from local storage
export const get_recently_opened_document_paths =
  (): RecentDocumentPathsModel => {
    let raw = localStorage.getItem(RECENT_DOCUMENT_PATHS_LS_KEY) ?? "[]"
    return treedome_decode(raw) ?? []
  }

// DANGER: plaintext data use with caution!
// set recently opened document paths from local storage
export const set_opened_document_paths = (document_path: string): string[] => {
  let currentPaths = get_recently_opened_document_paths()

  let existed = find(currentPaths, (path) => path === document_path)
  if (existed !== undefined) {
    return currentPaths
  }

  let newPaths = [document_path, ...currentPaths]
  let toDrop = max([0, newPaths.length - MAX_RECENT_DOCUMENT_PATHS])
  newPaths = dropRight(newPaths, toDrop)

  localStorage.setItem(RECENT_DOCUMENT_PATHS_LS_KEY, treedome_encode(newPaths))
  return newPaths
}

export const remove_opened_document_paths = (
  document_path: string,
): string[] => {
  let currentPaths = get_recently_opened_document_paths()

  let filtered = filter(currentPaths, (path) => !(path === document_path))
  localStorage.setItem(RECENT_DOCUMENT_PATHS_LS_KEY, treedome_encode(filtered))
  return filtered
}

// TODO: only save when there's a change in title and content
export const saveNoteAndTrees = async (
  notify_save_success: boolean,
  saved_id: string,
  title: string,
  editor_content: string,
  text_only: string,
  treeData: NodeModel[],
  currentOpenNoteData: RecentDocumentPathsModel,
  onSave?: () => void,
): Promise<NodeModel[]> => {
  let newTree = cloneDeep(treeData)
  newTree = map(newTree, (data) => {
    if (data.id == saved_id) {
      data.text = title
    }
    return data
  })

  meta_put({
    tree: newTree,
    opened_notes: currentOpenNoteData,
    last_opened_note_id: saved_id,
  }).catch((e) => {
    console.error("failed saving your note tree to disk", e)
    notifications.show({
      message: "failed saving your note tree to disk",
      color: "red.7",
      autoClose: true,
      withCloseButton: true,
    })
  })

  note_put({
    id: saved_id,
    title,
    content: editor_content,
    text_only: text_only,
  })
    .catch((e) => {
      console.error("failed saving your notes to disk", e)
      notifications.show({
        message: "failed saving your notes to disk",
        color: "red.7",
        autoClose: true,
        withCloseButton: true,
      })
    })
    .then(() => {
      onSave?.()
      if (notify_save_success) {
        notifications.show({
          id: "success-saving-note",
          message: "successfully saving to disk",
          color: "green.7",
          autoClose: 750,
          withCloseButton: true,
        })
      }
    })

  return newTree
}

// returns the id of the current date, used to redirect it to that note id
// TODO: optimize this, please
export const createDateNote = async (
  treeData: NodeModel[],
  currentOpenNoteData: string[],
): Promise<[string, NodeModel[], string[]]> => {
  let dateObj = new Date()
  let yearText = String(dateObj.getFullYear())
  let monthText = MONTH_NAMES[dateObj.getMonth()]
  let dateText = String(dateObj.getDate())

  // if empty string then it doesn't exist
  let newTree = cloneDeep(treeData)

  // generate year node
  let yearID = find(newTree, (v) => {
    return String(v.parent) === "0" && v.text === yearText
  })?.id

  if (yearID === undefined) {
    let res = await note_put({
      title: yearText,
      content: DEFAULT_DOCUMENT_CONTENT_STRING,
      text_only: "",
    })

    yearID = res.id
    newTree = [
      ...newTree,
      {
        id: yearID,
        parent: "0",
        text: yearText,
        droppable: true,
      },
    ]
  }

  // generate month node
  let monthID = find(newTree, (v) => {
    return String(v.parent) === yearID && v.text === monthText
  })?.id

  if (monthID === undefined) {
    let res = await note_put({
      title: monthText,
      content: DEFAULT_DOCUMENT_CONTENT_STRING,
      text_only: "",
    })

    monthID = res.id
    newTree = [
      ...newTree,
      {
        id: monthID,
        parent: yearID,
        text: monthText,
        droppable: true,
      },
    ]
  }

  // generate date node
  let dateID = find(newTree, (v) => {
    return String(v.parent) === (monthID ?? "") && v.text === dateText
  })?.id

  if (dateID === undefined) {
    let res = await note_put({
      title: dateText,
      content: DEFAULT_DOCUMENT_CONTENT_STRING,
      text_only: "",
    })

    dateID = res.id
    newTree = [
      ...newTree,
      {
        id: dateID,
        parent: monthID,
        text: dateText,
        droppable: true,
      },
    ]
  }

  return [
    String(dateID),
    newTree,
    [...currentOpenNoteData, String(yearID), String(monthID)],
  ]
}

export const get_terminal_command = (): string => {
  return localStorage.getItem(TERMINAL_COMMAND_LS_KEY) ?? ""
}

export const set_terminal_command = (command: string): void => {
  localStorage.setItem(TERMINAL_COMMAND_LS_KEY, command)
}

export const get_tree_children = (
  treeNote: NodeModel[],
  note_id: string,
): string[] => {
  // build map of parent => children
  const parentToChildren: Record<string, string[]> = {}
  forEach(treeNote, (v) => {
    parentToChildren[v.parent] = concat(
      parentToChildren[v.parent] ?? [],
      String(v.id),
    )
  })

  // build note ids to delete
  let childrenIDsWithParent: Array<string> = [note_id]
  let deleteNoteIDsMap: Record<string, boolean> = { [note_id]: true }
  let idx = 0
  while (childrenIDsWithParent.length > idx) {
    let toSearch = childrenIDsWithParent[idx]

    if (toSearch !== undefined) {
      let children = parentToChildren[toSearch] ?? []
      childrenIDsWithParent = concat(childrenIDsWithParent, children)
      deleteNoteIDsMap[toSearch] = true
      forEach(children, (v) => {
        deleteNoteIDsMap[v] = true
      })
    }

    idx += 1
  }

  return childrenIDsWithParent
}

// Types
export type RecentDocumentPathsModel = string[]
export type TerminalCommand = string
