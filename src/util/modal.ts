import { ModalProps } from "@mantine/core"

export const INSTANT_TRANSITION_MODAL: Partial<ModalProps> = {
  overlayProps: {
    transitionProps: {
      duration: 0,
    },
  },
  transitionProps: {
    duration: 0,
  },
}
