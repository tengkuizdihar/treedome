import { atom } from "jotai"
import { SearchResult } from "./treedome-app"

export const searchResult = atom<SearchResult[]>([])
export enum SearchingStateEnum {
  InProgress,
  Done,
}
export const searchingState = atom<SearchingStateEnum>(SearchingStateEnum.Done)
