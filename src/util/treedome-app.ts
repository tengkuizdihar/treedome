import { invoke } from "@tauri-apps/api"

export const GIT_URL = "https://gitlab.com/tengkuizdihar/treedome"
export const CONTACT_URL = "https://matrix.to/#/#treedome:matrix.org"

// All commands below should be synced with treedome/src-tauri/src/commands.rs
export const note_put = async (param: NotePutParam): Promise<NotePutResult> => {
  return invoke("note_put", { param })
}

export const note_get = async (param: NoteGetParam): Promise<NoteGetResult> => {
  return invoke("note_get", { param })
}

export const note_search = async (
  param: NoteSearchParam,
): Promise<NoteSearchResult> => {
  return invoke("note_search", { param })
}

export const note_delete = async (
  param: NoteDeleteParam,
): Promise<NoteDeleteResult> => {
  return invoke("note_delete", { param })
}

export const note_duplicate = async (
  param: NoteDuplicateParam,
): Promise<NoteDuplicateResult> => {
  return invoke("note_duplicate", { param })
}

export const document_close = async (
  param: DocumentCloseParamCommand,
): Promise<DocumentOpenResultCommand> => {
  return invoke("document_close", { param })
}

export const document_open = async (
  param: DocumentOpenParamCommand,
): Promise<DocumentOpenResultCommand> => {
  return invoke("document_open", { param })
}

export const password_quality_check = (
  param: PasswordQualityCheckParamCommand,
): Promise<void> => {
  return invoke("password_quality_check", { param })
}

export const meta_put = (param: MetaPutParamCommand): Promise<void> => {
  return invoke("meta_put", { param })
}

export const document_compact = (): Promise<void> => {
  return invoke("document_compact", { param: {} })
}

export const document_change_password_export = (
  param: DocumentChangePasswordExportParamCommand,
): Promise<void> => {
  return invoke("document_change_password_export", { param })
}

export const spawn_shell_in_document_directory = (
  param: SpawnShellInDocumentDirectoryParamCommand,
): Promise<SpawnShellInDocumentDirectoryResultCommand> => {
  return invoke("spawn_shell_in_document_directory", { param })
}

/// ================== Param/Result ==================

export interface NotePutParam {
  // note that already exists must fill id
  id?: string

  title: string
  content: string
  text_only: string
}

export interface NotePutResult {
  id: string
}

export interface NoteGetParam {
  ids: string[]
}

export interface NoteGetResult {
  result: DecryptedModel<NoteModel>[]
}

export interface NoteDeleteParam {
  note_ids: string[]
}

export interface NoteDeleteResult {}

export interface DocumentCloseParamCommand {}

export interface DocumentOpenParamCommand {
  path: string
  password: string
}

export interface DocumentOpenResultCommand {
  meta_model: MetaModel
}

export interface PasswordQualityCheckParamCommand {
  password: string
}

export interface MetaPutParamCommand {
  tree: NoteTreeData[]
  opened_notes: string[]
  last_opened_note_id: string
}

export interface MetaGetResultCommand {
  result: DecryptedModel<MetaModel>
}

export interface NoteDuplicateParam {
  ids: string[]
}

export interface NoteDuplicateResult {
  result: DuplicatedNote[]
}

export interface DocumentChangePasswordExportParamCommand {
  new_password: string
}

export interface SpawnShellInDocumentDirectoryParamCommand {
  command: string
}

export interface SpawnShellInDocumentDirectoryResultCommand {}

export interface NoteSearchParam {
  query: string
}

export interface NoteSearchResult {
  result: SearchResult[]
}

/// ================== Data Models ==================

export interface DecryptedModel<T> {
  id: string
  data: T
}

export interface NoteModel {
  title: string
  content: string
  text_only: string
}

export interface MetaModel {
  tree: NoteTreeData[]
  opened_notes: string[]
  last_opened_note_id: string
}

export interface NoteTreeData {
  id: string | number
  parent: string | number
  droppable?: boolean
  text: string
}

export interface SearchResult {
  note_id: string
  title: string
  ranges: FoundRange[]
}

export interface FoundRange {
  start: number
  end: number
}

export interface DuplicatedNote {
  from: string
  to: string
}
