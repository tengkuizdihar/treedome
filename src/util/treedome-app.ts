import { invoke } from "@tauri-apps/api/core"

export const GIT_URL = "https://codeberg.org/solver-orgz/treedome"
export const GIT_ISSUE_URL =
  "https://codeberg.org/solver-orgz/treedome/issues/new"
export const CONTACT_URL = "https://matrix.to/#/%23treedome:matrix.org"
export const LICENSE_URL =
  "https://codeberg.org/solver-orgz/treedome/src/branch/master/LICENSE"

// All commands below should be synced with treedome/src-tauri/src/commands.rs
export const note_put = async (param: NotePutParam): Promise<NotePutResult> => {
  return invoke("note_put", { param })
}

export const note_get = async (param: NoteGetParam): Promise<NoteGetResult> => {
  return invoke("note_get", { param })
}

export const note_search = async (
  param: NoteSearchParam,
): Promise<NoteSearchResult> => {
  return invoke("note_search", { param })
}

export const note_delete = async (
  param: NoteDeleteParam,
): Promise<NoteDeleteResult> => {
  return invoke("note_delete", { param })
}

export const note_duplicate = async (
  param: NoteDuplicateParam,
): Promise<NoteDuplicateResult> => {
  return invoke("note_duplicate", { param })
}

export const note_get_size = async (): Promise<NoteGetSizeResult> => {
  return invoke("note_get_size", { param: {} })
}

export const document_close = async (
  param: DocumentCloseParamCommand,
): Promise<DocumentOpenResultCommand> => {
  return invoke("document_close", { param })
}

export const document_open = async (
  param: DocumentOpenParamCommand,
): Promise<DocumentOpenResultCommand> => {
  return invoke("document_open", { param })
}

export const password_quality_check = (
  param: PasswordQualityCheckParamCommand,
): Promise<void> => {
  return invoke("password_quality_check", { param })
}

export const meta_put = (
  param: MetaPutParamCommand,
): Promise<MetaPutResultCommand> => {
  return invoke("meta_put", { param })
}

export const document_compact = (): Promise<void> => {
  return invoke("document_compact", { param: {} })
}

export const document_change_password_export = (
  param: DocumentChangePasswordExportParamCommand,
): Promise<void> => {
  return invoke("document_change_password_export", { param })
}

export const spawn_shell_in_document_directory = (
  param: SpawnShellInDocumentDirectoryParamCommand,
): Promise<SpawnShellInDocumentDirectoryResultCommand> => {
  return invoke("spawn_shell_in_document_directory", { param })
}

export const get_version = (): Promise<string> => {
  return invoke("get_version", {})
}

/// ================== Param/Result ==================

export interface NotePutParam {
  // note that already exists must fill id
  id?: string

  title: string
  content: string
  text_only: string
  tags: string[]
  create_at: string
  create_new: boolean
}

export interface NotePutResult {
  id: string
  title: string
}

export interface NoteGetParam {
  ids: string[]
}

export interface NoteGetResult {
  result: DecryptedModel<NoteModel>[]
}

export interface NoteDeleteParam {
  note_ids: string[]
}

export interface NoteDeleteResult {}

export interface DocumentCloseParamCommand {}

export interface DocumentOpenParamCommand {
  path: string
  password: string
}

export interface DocumentOpenResultCommand {
  meta_model: MetaModel
}

export interface PasswordQualityCheckParamCommand {
  password: string
}

export interface MetaPutParamCommand {
  tree: NoteTreeData[]
  opened_notes: string[]
  last_opened_note_id: string
  preference: PreferenceModel
}

export interface MetaPutResultCommand {
  tree: NoteTreeData[]
}

export interface MetaGetResultCommand {
  result: DecryptedModel<MetaModel>
}

export interface NoteDuplicateParam {
  ids: string[]
}

export interface NoteDuplicateResult {
  result: DuplicatedNote[]
}

export interface NoteGetSizeResult {
  note_sizes: NoteSizeData[]
}

export interface NoteSizeData {
  note_id: string
  size: number
}

export interface DocumentChangePasswordExportParamCommand {
  new_password: string
}

export interface SpawnShellInDocumentDirectoryParamCommand {
  command: string
}

export interface SpawnShellInDocumentDirectoryResultCommand {}

export interface NoteSearchParam {
  query: string
}

export interface NoteSearchResult {
  result: SearchResult[]
}

/// ================== Data Models ==================

export interface DecryptedModel<T> {
  id: string
  data: T
}

export interface NoteModel {
  title: string
  content: string
  text_only: string
  tags?: string[]
  create_at?: string
  update_at?: string
}

export interface MetaModel {
  tree: NoteTreeData[]
  opened_notes: string[]
  last_opened_note_id: string
  preference?: PreferenceModel
}

export interface PreferenceModel {
  open_timeout_ms?: number
  enable_toolbar?: boolean
  enable_floating_toolbar?: boolean
  document_name?: string
}

export interface NoteTreeData {
  id: string | number
  parent: string | number
  droppable?: boolean
  text: string
  data?: DeepNoteData
}

export interface DeepNoteData {
  tags?: string[]
}

export interface SearchResult {
  note_id: string
  title: string
  ranges: FoundRange[]
  tags: string[]
  text: string
}

export interface FoundRange {
  start: number
  end: number
  before: string
  current: string
  after: string
}

export interface DuplicatedNote {
  from: string
  to: string
}
