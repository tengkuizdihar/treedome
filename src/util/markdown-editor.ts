import { atom } from "jotai"
import { KeyboardEvent } from "react"
import { NodeModel } from "@minoru/react-dnd-treeview"
import { forEach, isEqual, map, omit } from "lodash"
import { Editor, JSONContent } from "@tiptap/react"
import { SpotlightActionData } from "@mantine/spotlight"
import { OsType, type } from "@tauri-apps/plugin-os"
import { DeepNoteData } from "./treedome-app"

export const osType = atom<OsType>("linux")
export const initOsType = (setAtom: (s: OsType) => void) => {
  setAtom(type())
}

export const editorRef = atom<Editor | null>(null) // use this to get editor's content
export const editorContent = atom<JSONContent>({}) // use this to set editor's content
export const openNoteData = atom<string[]>([])
export const editorIsChanged = atom(false)
export const noteSizeData = atom<NoteSizeData>()

export type Actions =
  | "save"
  | "exit"
  | "createNote"
  | "createNoteAsChild"
  | "createNoteDate"
  | "escapeMenu"
  | "searchMenu"
  | "quickAccessMenu"
  | "helpMenu"
  | "spawnNewTerminal"

export type ActionKeyItems = {
  [k in Actions]: KeyItems
}

export type ShortcutCallbacks = {
  [k in Actions]?: () => Promise<void>
}

export interface KeyItems {
  isModifier: boolean
  isShift: boolean
  key: string
  title: string
}

export const DEFAULT_ACTION_KEY_ITEMS: ActionKeyItems = {
  save: {
    isModifier: true,
    isShift: false,
    key: "s",
    title: "Save Note",
  },

  exit: {
    isModifier: true,
    isShift: true,
    key: "q",
    title: "Exit Program",
  },

  createNote: {
    isModifier: true,
    isShift: false,
    key: "n",
    title: "Create Note",
  },

  createNoteAsChild: {
    isModifier: true,
    isShift: true,
    key: "n",
    title: "Create Child Note",
  },

  escapeMenu: {
    isModifier: false,
    isShift: false,
    key: "escape",
    title: "Menu",
  },

  createNoteDate: {
    isModifier: true,
    isShift: true,
    key: "d",
    title: "Create Diary Note",
  },

  searchMenu: {
    isModifier: true,
    isShift: true,
    key: "f",
    title: "Open Search",
  },

  helpMenu: {
    isModifier: false,
    isShift: false,
    key: "f1",
    title: "Open Help",
  },

  quickAccessMenu: {
    isModifier: true,
    isShift: true,
    key: "p",
    title: "Open Quick Access",
  },

  spawnNewTerminal: {
    isModifier: true,
    isShift: true,
    key: "c",
    title: "Spawn a New Terminal",
  },
}

export interface NoteSizeData {
  show: boolean
  data: Map<string, number> // key: note_id, value in byte
}

export const handleShortcuts = async <T = Element>(
  event: KeyboardEvent<T>,
  callbacks: ShortcutCallbacks,
) => {
  let pressed: KeyItems = {
    isModifier: event.ctrlKey || event.metaKey,
    isShift: event.shiftKey,
    key: event.key.toLowerCase(),
    title: "",
  }

  // TODO-KEYBIND: make it check from a store or something, not hardcoded like this
  forEach(DEFAULT_ACTION_KEY_ITEMS, (keyItem, key) => {
    if (isEqual(omit(keyItem, ["title"]), omit(pressed, ["title"]))) {
      let callback = callbacks[key as Actions]
      callback?.()
    }
  })
}

const SampleData = [
  {
    id: "1",
    parent: "0",
    droppable: true,
    text: "Folder 1",
  },
  {
    id: "2",
    parent: "1",
    droppable: true,
    text: "File 1-1",
  },
  {
    id: "3",
    parent: "2",
    droppable: true,
    text: "File 1-2",
  },
  {
    id: "4",
    parent: "3",
    droppable: true,
    text: "Folder 2",
  },
  {
    id: "5",
    parent: "4",
    droppable: true,
    text: "Folder 2-1",
  },
  {
    id: "6",
    parent: "5",
    droppable: true,
    text: "File 2-1-1",
  },
  {
    id: "7",
    parent: "6",
    droppable: true,
    text: "File 3",
  },
]

export const currentTreeData = atom<NodeModel<DeepNoteData>[]>([])

export const uniqueTagsFromTreeData = (
  tree: NodeModel<DeepNoteData>[],
): string[] => {
  let tags = new Set<string>()

  map(tree, (v) => {
    map(v.data?.tags, (t) => {
      if (!tags.has(t)) {
        tags.add(t)
      }
    })
  })

  return Array.from(tags)
}

// Serialization related
// Changing this function will break the way we store things to backend!
export function treedome_encode<T>(input: T): string {
  try {
    return JSON.stringify(input)
  } catch (e) {
    console.error("somehow failed to encode from object")
    return ""
  }
}

// Changing this function will break the way we store things to backend!
export function treedome_decode<T>(input: string): T | undefined {
  try {
    return JSON.parse(input)
  } catch (e) {
    return undefined
  }
}
