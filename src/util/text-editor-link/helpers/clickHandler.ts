// forked from https://github.com/ueberdosis/tiptap/blob/ec6121da1c3f808987d32de7a8c56b52520bfea8/packages/extension-link
// Originally, MIT License Copyright (c) 2024, Tiptap GmbH
// Now, GNU Affero Public License version 3 or any later version, Copyright (c) 2024, Solver Orgz

import { getAttributes } from "@tiptap/core"
import { MarkType } from "@tiptap/pm/model"
import { Plugin, PluginKey } from "@tiptap/pm/state"
import { open } from "@tauri-apps/plugin-shell"
import { isAllowedUri } from ".."

type ClickHandlerOptions = {
  type: MarkType
}

export function clickHandler(options: ClickHandlerOptions): Plugin {
  return new Plugin({
    key: new PluginKey("handleClickLink"),
    props: {
      handleClick: (view, _pos, event) => {
        if (event.button !== 0) {
          return false
        }
        if (!view.editable) {
          return false
        }

        let a = event.target as HTMLElement
        const els = []

        while (a.nodeName !== "DIV") {
          els.push(a)
          a = a.parentNode as HTMLElement
        }

        const attrs = getAttributes(view.state, options.type.name)
        const link = event.target as HTMLLinkElement

        const href = link?.href ?? attrs.href

        if (!isAllowedUri(href)) {
          return false
        }

        if (link && href) {
          open(href)
          return false
        }

        return false
      },
    },
  })
}
