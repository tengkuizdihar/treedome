// forked from https://github.com/ueberdosis/tiptap/blob/ec6121da1c3f808987d32de7a8c56b52520bfea8/packages/extension-link
// Originally, MIT License Copyright (c) 2024, Tiptap GmbH
// Now, GNU Affero Public License version 3 or any later version, Copyright (c) 2024, Solver Orgz

import { Editor } from "@tiptap/core"
import { MarkType } from "@tiptap/pm/model"
import { Plugin, PluginKey } from "@tiptap/pm/state"
import { find } from "linkifyjs"

type PasteHandlerOptions = {
  editor: Editor
  defaultProtocol: string
  type: MarkType
}

export function pasteHandler(options: PasteHandlerOptions): Plugin {
  return new Plugin({
    key: new PluginKey("handlePasteLink"),
    props: {
      handlePaste: (view, event, slice) => {
        const { state } = view
        const { selection } = state
        const { empty } = selection

        if (empty) {
          return false
        }

        let textContent = ""

        slice.content.forEach((node) => {
          textContent += node.textContent
        })

        const link = find(textContent, {
          defaultProtocol: options.defaultProtocol,
        }).find((item) => item.isLink && item.value === textContent)

        if (!textContent || !link) {
          return false
        }

        options.editor.commands.setMark(options.type, {
          href: link.href,
        })

        return true
      },
    },
  })
}
