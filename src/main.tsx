import React from "react"
import App from "./components/organism/app"
import "./style.css"
import ReactDOM from "react-dom"
import { MantineProvider } from "@mantine/core"
import { ModalsProvider } from "@mantine/modals"
import { Notifications } from "@mantine/notifications"
import "@fontsource/noto-sans"
import "@fontsource/noto-sans-mono"

ReactDOM.render(
  <React.StrictMode>
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        fontFamily: "Noto Sans",
        fontFamilyMonospace: "Noto Sans Mono",
        spacing: {
          xs: "1rem",
          sm: "1.2rem",
          md: "1.8rem",
          lg: "2.2rem",
          xl: "2.8rem",
        },
        colorScheme: "dark",
      }}
    >
      <ModalsProvider>
        <Notifications />
        <App />
      </ModalsProvider>
    </MantineProvider>
  </React.StrictMode>,
  document.getElementById("root"),
)
