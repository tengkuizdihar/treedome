# from https://github.com/ivangabriele/docker-tauri
# to build and push this image do these commands
# don't forget to login using: docker login -u tengkuizdihar
#
# - docker build -t rustyarn . --network=host
# - docker image tag rustyarn tengkuizdihar/rustyarn:latest
# - docker image push tengkuizdihar/rustyarn:latest

FROM rust:bookworm

LABEL "org.opencontainers.image.title"="Rust + Yarn"
LABEL "org.opencontainers.image.description"="Rust and Yarn Docker perfect base build for tauri application"
LABEL "org.opencontainers.image.authors"="tengkuizdihar"

# Install base utils
RUN apt-get update
RUN apt-get install -y \
  curl \
  psmisc

# Install Node.js
RUN curl -fsSL "https://deb.nodesource.com/setup_18.x" | bash -
RUN apt-get install -y nodejs

# Install Yarn
RUN corepack enable

# Install Tarpaulin
RUN cargo install cargo-tarpaulin

# Install Tauri dependencies
# https://tauri.app/v1/guides/getting-started/prerequisites#setting-up-linux
RUN apt-get install -y \
  libwebkit2gtk-4.0-dev \
  build-essential \
  curl \
  wget \
  libssl-dev \
  libgtk-3-dev \
  libayatana-appindicator3-dev \
  librsvg2-dev

# Install tauri-driver
# https://tauri.app/v1/guides/testing/webdriver/introduction#system-dependencies
# DON'T INSTALL, STILL PRE-ALPHA
# Install tauri-driver dependencies
# RUN apt-get install -y \
#   webkit2gtk-4.0-dev \
#   webkit2gtk-driver \
#   xvfb

# RUN cargo install tauri-driver

CMD ["/bin/bash"]