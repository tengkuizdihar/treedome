{
  # main
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/0c582677378f2d9ffcb01490af2f2c678dcb29d3";
  };

  # dev
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      ...
    }@inputs:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (
      system:
      let
        pkgs = import nixpkgs { inherit system; };
        treedome = pkgs.callPackage ./nix/treedome.nix { };
      in
      {
        devShells.default = pkgs.mkShell {
          name = "development-environment-treedome";
          WEBKIT_DISABLE_COMPOSITING_MODE = "1"; # essential in NVIDIA + compositor https://github.com/NixOS/nixpkgs/issues/212064#issuecomment-1400202079
          XDG_DATA_DIRS = "${pkgs.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}:${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}:$XDG_DATA_DIRS";
          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
          packages = with pkgs; [
            cargo
            rustc
            rustfmt
            clippy
            nodePackages.yarn
            nodePackages.nodejs
            glib
            dbus
            cairo
            atk
            openssl
            libsoup
            pango
            gdk-pixbuf
            gtk3
            harfbuzz
            zlib
            gcc
            pkg-config
            webkitgtk_4_1
            appimagekit
            cargo-tauri
            nixpkgs-fmt
            gsettings-desktop-schemas
            sqlite
            just
          ];
        };
        packages.default = treedome;
      }
    );
}
