{ lib
, clang
, fetchFromGitHub
, llvmPackages_11
, rustPlatform
}:

rustPlatform.buildRustPackage rec {
  pname = "edma";
  version = "0.1.0-beta.5";

  src = fetchFromGitHub {
    owner = "nomadiz";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-IhzvEGlkUy6B9IWrwDrVtOFCtfim5KUxq+9fa+ef81c=";
  };

  cargoSha256 = "sha256-uPWwzlZ3GUdjWRT8gfMuQW0xtq54zKtkbNZCwPyzW1w=";

  # LIBCLANG_PATH = "${llvmPackages_11.libclang.lib}/lib";
  # BINDGEN_EXTRA_CLANG_ARGS = with pkgs; ''
  #   -isystem ${llvmPackages_11.libclang.lib}/lib/clang/${lib.getVersion clang}/include
  #   -isystem ${llvmPackages_11.libclang.out}/lib/clang/${lib.getVersion clang}/include
  #   -isystem ${glibc.dev}/include
  # '';

  nativeBuildInputs = [ rustPlatform.bindgenHook ];

  # test about db that doesn't work here
  doCheck = false;

  meta = with lib; {
    description = "an interactive terminal app for managing multiple embedded databases system at once with powerful byte deserializer support.";
    homepage = "https://github.com/nomadiz/edma";
    license = licenses.mit;
    maintainers = [{
      name = "Tengku Izdihar";
      email = "tengkuizdihar@gmail.com";
      matrix = "@tengkuizdihar:matrix.org";
      github = "tengkuizdihar";
      githubId = 22078730;
    }];
  };
}
